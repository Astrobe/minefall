-- doors/init.lua

-- our API object
mf_doors = {}

mf_doors.registered_trapdoors = {}

-- Load support for MT game translation.
local S = minetest.get_translator("doors")



----trapdoor----

function mf_doors.trapdoor_toggle(pos, node, clicker)
	node = node or minetest.get_node(pos)

	if clicker and not default.can_interact_with_node(clicker, pos) then
		return false
	end

	local def = minetest.registered_nodes[node.name]

	if string.sub(node.name, -5) == "_open" then
		minetest.sound_play(def.sound_close,
			{pos = pos, gain = def.gain_close, max_hear_distance = 10}, true)
		minetest.swap_node(pos, {name = string.sub(node.name, 1,
			string.len(node.name) - 5), param1 = node.param1, param2 = node.param2})
	else
		minetest.sound_play(def.sound_open,
			{pos = pos, gain = def.gain_open, max_hear_distance = 10}, true)
		minetest.swap_node(pos, {name = node.name .. "_open",
			param1 = node.param1, param2 = node.param2})
	end
	-- linking
	pos.y=pos.y-1
	node=minetest.get_node(pos)
	if minetest.get_item_group(node.name, "door")~=0 then
		mf_doors.trapdoor_toggle(pos, node, clicker)
	end
end

function mf_doors.register_trapdoor(name, def)
	if not name:find(":") then
		name = "mf_doors:" .. name
	end

	local name_closed = name
	local name_opened = name.."_open"

	def.on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
		mf_doors.trapdoor_toggle(pos, node, clicker)
		return itemstack
	end

	-- Common trapdoor configuration
	def.drawtype = "nodebox"
	def.paramtype = "light"
	def.paramtype2 = "facedir"
	def.is_ground_content = false
	def.use_texture_alpha = def.use_texture_alpha or "clip"

	if def.groups.protected then
		def.after_place_node = function(pos, placer, itemstack, pointed_thing)
			local pn = placer:get_player_name()
			local meta = minetest.get_meta(pos)
			meta:set_string("owner", pn)
			meta:set_string("infotext", def.description .. "\n" .. S("Owned by @1", pn))

			return minetest.is_creative_enabled(pn)
		end

		def.on_key_use = function(pos, player)
			mf_doors.trapdoor_toggle(pos, nil, player)
		end
		def.on_skeleton_key_use = function(pos, player, newsecret)
			local meta = minetest.get_meta(pos)
			local owner = meta:get_string("owner")
			local pname = player:get_player_name()

			-- verify placer is owner of lockable door
			if owner ~= pname then
				minetest.record_protection_violation(pos, pname)
				minetest.chat_send_player(pname, S("You do not own this trapdoor."))
				return nil
			end

			local secret = meta:get_string("key_lock_secret")
			if secret == "" then
				secret = newsecret
				meta:set_string("key_lock_secret", secret)
			end

			return secret, S("a locked trapdoor"), owner
		end
		def.node_dig_prediction = ""
	else
		def.on_blast = function(pos, intensity)
			minetest.remove_node(pos)
			return {name}
		end
	end

	def.sounds = def.sounds or default.node_sound_wood_defaults()
	def.sound_open = def.sound_open or "doors_door_open"
	def.sound_close = def.sound_close or "doors_door_close"

	def.gain_open = def.gain_open or 0.3
	def.gain_close = def.gain_close or 0.3

	local def_opened = table.copy(def)
	local def_closed = table.copy(def)

	if def.nodebox_closed and def.nodebox_opened then
		def_closed.node_box = def.nodebox_closed
	else
		def_closed.node_box = {
		    type = "fixed",
		    fixed = {-0.5, -0.5, -0.5, 0.5, -6/16, 0.5}
		}
	end
	def_closed.selection_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, -6/16, 0.5}
	}
	def_closed.tiles = {
		def.tile_front,
		def.tile_front, -- .. '^[transformFY',
		def.tile_side,
		def.tile_side,
		def.tile_side,
		def.tile_side
	}
	def_closed.wield_image=def.tile_front

	if def.nodebox_opened and def.nodebox_closed then
		def_opened.node_box = def.nodebox_opened
	else
		def_opened.node_box = {
		    type = "fixed",
		    fixed = {-0.5, -0.5, 6/16, 0.5, 0.5, 0.5}
		}
	end
	def_opened.selection_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, 6/16, 0.5, 0.5, 0.5}
	}
	def_opened.tiles = {
		def.tile_side,
		def.tile_side .. '^[transform2',
		def.tile_side .. '^[transform3',
		def.tile_side .. '^[transform1',
		def.tile_front .. '^[transform46',
		def.tile_front .. '^[transform6'
	}

	def_opened.drop = name_closed
	def_opened.groups.not_in_creative_inventory = 1
	def_opened.on_place = minetest.rotate_node
	def_closed.on_place = minetest.rotate_node

	minetest.register_node(name_opened, def_opened)
	minetest.register_node(name_closed, def_closed)

	mf_doors.registered_trapdoors[name_opened] = true
	mf_doors.registered_trapdoors[name_closed] = true
end

mf_doors.register_trapdoor("mf_doors:trapdoor", {
	description = S("Wooden Trapdoor"),
	tile_front = "default_junglewood.png^[mask:td_mask.png",
	tile_side = "default_junglewood.png",
	gain_open = 0.06,
	gain_close = 0.13,
	groups = {oddly_breakable_by_hand = 2, flammable = 2, door = 1},
})

minetest.register_craft({
	output = "mf_doors:trapdoor",
	recipe = {
		{"", "group:stick", ""},
		{"group:stick", "craft_table:simple", "group:stick"},
		{"", "group:stick", ""},
	}
})

mf_doors.register_trapdoor("mf_doors:trapdoor_steel", {
	description = S("Steel Trapdoor"),
	tile_front = "default_steel_block.png^[mask:td_mask.png",
	tile_side = "default_steel_block.png",

	sounds = default.node_sound_metal_defaults(),
	sound_open = "doors_steel_door_open",
	sound_close = "doors_steel_door_close",
	gain_open = 0.2,
	gain_close = 0.2,
	groups = {cracky = 1, level = 2, door = 1, protected=1},
})

minetest.register_craft({
	output = "mf_doors:trapdoor_steel",
	type="shapeless",
	recipe = { "mf_doors:trapdoor", "default:skeleton_key", "default:steel_ingot" }
})

mf_doors.register_trapdoor("mf_doors:trapdoor_obsidian", {
	description = S("Obsidian Trapdoor"),
	tile_front = "default_obsidian_glass.png^(default_cloud.png^[multiply:black^[opacity:192)",
	tile_side = "default_obsidian_block.png",
	use_texture_alpha="blend",

	sounds = default.node_sound_metal_defaults(),
	sound_open = "doors_steel_door_open",
	sound_close = "doors_steel_door_close",
	gain_open = 0.1,
	gain_close = 0.1,
	groups = {cracky = 1, level = 2, door = 1, protected=1},
})
minetest.register_craft({
	output = "mf_doors:trapdoor_obsidian",
	type="shapeless",
	recipe = { "mf_doors:trapdoor", "default:obsidian_glass" }
})


