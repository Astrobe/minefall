-- Aberrant trees
-- By Astrobe, based on:

-- naturally growing trees
-- rnd, 2015

-- Might be different from the version of my Gitlab repo.

-- Licence is LGPL.

-- version 0.1

dofile(minetest.get_modpath("a_trees").."/lib.lua")

-- a_trees.register { trunk="default:fence_wood", leaves="default:fence_aspen_wood", fruit= "default:fence_rail_wood", fruity=40 }

-- a_trees.register {} -- default
-- a_trees.register { fruit="default:apple", fruity=100 } -- default with fruits
-- a_trees.register { trunk="default:ice", leaves="default:snowblock", slanty=2, fruit="default:river_water_source", fruity=100 }

--[[ aspen tree 
a_trees.register
{
	trunk="default:aspen_tree",
	leaves="default:aspen_leaves",
	branchy=10,
	height=5,
	width=5,
	pace=5
}
--]]

--[[
a_trees.register
{
	trunk="default:pine_tree",
	leaves="default:pine_needles",
	branchy=1,
	height=5,
	width=4,
	pace=5
}
--]]

--[[ 
a_trees.register
{
	trunk="default:jungletree",
	leaves="default:jungleleaves",
	branchy=1,
	size=50,
	height=15,
	width=4,
	pace=5
}
--]]

--[[
a_trees.register
{
	trunk="default:acacia_tree",
	leaves="default:acacia_leaves",
	branchy=1,
	height=2,
	width=10,
	pace=5
}
--]]

--[[
a_trees.register
{
	trunk="default:stone",
	leaves="default:cobble",
	fruit="default:stone_with_coal",
	fruity=50,
	pace=5
}
--]]

--[[
a_trees.register
{
	trunk="default:desert_stone",
	leaves="air",
	branchy=3,
	size=15,
	height=5,
	width=10,
	pace=5
}
--]]

--[[
a_trees.register
{
	trunk="default:steelblock",
	leaves="default:glass",
	fruit="default:meselamp",
	size=5,
	height=2,
	width=5,
	pace=5
}
--]]

