-- This mod is derived from Rnd's "rnd_trees" mod.
-- It adds a little API and some changes in the generation algorithm
-- Author: Astrobe
-- Licence: Same as Minetest.

local mfpace= 307


a_trees.register {fruit="default:apple", fruity=50, pace=mfpace}

--[[
a_trees.register
{
	trunk="default:basalt",
	leaves="default:basalt_cobble",
	fruit="default:lava_source",
	fruity=50,
	size=5,
	height=1,
	width=6,
	pace=mfpace
}
]]

a_trees.register
{
	trunk="default:palm_tree",
	leaves="default:palm_leaves",
	branchy=10,
	height=5,
	width=5,
	slanty=8,
	pace=mfpace
}

--[[
a_trees.register
{
	trunk="default:stone",
	fruit="mobs:cobweb",
	leaves="default:stone_with_tin",
	size=5,
	height=2,
	width=5,
	pace=mfpace
}

a_trees.register
{
	trunk="default:stone",
	leaves="default:mossycobble",
	fruit="default:stone_with_coal",
	fruity=50,
	pace=mfpace
}

a_trees.register
{
	trunk="default:stone",
	leaves="default:mossycobble",
	fruit="default:stone_with_tin",
	fruity=50,
	pace=mfpace
}
--]]

a_trees.register
{
	trunk="default:aspen_tree",
	leaves="default:aspen_leaves",
	branchy=10,
	height=5,
	width=5,
	slanty=8,
	pace=mfpace
}

a_trees.register
{
	trunk="default:pine_tree",
	leaves="default:pine_needles",
	branchy=1,
	height=5,
	width=4,
	pace=mfpace
}

a_trees.register
{
	trunk="default:jungletree",
	leaves="default:jungleleaves",
	branchy=1,
	size=50,
	height=15,
	width=4,
	pace=mfpace
}

a_trees.register
{
	trunk="default:acacia_tree",
	leaves="default:acacia_leaves",
	branchy=1,
	height=2,
	width=10,
	pace=mfpace
}

--[[
a_trees.register
{
	trunk="default:desert_stone",
	leaves="default:sandstone",
	fruit="default:desert_stone_with_tin",
	branchy=3,
	size=15,
	height=5,
	width=10,
	pace=mfpace
}

a_trees.register
{
	trunk="default:sandstone",
	leaves="default:limestone",
	fruit="default:river_water_source",
	size=5,
	height=1,
	width=6,
	pace=mfpace
}
]]

a_trees.register
{
	trunk="default:cactus",
	leaves="air",
	branchy=3,
	size=15,
	height=5,
	width=10,
	pace=mfpace
}

a_trees.register
{
	trunk="default:pine_bush_stem",
	leaves="default:blueberry_bush_leaves_with_berries",
	fruit="default:blueberry_bush_leaves",
	fruity=50,
	size=5,
	height=2,
	width=5,
	pace=mfpace
}

--[[
a_trees.register
{
	trunk="abyss:rack",
	leaves="abyss:cobble",
	fruit="abyss:glowstone",
	fruity=50,
	pace=mfpace
}

a_trees.register
{
	trunk="default:pine_tree",
	leaves="mf_decor:slate_cobble",
	fruit="mf_decor:slate",
	size=5,
	height=1,
	width=6,
	pace=mfpace
}

]]
a_trees.register
{
	trunk="default:coral_skeleton",
	leaves="default:coral2",
	size=2,
	height=1,
	width=6,
	pace=mfpace
}
a_trees.register
{
	trunk="bigshroom:body",
	leaves="bigshroom:top",
	size=5,
	height=1,
	width=6,
	pace=mfpace
}
