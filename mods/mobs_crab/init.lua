mobs:register_arrow("mobs_crab:tp_arrow", {
	visual = "sprite",
	visual_size = {x = 1, y = 1},
	textures = {"magic_wand_noogberry.png"},
	velocity = 6,
	tail=1,
	tail_texture="magic_wand_noogberry.png",
	tail_size=2,
	glow=7,
	expire=0.05,

	hit_player = function(self, player)
		-- if player==self.owner_id then return end
		player:punch(self.object, 1.0, {
			full_punch_interval = 0.1,
			damage_groups = {fleshy = 2},
		})
	end,

	hit_mob = function(self, player)
		if not self.owner_id then return end
		player:punch(self.object, 1.0, {
			full_punch_interval = 0.1,
			damage_groups = {fleshy = 2},
		})
		--[[
		local target=player:get_pos()
		local owner=self.owner_id:get_pos()
		if target and owner then
			self.owner_id:move_to(target, true)
			player:move_to(owner, true)
			player:get_luaentity().state="stand"
		end
		--]]
	end,

	hit_node = function(self, pos, node)
		--if node.name=="ignore" then return end
		local target=minetest.find_node_near(self.lastpos or pos, 1, { "air", "group:liquid" }, true)
		if target then
			local owner=self.owner_id
			if owner and owner:get_hp() then
				owner:move_to(target, true)
			end
		end

		self.object:remove()
	end,
})
mobs:register_mob("mobs_crab:cavecrab", {
	type = "monster",
	suicidal=true,
	passive = false,
	reach = 2.25,
	damage = 2,
	attack_type="shoot", -- attack_type = "dogfight",
	arrow="mobs_crab:tp_arrow",
	shoot_interval=2,
	shoot_offset=-0.5,
	hp_max = 40,
	collisionbox = {-0.4,-0.1,-0.4, 0.4,1,0.4},
	visual = "mesh",
	-- visual_size=
	mesh = "cavecrab.b3d",
	textures = {
		{"mobs_cavecrab.png"},
	},
	makes_footstep_sound = true,
	walk_velocity = 1.5,
	walk_chance=10,
	run_velocity = 3.0,
	jump = true,
	jump_height=6,
	floats=0,
	water_damage = -0.5,
	fear_height=0,
	view_range = 16,
	drops = {
		{name = "default:mese_crystal_fragment", chance = 2},
		{name = "default:mese_crystal_fragment", chance = 2},
		{name = "more_mese:mese_crystal_fragment_grey", chance = 2},
		{name = "more_mese:mese_crystal_fragment_grey", chance = 2},
		{name = "default:coral_skeleton", chance=4},
	},
	animation = {
	--different stand animation from 1 to 15
		normal_speed = 20,
		run_speed = 25,
		stand_speed=1,
		stand_start = 45,
		stand_end = 65,
		walk_start = 20,
		walk_end = 40,
		run_start = 20,
		run_end = 40,
		punch_start = 20,
		punch_end = 40,
	},
	env_alt = {
		["default:marram_grass_3"]={ "default:palmtree_sapling", 5},
	},
})


mobs:spawn({
	name = "mobs_crab:cavecrab",
	nodes="default:water_source",
	neighbors = {"group:ore", "default:stone_with_coal", "dungeon_crates:crate", "anchorstone:anchorstone" },
	min_light = 0,
	max_light = 15,
	chance = 200,
	active_object_count = 2,
	max_height = 0,
	min_height = -60,
	interval=37,
})

mobs:register_mob("mobs_crab:rockcrab", {
	type = "monster",
	suicidal=true,
	passive = false,
	reach = 2.25,
	damage = 2,
	attack_type="shoot",
	arrow="mob_wasp:sting_smoke",
	shoot_interval=2.5,
	shoot_offset=-0.5,
	hp_max = 30,
	collisionbox = {-0.4,-0.1,-0.4, 0.4,1,0.4},
	visual = "mesh",
	-- visual_size=
	mesh = "cavecrab.b3d",
	textures = {
		{"mobs_cavecrab.png^[colorizehsl:0:0:-50"},
	},
	makes_footstep_sound = true,
	walk_velocity = 1.5,
	walk_chance=10,
	run_velocity = 2.0,
	jump = true,
	jump_height=6,
	floats=0,
	env_dmg= {["smoke:smog"]=-0.5, ["mobs:cobweb"]=0 },
	water_damage = 0,
	fear_height=0,
	view_range = 16,
	drops = {
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "default:copper_lump", chance=2},
		{name = "default:tin_lump", chance=4},
		{name = "default:iron_lump", chance=8},
		{name = "default:gold_lump", chance=16},
	},
	animation = {
	--different stand animation from 1 to 15
		normal_speed = 20,
		run_speed = 25,
		stand_speed=1,
		stand_start = 45,
		stand_end = 65,
		walk_start = 20,
		walk_end = 40,
		run_start = 20,
		run_end = 40,
		punch_start = 20,
		punch_end = 40,
	},
	on_die=function(self, pos) minetest.after(0.1, function(pos) mobs:explosion(pos, 3) end, pos) end
})

mobs:spawn({
	name = "mobs_crab:rockcrab",
	nodes = {"default:stone_with_coal"},
	min_light = 0,
	max_light = 2,
	chance = 8,
	active_object_count = 3,
	min_height = -999,
	max_height = 0,
	interval=37,
})

