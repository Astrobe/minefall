This is a MODIFIED version of the bones mod. It adds a per-player "bones"
mode.

Copyright: Astrobe.
License: same as original.
Orginal README below.

---

Minetest Game mod: bones
========================
See license.txt for license information.

Authors of source code
----------------------
Originally by PilzAdam (MIT)
Various Minetest developers and contributors (MIT)

Authors of media (textures)
---------------------------
All textures: paramat (CC BY-SA 3.0)
