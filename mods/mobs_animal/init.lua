
local S = mobs.intllib


-- Kitten by Jordach / BFD

mobs:register_mob("mob_cats:kitten", {
	type = "npc",
	passive = false,
	attacks_monsters=true,
	damage = 2,
	reach=2,
	attack_type = "dogfight",
	hp_min = 20,
	hp_max = 30,
	collisionbox = {-0.3, -0.3, -0.3, 0.3, 0.1, 0.3},
	visual = "mesh",
	visual_size = {x = 0.5, y = 0.5},
	mesh = "mobs_kitten.b3d",
	textures = {
		{"mobs_kitten_striped.png"},
		{"mobs_kitten_splotchy.png"},
		{"mobs_kitten_ginger.png"},
		{"mobs_kitten_sandy.png"},
	},
	makes_footstep_sound = false,
	sounds = {
		random = "mobs_kitten",
		distance=3,
	},
	walk_velocity = 0.6,
	run_velocity = 3,
	runaway = true,
	jump = true,
	jump_height=0.01,
	stepheight=0.6,
	walk_chance=10,
	fear_height = 1,
	animation = {
		speed_normal = 42,
		stand_start = 97,
		stand_end = 192,
		walk_start = 0,
		walk_end = 96,
	},
	view_range = 20,
	on_rightclick = function(self, clicker) mobs:capture_mob(self, clicker, 90, 95, 99, true, nil) end,
})


mobs:register_egg("mob_cats:kitten", "Kitten", "mobs_kitten_inv.png", 0)

