-- mods/default/nodes.lua


--[[ Node name convention:

Although many node names are in combined-word form, the required form for new
node names is words separated by underscores. If both forms are used in written
language (for example pinewood and pine wood) the underscore form should be used.

--]]


--[[ Index:

Stone
-----
(1. Material 2. Cobble variant 3. Brick variant 4. Modified forms)

default:stone
default:cobble
default:stonebrick
default:stone_block
default:mossycobble

default:desert_stone
default:desert_cobble
default:desert_stonebrick
default:desert_stone_block

default:sandstone
default:sandstonebrick
default:sandstone_block
default:desert_sandstone
default:desert_sandstone_brick
default:desert_sandstone_block

default:obsidian
default:obsidianbrick
default:obsidian_block

Soft / Non-Stone
----------------
(1. Material 2. Modified forms)

default:dirt
default:dirt_with_grass
default:dirt_with_grass_footsteps
default:dirt_with_dry_grass
default:dirt_with_snow
default:dirt_with_rainforest_litter
default:dirt_with_coniferous_litter

default:permafrost
default:permafrost_with_stones
default:permafrost_with_moss

default:sand
default:desert_sand

default:gravel

default:clay

default:snow
default:snowblock
default:ice
default:cave_ice

Trees
-----
(1. Trunk 2. Fabricated trunk 3. Leaves 4. Sapling 5. Fruits)

default:tree
default:wood
default:leaves
default:sapling
default:apple

default:jungletree
default:junglewood
default:jungleleaves
default:junglesapling
default:emergent_jungle_sapling

default:pine_tree
default:pine_wood
default:pine_needles
default:pine_sapling

default:acacia_tree
default:acacia_wood
default:acacia_leaves
default:acacia_sapling

default:aspen_tree
default:aspen_wood
default:aspen_leaves
default:aspen_sapling

Ores
----
(1. In stone 2. Blocks)

default:stone_with_coal
default:coalblock

default:stone_with_iron
default:steelblock

default:stone_with_copper
default:copperblock

default:stone_with_tin
default:tinblock

default:bronzeblock

default:stone_with_gold
default:goldblock

default:stone_with_mese
default:mese

default:stone_with_diamond
default:diamondblock

Plantlife
---------

default:cactus
default:large_cactus_seedling

default:papyrus
default:dry_shrub
default:junglegrass

default:grass_1
default:grass_2
default:grass_3
default:grass_4
default:grass_5

default:dry_grass_1
default:dry_grass_2
default:dry_grass_3
default:dry_grass_4
default:dry_grass_5

default:fern_1
default:fern_2
default:fern_3

default:marram_grass_1
default:marram_grass_2
default:marram_grass_3

default:bush_stem
default:bush_leaves
default:bush_sapling
default:acacia_bush_stem
default:acacia_bush_leaves
default:acacia_bush_sapling
default:pine_bush_stem
default:pine_bush_needles
default:pine_bush_sapling
default:blueberry_bush_leaves_with_berries
default:blueberry_bush_leaves
default:blueberry_bush_sapling

default:sand_with_kelp

Corals
------

default:coral_brown
default:coral_orange
default:coral_skeleton

Liquids
-------
(1. Source 2. Flowing)

default:water_source
default:water_flowing

default:river_water_source
default:river_water_flowing

default:lava_source
default:lava_flowing

Tools / "Advanced" crafting / Non-"natural"
-------------------------------------------

default:bookshelf

default:sign_wall_wood
default:sign_wall_steel

default:ladder_wood
default:ladder_steel

default:fence_wood
default:fence_acacia_wood
default:fence_junglewood
default:fence_pine_wood
default:fence_aspen_wood

default:glass
default:obsidian_glass

default:brick

default:meselamp
default:mese_post_light

Misc
----

default:cloud

--]]

--
-- Stone
--
minetest.register_node(":default:limestone", {
	description = "Limestone",
	-- tiles = {"mapgen_limestone.png^[multiply:antiquewhite"},
	-- tiles = {"default_coral_skeleton.png"},
	tiles = {"default_silver_sandstone.png^[multiply:antiquewhite"},
	groups = {cracky = 3, stone = 1},
	sounds = default.node_sound_stone_defaults(),
	drop = "", -- 'stairs:stair_limestone 2',
})

minetest.register_node(":default:stone", {
	description = "Stone",
	tiles = {"default_stone.png"},
	groups = {cracky = 2, stone = 1, level=1},
	drop = "",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:cobble", {
	description = "Cobblestone",
	tiles = {"default_cobble.png"},
	is_ground_content = false,
	groups = {cracky = 3, yatpm = 1 },
	sounds = default.node_sound_stone_defaults(),
	drop="stairs:stair_cobble 4",
})

minetest.register_node(":default:stonebrick", {
	description = "Stone Brick",
	paramtype2 = "facedir",
	tiles = {"default_stone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2, level=4},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:stone_block", {
	description = "Stone Block",
	tiles = {"default_stone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, level=4},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:silver_sandstonebrick", {
	description = "Limestone Brick",
	paramtype2 = "facedir",
	tiles = {"default_silver_sandstone_brick.png^[multiply:antiquewhite"},
	is_ground_content = false,
	groups = {cracky = 2, level=4},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:silver_sandstone_block", {
	description = "Limestone Block",
	tiles = {"default_silver_sandstone_block.png^[multiply:antiquewhite"},
	is_ground_content = false,
	groups = {cracky = 2, level=4},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:mossycobble", {
	description = "Mossy Cobblestone",
	tiles = {"default_mossycobble.png"},
	is_ground_content = false,
	groups = {cracky = 3, yatpm = 1, slippery=1},
	sounds = default.node_sound_stone_defaults(),
	light_source=2,
	drop="stairs:stair_mossycobble 4",
})


minetest.register_node(":default:desert_stone", {
	description = "Desert Stone",
	tiles = {"default_desert_stone.png"},
	groups = {cracky = 2, stone = 1, level=1},
	drop = "",
	legacy_mineral = true,
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:desert_cobble", {
	description = "Desert Cobblestone",
	tiles = {"default_desert_cobble.png"},
	is_ground_content = false,
	groups = {cracky = 3, yatpm = 1, },
	sounds = default.node_sound_stone_defaults(),
	drop="stairs:stair_desert_cobble 4",
})

minetest.register_node(":default:desert_stonebrick", {
	description = "Desert Stone Brick",
	paramtype2 = "facedir",
	tiles = {"default_desert_stone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2, level=4},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:desert_stone_block", {
	description = "Desert Stone Block",
	tiles = {"default_desert_stone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, level=4},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:sandstone", {
	description = "Sandstone",
	tiles = {"default_sandstone.png"},
	groups = { cracky = 2},
	drop = "stairs:stair_sandstone 4",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:sandstonebrick", {
	description = "Sandstone Brick",
	paramtype2 = "facedir",
	tiles = {"default_sandstone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2, level=4},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:sandstone_block", {
	description = "Sandstone Block",
	tiles = {"default_sandstone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, level=4},
	sounds = default.node_sound_stone_defaults(),
})

--[[
minetest.register_node(":default:desert_sandstone", {
	description = "Desert Sandstone",
	tiles = {"default_desert_sandstone.png"},
	groups = { cracky = 3},
	sounds = default.node_sound_stone_defaults(),
	drop="",
})
]]

minetest.register_node(":default:desert_sandstone_brick", {
	description = "Desert Sandstone Brick",
	paramtype2 = "facedir",
	tiles = {"default_desert_sandstone_brick.png"},
	is_ground_content = false,
	groups = {cracky = 2, level=4},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:desert_sandstone_block", {
	description = "Desert Sandstone Block",
	tiles = {"default_desert_sandstone_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, level=4},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:obsidian", {
	description = "Obsidian\nGet: cool lava with water or ice.\nLava is found underground (-100 m)",
	tiles = {"default_obsidian.png"},
	sounds = default.node_sound_stone_defaults(),
	groups = {cracky = 1, level = 2},
})

minetest.register_node(":default:obsidianbrick", {
	description = "Obsidian Brick",
	paramtype2 = "facedir",
	tiles = {"default_obsidian_brick.png"},
	is_ground_content = false,
	sounds = default.node_sound_stone_defaults(),
	groups = {cracky = 1, level = 4},
})

minetest.register_node(":default:obsidian_block", {
	description = "Obsidian Block",
	tiles = {"default_obsidian_block.png"},
	is_ground_content = false,
	sounds = default.node_sound_stone_defaults(),
	groups = {cracky = 1, level = 4, },
})

--
-- Soft / Non-Stone
--

minetest.register_node(":default:dirt", {
	description = "Dirt",
	tiles = {"default_dirt.png"},
	groups = {crumbly = 1, falling_node = 1,soil = 1, level=1},
	damage_per_second = 1,
	sounds = default.node_sound_dirt_defaults(),
	paramtype2="facedir",
	--floodable=true,
	--on_flood=function(pos) minetest.set_node(pos, {name="default:gravel"}) return true end,

})

minetest.register_node(":default:dry_dirt", {
	description = "Dry dirt",
	tiles = {"default_dry_dirt.png"},
	groups = {crumbly = 1, falling_node = 1,soil = 1, spreading_dirt_type = 1, level=1},
	damage_per_second = 1,
	sounds = default.node_sound_dirt_defaults(),
	paramtype2="facedir",
	drop="default:dirt",
})

minetest.register_node(":default:dirt_with_grass", {
	description = "Dirt with Grass",
	tiles = {"default_grass.png"},
	groups = {crumbly = 1, soil = 1, falling_node = 1,spreading_dirt_type = 1, level=1},
	drop = 'default:dirt',
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.25},
	}),
	damage_per_second = 1,
	paramtype2="facedir",
})

minetest.register_node(":default:dirt_with_dry_grass", {
	description = "Dirt with Dry Grass",
	tiles = {"default_dry_grass.png"},
	groups = {crumbly = 1, soil = 1, falling_node = 1,spreading_dirt_type = 1, level=1},
	drop = 'default:dirt',
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.4},
	}),
	damage_per_second = 1,
	paramtype2="facedir",
	on_burn=function(pos) minetest.swap_node(pos, {name="default:dirt"}) end,
})

minetest.register_node(":default:dirt_with_snow", {
	description = "Dirt with Snow",
	tiles = {"default_snow.png"},
	groups = {crumbly = 1, soil = 1, falling_node = 1,spreading_dirt_type = 1, snowy = 1, slippery=1, level=1},
	drop = 'default:dirt',
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_snow_footstep", gain = 0.2},
	}),
	damage_per_second = 1,
	paramtype2="facedir",
})

minetest.register_node(":default:dirt_with_rainforest_litter", {
	description = "Dirt with Rainforest Litter",
	tiles = { "default_rainforest_litter.png"},
	groups = {crumbly = 1, soil = 1, falling_node = 1,spreading_dirt_type = 1, level=1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.4},
	}),
	damage_per_second = 1,
})

minetest.register_node(":default:dirt_with_coniferous_litter", {
	description = "Dirt with Coniferous Litter",
	tiles = { "default_coniferous_litter.png"},
	groups = {crumbly = 1, soil = 1, falling_node = 1,spreading_dirt_type = 1, level=1},
	drop = "default:dirt",
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.4},
	}),
	damage_per_second = 1,
	paramtype2="facedir",
})

minetest.register_node(":default:permafrost", {
	description = "Permafrost",
	tiles = {"default_permafrost.png"},
	groups = {crumbly = 1, level=2},
	sounds = default.node_sound_dirt_defaults(),
	drop="default:dirt",
})

minetest.register_node(":default:sand", {
	description = "Sand",
	tiles = {"default_sand.png"},
	groups = {crumbly = 3, falling_node = 1, sand = 1, fall_damage_add_percent=-10},
	sounds = default.node_sound_sand_defaults(),
	damage_per_second = 1,
	paramtype2="facedir",
})

minetest.register_node(":default:desert_sand", {
	description = "Desert Sand",
	tiles = {"default_desert_sand.png"},
	groups = {crumbly = 3, falling_node = 1, sand = 1, fall_damage_add_percent=-10},
	sounds = default.node_sound_sand_defaults(),
	damage_per_second = 1,
	paramtype2="facedir",
})

minetest.register_node(":default:gravel", {
	description = "Dirt with stones",
	-- tiles = {"default_stone.png^[transformR90^default_stones.png^(default_stones.png^[transformFYR90)"},
	tiles={"default_gravel.png"},
	groups = {crumbly = 2, falling_node = 1, slippery=2},
	sounds = default.node_sound_gravel_defaults(),
	damage_per_second = 1,
	paramtype2="facedir",
})

minetest.register_node(":default:snowblock", {
	description = "Snow Block",
	tiles = {"default_snow.png"},
	groups = {crumbly = 3, cools_lava = 1, snowy = 1, fall_damage_add_percent=-20, falling_node=1, slippery=1},
	sounds = default.node_sound_snow_defaults(),
	-- damage_per_second = 1, -- dps would prevent whales from "eating" snow when stuck in them 
	paramtype2="facedir",
})

minetest.register_node(":default:ice", {
	description = "Ice",
	drawtype="glasslike",
	post_effect_color={a=128, r=0, g=0, b=255},
	tiles = {"default_ice.png"},
	is_ground_content = false,
	paramtype = "light",
	groups = {cracky = 3, cools_lava = 1, slippery = 3},
	light_source=1,
	sounds = default.node_sound_ice_defaults(),
	damage_per_second = 1,
	--drop="stairs:stair_ice",
})

minetest.register_node(":default:cave_ice", {
	description = "Cave Ice",
	tiles = {"default_ice.png"},
	paramtype = "light",
	light_source=1,
	groups = {cracky = 3, cools_lava = 1, slippery = 3 },
	drop="default:ice",
	sounds = default.node_sound_ice_defaults(),
})

--
-- Trees
--

minetest.register_node(":default:tree", {
	description = "Apple Tree",
	tiles = {"default_tree_top.png", "default_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree = 1, yatpm=1, choppy = 1,  flammable = 2, level=1},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node(":default:wood", {
	description = "Apple Wood Planks",
	tiles = {"default_wood.png"},
	is_ground_content = false,
	groups = {choppy = 2, flammable = 2, wood = 1},
	sounds = default.node_sound_wood_defaults(),
	drop="stairs:stair_wood 4",
})

minetest.register_node(":default:sapling", {
	description = "Apple Tree Sapling",
	drawtype = "plantlike",
	floodable=true,
	tiles = {"default_sapling.png"},
	inventory_image = "default_sapling.png",
	wield_image = "default_sapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = default.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 7 / 16, 4 / 16}
	},
	groups = {snappy = 3, flammable = 2, attached_node = 1, sapling = 1, treesapling=1},
	sounds = default.node_sound_leaves_defaults(),

	on_construct = function(pos)
		default.start_grow_timer(6000, pos, "deciduous_forest")
	end,
})

minetest.register_node(":default:leaves", {
	description = "Apple Tree Leaves",
	drawtype = "allfaces",
	waving = 1,
	tiles = {"default_leaves.png"},
	inventory_image="default_leaves.png",
	--special_tiles = {"default_leaves_simple.png"},
	post_effect_color={a=192, r=0, g=64, b=0},
	paramtype = "light",
	is_ground_content = false,
	walkable=false,
	groups = {snappy = 2, leafdecay_drop = 3, flammable = 2, leaves = 1, level=1},
	--move_resistance=1,
	sounds = default.node_sound_leaves_defaults(),

	after_place_node = default.after_place_leaves,
	floodable=true,
})

minetest.register_node(":default:apple", {
	description = "Apple",
	drawtype = "plantlike",
	paramtype2="meshoptions",
	param2=0;
	tiles = {"default_apple.png^default_leaves.png"},
	--special_tiles = {"default_apple.png^default_leaves_simple.png"},
	post_effect_color={a=192, r=0, g=64, b=0},
	inventory_image = "default_apple.png",
	wield_image = "default_apple.png",
	waving = 0,
	floodable=true,
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	is_ground_content = false,
	place_param2=1,
	groups = {snappy = 3, flammable = 2, leaves = 1, dig_immediate = 3, leafdecay = 3, leafdecay_drop = 1, food_apple = 1, heal=2},
	--move_resistance=1,
	on_use = minetest.item_eat(2),
	sounds = default.node_sound_leaves_defaults(),

	after_dig_node = function(pos, oldnode, oldmetadata, digger)
		if oldnode.param2 == 0 and math.random()> 0.25 then
			minetest.set_node(pos, {name = "default:apple_mark"})
			default.start_grow_timer(6000, pos, "deciduous_forest")
		end
	end,
	node_dig_prediction="default:apple_mark",
})

minetest.register_node(":default:apple_mark", {
	description = "Apple Marker",
	drawtype = "plantlike",
	waving = 0,
	tiles = {"default_leaves.png"},
	--special_tiles = {"default_leaves_simple.png"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	drop = 'default:leaves',
	groups = {snappy = 2, leaves = 1},
	on_timer = function(pos, elapsed) 
		if minetest.find_node_near(pos, 1, "default:leaves") then
			minetest.set_node(pos, {name = "default:apple"})
		else
			minetest.remove_node(pos)
		end
	end,
	floodable=true,
})

minetest.register_node(":default:palm_tree", {
	description = "Palm Tree",
	tiles = {
		"mapgen_palmtree_top.png",
		"mapgen_palmtree_side.png",
	},
	groups = {choppy = 1, tree = 1, yatpm=1, flammable = 1, level=1},
	sounds = default.node_sound_wood_defaults(),
	paramtype2 = "facedir",
	on_place = minetest.rotate_node
})

minetest.register_node(":default:palm_wood", {
	description = "Palm Wood Planks",
	tiles = {
		"mapgen_palm_wood.png",
	},
	groups = {choppy = 2, flammable = 1, wood=1},
	sounds = default.node_sound_wood_defaults(),
	drop="stairs:stair_palm_wood 4",
})


minetest.register_node(":default:palm_leaves", {
	description = "Palm Leaves",
	drawtype = "allfaces",
	tiles = {"mapgen_palmleaves.png"},
	inventory_image="mapgen_palmleaves.png",
	--special_tiles = {"mapgen_palmleaves_simple.png",},
	post_effect_color={a=192, r=0, g=64, b=0},
	waving = 2,
	walkable=false,
	paramtype = "light",
	--move_resistance=1,
	groups = {snappy = 2, flammable = 1, leaves = 1, leafdecay_drop = 1, level=1},
	sounds = default.node_sound_leaves_defaults()
})

minetest.register_node(":default:giantgrass", {
	description = "Giant Grass",
	drawtype = "plantlike",
	floodable=true,
	waving = 1,
	tiles = {"mapgen_giantgrass.png"},
	paramtype = "light",
	is_ground_content = false,
	buildable_to = true, 
	sunlight_propagates = true,
	inventory_image = "mapgen_giantgrass.png",
	visual_scale = 4,
	groups = {snappy=3, flammable=1, attached_node=1, flora=1, grass=1},
	sounds = default.node_sound_leaves_defaults(),
	drop='',
	walkable = false,
	move_resistance=1,
})



minetest.register_node(":default:jungletree", {
	description = "Jungle Tree",
	tiles = {
		"default_jungletree_top.png",
		"default_jungletree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree = 1, yatpm=1, choppy = 1,  flammable = 2, level=1},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node(":default:junglewood", {
	description = "Jungle Wood Planks",
	tiles = {"default_junglewood.png"},
	is_ground_content = false,
	groups = {choppy = 2, flammable = 2, wood = 1},
	sounds = default.node_sound_wood_defaults(),
	drop="stairs:stair_junglewood 4",
})

minetest.register_node(":default:jungleleaves", {
	description = "Jungle Tree Leaves",
	drawtype = "allfaces",
	waving = 1,
	tiles = {"default_jungleleaves.png"},
	inventory_image="default_jungleleaves.png",
	--special_tiles = {"default_jungleleaves_simple.png"},
	post_effect_color={a=192, r=0, g=64, b=0},
	paramtype = "light",
	is_ground_content = false,
	walkable=false,
	move_resistance=1,
	groups = {snappy = 2, leafdecay_drop = 3, flammable = 2, leaves = 1, level=1},
	sounds = default.node_sound_leaves_defaults(),

	after_place_node = default.after_place_leaves,
})

minetest.register_node(":default:junglesapling", {
	description = "Jungle Tree Sapling",
	drawtype = "plantlike",
	floodable=true,
	tiles = {"default_junglesapling.png"},
	inventory_image = "default_junglesapling.png",
	wield_image = "default_junglesapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = default.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 7 / 16, 4 / 16}
	},
	groups = {snappy = 3, flammable = 2, attached_node = 1, sapling = 1, treesapling=1},
	sounds = default.node_sound_leaves_defaults(),

	on_construct = function(pos)
		default.start_grow_timer(6000, pos, "rainforest")
	end,

})

minetest.register_node(":default:pine_tree", {
	description = "Pine Tree",
	tiles = {
		"default_pine_tree_top.png",
		"default_pine_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree = 1, yatpm=1, choppy = 1,  flammable = 3, level=1},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node(":default:pine_wood", {
	description = "Pine Wood Planks",
	tiles = {"default_pine_wood.png"},
	is_ground_content = false,
	groups = {choppy = 2, flammable = 3, wood = 1},
	sounds = default.node_sound_wood_defaults(),
	drop="stairs:stair_pine_wood 4",
})

minetest.register_node(":default:pine_needles",{
	description = "Pine Needles",
	drawtype = "allfaces",
	tiles = {"mf_pine_needles.png"},
	post_effect_color={a=192, r=0, g=64, b=0},
	waving = 1,
	paramtype = "light",
	is_ground_content = false,
	walkable=false,
	--move_resistance=1,
	groups = {snappy = 2, leafdecay_drop = 3, flammable = 2, leaves = 1, level=1},
	sounds = default.node_sound_leaves_defaults(),

	after_place_node = default.after_place_leaves,
})

minetest.register_node(":default:pine_sapling", {
	description = "Pine Tree Sapling",
	drawtype = "plantlike",
	floodable=true,
	tiles = {"default_pine_sapling.png"},
	inventory_image = "default_pine_sapling.png",
	wield_image = "default_pine_sapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = default.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 7 / 16, 4 / 16}
	},
	groups = {snappy = 3, flammable = 3, attached_node = 1, sapling = 1, treesapling=1},
	sounds = default.node_sound_leaves_defaults(),

	on_construct = function(pos)
		default.start_grow_timer(6000, pos, "coniferous_forest") 
	end,

})


minetest.register_node(":default:acacia_tree", {
	description = "Acacia Tree",
	tiles = {
		"default_acacia_tree_top.png",
		"default_acacia_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree = 1, yatpm=1, choppy = 1,  flammable = 2, level=1},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node(":default:acacia_wood", {
	description = "Acacia Wood Planks",
	tiles = {"default_acacia_wood.png"},
	is_ground_content = false,
	groups = {choppy = 2, flammable = 2, wood = 1},
	sounds = default.node_sound_wood_defaults(),
	drop="stairs:stair_acacia_wood 4",
})

minetest.register_node(":default:acacia_leaves", {
	description = "Acacia Tree Leaves",
	drawtype = "allfaces",
	tiles = {"default_acacia_leaves_simple.png"},
	inventory_image="default_acacia_leaves_simple.png",
	--special_tiles = {"default_acacia_leaves_simple.png"},
	post_effect_color={a=192, r=0, g=64, b=0},
	waving = 1,
	paramtype = "light",
	is_ground_content = false,
	walkable=false,
	--move_resistance=1,
	groups = {snappy = 2, leafdecay_drop = 3, flammable = 2, leaves = 1, level=1},
	sounds = default.node_sound_leaves_defaults(),

	after_place_node = default.after_place_leaves,
})

minetest.register_node(":default:acacia_sapling", {
	description = "Acacia Tree Sapling",
	drawtype = "plantlike",
	floodable=true,
	tiles = {"default_acacia_sapling.png"},
	inventory_image = "default_acacia_sapling.png",
	wield_image = "default_acacia_sapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = default.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 7 / 16, 4 / 16}
	},
	groups = {snappy = 3, flammable = 2, attached_node = 1, sapling = 1, treesapling=1},
	sounds = default.node_sound_leaves_defaults(),

	on_construct = function(pos)
		default.start_grow_timer(6000, pos, "savanna")
	end,

})

minetest.register_node(":default:aspen_tree", {
	description = "Aspen Tree",
	tiles = {
		"default_aspen_tree_top.png",
		"default_aspen_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree = 1, yatpm=1, choppy = 1,  flammable = 3, level=1},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node(":default:aspen_wood", {
	description = "Aspen Wood Planks",
	tiles = {"default_aspen_wood.png"},
	is_ground_content = false,
	groups = {choppy = 2, flammable = 3, wood = 1},
	sounds = default.node_sound_wood_defaults(),
	drop="stairs:stair_aspen_wood 4",
})

minetest.register_node(":default:aspen_leaves", {
	description = "Aspen Tree Leaves",
	drawtype = "allfaces",
	tiles = {"default_aspen_leaves.png"},
	inventory_image="default_aspen_leaves.png",
	post_effect_color={a=192, r=0, g=64, b=0},
	waving = 1,
	paramtype = "light",
	is_ground_content = false,
	walkable=false,
	--move_resistance=1,
	groups = {snappy = 2, leafdecay_drop = 3, flammable = 2, leaves = 1, level=1},
	sounds = default.node_sound_leaves_defaults(),

	after_place_node = default.after_place_leaves,
})

minetest.register_node(":default:aspen_sapling", {
	description = "Aspen Tree Sapling",
	drawtype = "plantlike",
	floodable=true,
	tiles = {"default_aspen_sapling.png"},
	inventory_image = "default_aspen_sapling.png",
	wield_image = "default_aspen_sapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = default.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-3 / 16, -0.5, -3 / 16, 3 / 16, 0.5, 3 / 16}
	},
	groups = {snappy = 3, flammable = 3, attached_node = 1, sapling = 1, treesapling=1},
	sounds = default.node_sound_leaves_defaults(),

	on_construct = function(pos)
		default.start_grow_timer(6000, pos, "swamp") 
	end,

})
minetest.register_node(":default:palmtree_sapling", {
	description = "Palm Tree Sapling",
	drawtype = "plantlike",
	floodable=true,
	tiles = {"mapgen_palmtree_sapling.png"},
	inventory_image = "mapgen_palmtree_sapling.png",
	wield_image = "mapgen_palmtree_sapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = default.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-3 / 16, -0.5, -3 / 16, 3 / 16, 0.5, 3 / 16}
	},
	groups = {snappy = 3, flammable = 3, attached_node = 1, sapling = 1, treesapling=1},
	sounds = default.node_sound_leaves_defaults(),

	on_construct = function(pos)
		default.start_grow_timer(6000, pos, "savanna") 
	end,

})

--
-- Ores
--

minetest.register_node(":default:stone_with_coal", {
	description = "Coal Ore",
	tiles = {"default_stone.png^default_mineral_coal.png"},
	groups = {cracky = 3, flammable=3, stone=1},
	drop = "default:coal_lump",
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:steelblock", {
	description = "Steel Block",
	tiles = {"default_steel_block.png"},
	is_ground_content = false,
	groups = {cracky = 1, level=2, protected=1 },
	sounds = default.node_sound_metal_defaults(),
})


minetest.register_node(":default:copperblock", {
	description = "Copper Block",
	tiles = {"default_copper_block.png"},
	is_ground_content = false,
	groups = {cracky = 1, level=2, protected=1},
	sounds = default.node_sound_metal_defaults(),
})

-- "_with_tin" is of course a misnomer. I changed this stuff several times and
-- never botherd to change the internal name
minetest.register_node(":default:stone_with_tin", {
	description = "Stone with ore",
	tiles = {"default_stone.png^default_mineral_iron.png"},
	groups = {cracky = 2, ore=1, stone=1},
	drop = {
		max_items = 1,
		items = {
			{items = {'default:copper_lump'}, rarity = 2},
			{items = {"default:tin_lump"}, rarity = 2},
			{items = {"default:iron_lump"}, rarity = 2},
			{items = {"default:gold_lump"}, rarity = 2},
			{items = {'default:copper_lump'}, rarity = 1}, -- fallback
		}
	},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:desert_stone_with_tin", {
	description = "Desert stone with ore",
	tiles = {"default_desert_stone.png^default_mineral_iron.png"},
	groups = {cracky = 2, ore=1, stone=1},
	drop = {
		max_items = 1,
		items = {
			{items = {"default:iron_lump"}, rarity = 2},
			{items = {'default:copper_lump'}, rarity = 2},
			{items = {"default:tin_lump"}, rarity = 2},
			{items = {"default:gold_lump"}, rarity = 2},
			{items = {'default:iron_lump'}, rarity = 1}, -- fallback
		}
	},
	sounds = default.node_sound_stone_defaults(),
})


minetest.register_node(":default:tinblock", {
	description = "Tin Block",
	tiles = {"default_tin_block.png"},
	is_ground_content = false,
	groups = {cracky = 1, level=2, protected=1},
	sounds = default.node_sound_metal_defaults(),
})


minetest.register_node(":default:bronzeblock", {
	description = "Bronze Block",
	tiles = {"default_bronze_block.png"},
	is_ground_content = false,
	groups = {cracky = 1, level=2, protected=1},
	sounds = default.node_sound_metal_defaults(),
})


minetest.register_node(":default:mese", {
	description = "Mese Block.\nCan be traded for a beacon.",
	tiles = {"default_mese_block.png"},
	paramtype = "light",
	groups = {cracky = 1, level = 2, protected=1},
	sounds = default.node_sound_stone_defaults(),
	light_source = 12,
})


minetest.register_node(":default:goldblock", {
	description = "Gold Block",
	tiles = {"default_gold_block.png"},
	is_ground_content = false,
	groups = {cracky = 1, level=2, protected=1},
	sounds = default.node_sound_metal_defaults(),
})
--
-- Plantlife (non-cubic)
--

minetest.register_node(":default:cactus", {
	description = "Cactus",
	tiles = {"default_cactus_side.png"},
	groups = {choppy = 1, tree=1, yatpm=1},
	sounds = default.node_sound_wood_defaults(),
})

minetest.register_node(":default:large_cactus_seedling", {
	description = "Large Cactus Seedling",
	drawtype = "plantlike",
	floodable=true,
	tiles = {"default_large_cactus_seedling.png"},
	inventory_image = "default_large_cactus_seedling.png",
	wield_image = "default_large_cactus_seedling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	selection_box = {
		type = "fixed",
		fixed = {
			-5 / 16, -0.5, -5 / 16,
			5 / 16, 0.5, 5 / 16
		}
	},
	groups = {snappy = 2, attached_node = 1, treesapling=1},
	sounds = default.node_sound_wood_defaults(),

	on_construct = function(pos)
		-- cactus grow slowly IRL.
		default.start_grow_timer(12000, pos, "desert")
	end,
	on_timer = default.grow_sapling,
})

minetest.register_node(":default:dry_shrub", {
	description = "Dry Shrub",
	drawtype = "plantlike",
	floodable=true,
	waving = 1,
	tiles = {"default_dry_shrub.png"},
	inventory_image = "default_dry_shrub.png",
	wield_image = "default_dry_shrub.png",
	paramtype = "light",
	paramtype2 = "meshoptions",
	place_param2 = 4,
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	groups = {snappy = 3, flammable = 3, attached_node = 1, flora=1},
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, 4 / 16, 6 / 16},
	},
	drop="",
})

minetest.register_node(":default:junglegrass", {
	description = "Jungle Grass",
	drawtype = "plantlike",
	floodable=true,
	waving = 1,
	visual_scale = 2,
	tiles = {"default_junglegrass.png"},
	inventory_image = "default_junglegrass.png",
	wield_image = "default_junglegrass.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	move_resistance=1,
	buildable_to = true,
	groups = {snappy = 3, flora = 1, attached_node = 1, flammable = 1},
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, 0, 6 / 16},
	},
	drop="",
})

minetest.register_node(":default:snow_grass_5", {
	description = "Frozen Grass",
	drawtype = "plantlike",
	tiles = {"mapgen_snow_grass_5.png"},
	paramtype = "light",
	is_ground_content = false,
	buildable_to = true, 
	sunlight_propagates = true,
	drop = "",
	inventory_image = "mapgen_snow_grass_5.png",
	groups = {snappy=3, attached_node=1, flora=1 },
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5}
	},
	floodable=true,
	walkable = false,
	light_source=1,
})


minetest.register_node(":default:grass_5", {
	description = "Grass",
	drawtype = "plantlike",
	floodable=true,
	waving = 1,
	tiles = {"default_grass_5.png"},
	inventory_image = "default_grass_5.png",
	wield_image = "default_grass_5.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	groups = {snappy = 3, flora = 1, attached_node = 1, grass = 1, flammable = 1},
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, -5 / 16, 6 / 16},
	},
	drop=""
})

minetest.register_node(":default:dry_grass_5", {
	description = "Dry Grass",
	drawtype = "plantlike",
	floodable=true,
	waving = 1,
	tiles = {"default_dry_grass_5.png"},
	inventory_image = "default_dry_grass_5.png",
	wield_image = "default_dry_grass_5.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	groups = {snappy = 3, flammable = 3, flora = 1,
		attached_node = 1, dry_grass = 1},
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, -3 / 16, 6 / 16},
	},
	drop="",
})



minetest.register_node(":default:fern_3", {
	description = "Fern",
	drawtype = "plantlike",
	floodable=true,
	visual_scale=2,
	waving = 1,
	tiles = {"default_fern_3.png"},
	inventory_image = "default_fern_3.png",
	wield_image = "default_fern_3.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	groups = {snappy = 3, flammable = 3, flora = 1, attached_node = 1},
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, -0.25, 6 / 16},
	},

	drop="",
})


minetest.register_node(":default:marram_grass_3", {
	description = "Marram Grass",
	drawtype = "plantlike",
	floodable=true,
	waving = 1,
	tiles = {"default_marram_grass_3.png"},
	inventory_image = "default_marram_grass_3.png",
	wield_image = "default_marram_grass_3.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	groups = {snappy = 3, flammable = 3, attached_node = 1, flora=1 },
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, -0.25, 6 / 16},
	},
	drop="",
})

default.register_fence(":default:bush_stem",
{
	description="Bush stem (\"stick\")",
	texture="default_tree.png",
	groups = {choppy = 2, flammable = 2, stick=1},
	sounds = default.node_sound_wood_defaults()
})

minetest.register_node(":default:bush_leaves", {
	description = "Bush Leaves",
	drawtype = "firelike",
	tiles = {"default_leaves_simple.png"},
	paramtype = "light",
	walkable=false,
	move_resistance=2,
	groups = {snappy = 2, flammable = 2, leaves = 1, leafdecay_drop=1},
	sounds = default.node_sound_leaves_defaults(),

	after_place_node = default.after_place_leaves,
})

minetest.register_node(":default:bush_sapling", {
	description = "Bush Sapling",
	drawtype = "plantlike",
	floodable=true,
	tiles = {"default_bush_sapling.png"},
	inventory_image = "default_bush_sapling.png",
	wield_image = "default_bush_sapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = default.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 2 / 16, 4 / 16}
	},
	groups = {snappy = 3, flammable = 2, attached_node = 1, sapling = 1},
	sounds = default.node_sound_leaves_defaults(),

	on_construct = function(pos)
		default.start_grow_timer(4000, pos, "deciduous_forest")
	end,

	--[[
	on_place = function(itemstack, placer, pointed_thing)
		itemstack = default.sapling_on_place(itemstack, placer, pointed_thing,
			"default:bush_sapling",
			-- minp, maxp to be checked, relative to sapling pos
			{x = -1, y = 0, z = -1},
			{x = 1, y = 1, z = 1},
			-- maximum interval of interior volume check
			2)

		return itemstack
	end,
	]]
})

default.register_fence(":default:bronze_fence",
{
	description="Bronze fence",
	texture="default_bronze_block.png",
	groups = {cracky = 2, protected=1},
	sounds = default.node_sound_metal_defaults()
})
default.register_fence(":default:tin_fence",
{
	description="Tin fence",
	texture="default_tin_block.png",
	groups = {cracky = 2, protected=1},
	sounds = default.node_sound_metal_defaults()
})
default.register_fence(":default:steel_fence",
{
	description="Steel fence",
	texture="default_steel_block.png",
	groups = {cracky = 2, protected=1},
	sounds = default.node_sound_metal_defaults()
})
default.register_fence(":default:gold_fence",
{
	description="Gold fence",
	texture="default_gold_block.png",
	groups = {cracky = 2, protected=1},
	sounds = default.node_sound_metal_defaults()
})

minetest.register_node(":default:blueberry_bush_leaves_with_berries", {
	description = "Blueberry Bush Leaves with Berries",
	drawtype = "allfaces",
	tiles = {"default_blueberry_bush_leaves.png^default_blueberry_overlay.png"},
	paramtype = "light",
	walkable=false,
	move_resistance=2,
	groups = {snappy = 3, flammable = 2, leaves = 1, dig_immediate = 3},
	drop = "default:blueberries",
	sounds = default.node_sound_leaves_defaults(),
	node_dig_prediction = "default:blueberry_bush_leaves",

	after_dig_node = function(pos, oldnode, oldmetadata, digger)
		minetest.set_node(pos, {name = "default:blueberry_bush_leaves"})
		if math.random()> 0.25 then
			default.start_grow_timer(6000, pos, "swamp")
		end
	end,
	floodable=true,
})

minetest.register_node(":default:blueberry_bush_leaves", {
	description = "Blueberry Bush Leaves",
	drawtype = "allfaces",
	tiles = {"default_blueberry_bush_leaves.png"},
	paramtype = "light",
	walkable=false,
	move_resistance=2,
	groups = {snappy = 3, flammable = 2, leaves = 1},
	sounds = default.node_sound_leaves_defaults(),

	on_timer = function(pos, elapsed)
		if minetest.get_node_light(pos) < 11 then
			minetest.get_node_timer(pos):start(200)
		else
			minetest.set_node(pos, {name = "default:blueberry_bush_leaves_with_berries"})
		end
	end,

	floodable=true,
})

minetest.register_node(":default:blueberry_bush_sapling", {
	description = "Blueberry Bush Sapling",
	drawtype = "plantlike",
	floodable=true,
	tiles = {"default_blueberry_bush_sapling.png"},
	inventory_image = "default_blueberry_bush_sapling.png",
	wield_image = "default_blueberry_bush_sapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = default.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 2 / 16, 4 / 16}
	},
	groups = {snappy = 3,  flammable = 2, attached_node = 1, sapling = 1},
	sounds = default.node_sound_leaves_defaults(),

	on_construct = function(pos)
		default.start_grow_timer(6000, pos, "deciduous_forest")
	end,

})

default.register_fence(":default:acacia_bush_stem",
{
	description="Acacia bush stem",
	texture="default_acacia_tree.png",
	groups = {choppy = 2, flammable = 2, stick=1},
	sounds = default.node_sound_wood_defaults()
})

minetest.register_node(":default:acacia_bush_leaves", {
	description = "Acacia Bush Leaves",
	drawtype = "firelike",
	waving = 1,
	tiles = {"default_acacia_leaves.png"},
	paramtype = "light",
	walkable=false,
	move_resistance=2,
	groups = {snappy = 2, flammable = 2, leaves = 1, leafdecay_drop=1},
	sounds = default.node_sound_leaves_defaults(),

	after_place_node = default.after_place_leaves,
	floodable=true,
})

minetest.register_node(":default:acacia_bush_sapling", {
	description = "Acacia Bush Sapling",
	drawtype = "plantlike",
	floodable=true,
	tiles = {"default_acacia_bush_sapling.png"},
	inventory_image = "default_acacia_bush_sapling.png",
	wield_image = "default_acacia_bush_sapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = default.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-3 / 16, -0.5, -3 / 16, 3 / 16, 2 / 16, 3 / 16}
	},
	groups = {snappy = 3, flammable = 2, attached_node = 1, sapling = 1},
	sounds = default.node_sound_leaves_defaults(),

	on_construct = function(pos)
		default.start_grow_timer(6000, pos, "savanna") 
	end,

})

default.register_fence(":default:pine_bush_stem",
{
	description="Pine bush stem",
	texture="default_pine_tree.png",
	groups = {choppy = 2, flammable = 2, stick=1},
	sounds = default.node_sound_wood_defaults()
})

minetest.register_node(":default:pine_bush_needles", {
	description = "Pine Bush Needles",
	drawtype = "firelike",
	waving = 1,
	tiles = {"mf_pine_needles.png"},
	paramtype = "light",
	walkable=false,
	move_resistance=2,
	groups = {snappy = 2, flammable = 2, leaves = 1, leafdecay_drop=1},
	sounds = default.node_sound_leaves_defaults(),

	after_place_node = default.after_place_leaves,
	floodable=true,
})

minetest.register_node(":default:pine_bush_sapling", {
	description = "Pine Bush Sapling",
	drawtype = "plantlike",
	floodable=true,
	tiles = {"default_pine_bush_sapling.png"},
	inventory_image = "default_pine_bush_sapling.png",
	wield_image = "default_pine_bush_sapling.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	on_timer = default.grow_sapling,
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, 2 / 16, 4 / 16}
	},
	groups = {snappy = 3, flammable = 2, attached_node = 1, sapling = 1},
	sounds = default.node_sound_leaves_defaults(),

	on_construct = function(pos)
		default.start_grow_timer(6000, pos, "coniferous_forest")
	end,

})

--
-- Corals
--

minetest.register_node(":default:coral", {
	description = "Coral flower",
	drawtype = "plantlike_rooted",
	waving = 1,
	paramtype = "light",
	tiles = {"default_coral_brown.png"},
	special_tiles = {{name = "default_coral_cyan.png", tileable_vertical = true}},
	inventory_image = "default_coral_cyan.png", -- could happen with obsidian picker.
	groups = {cracky = 3},
	drop = "default:coral_skeleton",
	sounds = default.node_sound_stone_defaults(),
	light_source=3,
})

minetest.register_node(":default:coral2", {
	description = "Coral",
	paramtype = "light",
	tiles = {"default_coral_brown.png"},
	groups = {cracky = 3},
	drop = "default:coral_skeleton",
	sounds = default.node_sound_stone_defaults(),
})


minetest.register_node(":default:coral_skeleton", {
	description = "Coral Skeleton",
	tiles = {"default_coral_skeleton.png"},
	groups = {cracky = 3},
	sounds = default.node_sound_stone_defaults(),
})


--
-- Liquids
--

minetest.register_node(":default:water_source", {
	description = "Water Source",
	drawtype = "liquid",
	tiles = {
		{
			name = "default_water_source_animated.png^[noalpha^[opacity:224",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
		{
			name = "default_water_source_animated.png^[noalpha^[opacity:224",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	use_texture_alpha="clip",
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = false,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_flowing = "default:water_flowing",
	liquid_alternative_source = "default:water_source",
	liquid_viscosity = 1,
	liquid_range = 1,
	post_effect_color = {a = 150, r = 30, g = 60, b = 90},
	groups = {water = 3, liquid = 3, cools_lava = 1, nopicker=1},
	sounds = default.node_sound_water_defaults(),
	light_source=1,
})

minetest.register_node(":default:water_flowing", {
	description = "Flowing Water",
	drawtype = "flowingliquid",
	tiles = {"default_water.png"},
	special_tiles = {
		{
			name = "default_water_flowing_animated.png^[noalpha^[opacity:224",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.5,
			},
		},
		{
			name = "default_water_flowing_animated.png^[noalpha^[opacity:224",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.5,
			},
		},
	},
	use_texture_alpha="clip",
	paramtype = "light",
	paramtype2 = "flowingliquid",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "flowing",
	liquid_renewable=false,
	liquid_alternative_flowing = "default:water_flowing",
	liquid_alternative_source = "default:water_source",
	liquid_viscosity = 1,
	liquid_range = 1,
	post_effect_color = {a = 103, r = 30, g = 60, b = 90},
	groups = {water = 3, liquid = 3, cools_lava = 1, nopicker=1},
	sounds = default.node_sound_water_defaults(),
	light_source=1,
})


minetest.register_node(":default:river_water_source", {
	description = "River Water Source",
	drawtype = "liquid",
	tiles = {
		{
			name = "default_river_water_source_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
		{
			name = "default_river_water_source_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	use_texture_alpha="clip",
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_flowing = "default:river_water_flowing",
	liquid_alternative_source = "default:river_water_source",
	liquid_viscosity = 1,
	liquid_renewable = false,
	liquid_range = 1,
	post_effect_color = {a = 103, r = 30, g = 76, b = 90},
	groups = {water = 3, liquid = 3, cools_lava = 1},
	sounds = default.node_sound_water_defaults(),
	--light_source = 2,

})

minetest.register_node(":default:river_water_flowing", {
	description = "Flowing River Water",
	drawtype = "flowingliquid",
	tiles = {"default_river_water.png"},
	special_tiles = {
		{
			name = "default_river_water_flowing_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1,
			},
		},
		{
			name = "default_river_water_flowing_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1,
			},
		},
	},
	use_texture_alpha="clip",
	paramtype = "light",
	paramtype2 = "flowingliquid",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "flowing",
	liquid_alternative_flowing = "default:river_water_flowing",
	liquid_alternative_source = "default:river_water_source",
	liquid_viscosity = 1,
	liquid_renewable = false,
	liquid_range = 1,
	post_effect_color = {a = 103, r = 30, g = 76, b = 90},
	groups = {water = 3, liquid = 3, nopicker=1, cools_lava = 1},
	sounds = default.node_sound_water_defaults(),
	--light_source = 2,
})


minetest.register_node(":default:lava_source", {
	description = "Lava Source",
	drawtype = "liquid",
	tiles = {
		{
			name = "default_lava_source_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
		{
			name = "default_lava_source_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 3.0,
			},
		},
	},
	paramtype = "light",
	light_source = default.LIGHT_MAX - 4,
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = false,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_flowing = "default:lava_source",
	liquid_alternative_source = "default:lava_source",
	liquid_viscosity = 3,
	liquid_range=0,
	liquid_renewable=false,
	damage_per_second = 8,
	post_effect_color = {a = 191, r = 255, g = 64, b = 0},
	groups = {lava = 3, liquid = 2, igniter = 1, noblast=1},
})

--
-- Tools / "Advanced" crafting / Non-"natural"
--

local function register_sign(material, desc, def)
	minetest.register_node(":default:sign_wall_" .. material, {
		description = desc,
		drawtype = "nodebox",
		tiles = {"default_sign_wall_" .. material .. ".png"},
		inventory_image = "default_sign_" .. material .. ".png",
		wield_image = "default_sign_" .. material .. ".png",
		paramtype = "light",
		paramtype2 = "wallmounted",
		sunlight_propagates = true,
		is_ground_content = false,
		walkable = false,
		use_texture_alpha="opaque",
		node_box = {
			type = "wallmounted",
			wall_top    = {-0.4375, 0.4375, -0.3125, 0.4375, 0.5, 0.3125},
			wall_bottom = {-0.4375, -0.5, -0.3125, 0.4375, -0.4375, 0.3125},
			wall_side   = {-0.5, -0.3125, -0.4375, -0.4375, 0.3125, 0.4375},
		},
		groups = def.groups,
		legacy_wallmounted = true,
		sounds = def.sounds,

		on_construct = function(pos)
			local meta = minetest.get_meta(pos)
			meta:set_string("formspec", "field[text;;${text}]")
		end,
		on_receive_fields = function(pos, formname, fields, sender)
			local player_name = sender:get_player_name()
			if minetest.is_protected(pos, player_name) then
				minetest.record_protection_violation(pos, player_name)
				return
			end
			local text = fields.text
			if not text then
				return
			end
			if string.len(text) > 512 then
				minetest.chat_send_player(player_name, "Text too long")
				return
			end
			minetest.log("action", (player_name or "") .. " wrote \"" ..
				text .. "\" to sign at " .. minetest.pos_to_string(pos))
			local meta = minetest.get_meta(pos)
			meta:set_string("text", text)
			meta:set_string("infotext", '"' .. text .. '"')
		end,
	})
end

register_sign("steel", "Sign", {
	sounds = default.node_sound_metal_defaults(),
	groups = {oddly_breakable_by_hand=1, attached_node = 1, protected=1}
})

--[[
minetest.register_node("default:ladder_wood", {
	description = "Wooden Ladder",
	drawtype = "signlike",
	tiles = {"default_ladder_wood.png"},
	inventory_image = "default_ladder_wood.png",
	wield_image = "default_ladder_wood.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	climbable = true,
	is_ground_content = false,
	selection_box = {
		type = "wallmounted",
		--wall_top = = <default>
		--wall_bottom = = <default>
		--wall_side = = <default>
	},
	groups = {choppy = 2, oddly_breakable_by_hand = 3, flammable = 2},
	legacy_wallmounted = true,
	sounds = default.node_sound_wood_defaults(),
})

minetest.register_node("default:ladder_steel", {
	description = "Steel Ladder",
	drawtype = "signlike",
	tiles = {"default_ladder_steel.png"},
	inventory_image = "default_ladder_steel.png",
	wield_image = "default_ladder_steel.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	climbable = true,
	is_ground_content = false,
	selection_box = {
		type = "wallmounted",
		--wall_top = = <default>
		--wall_bottom = = <default>
		--wall_side = = <default>
	},
	groups = {cracky = 2},
	sounds = default.node_sound_metal_defaults(),
})

default.register_fence("default:fence_wood", {
	description = "Apple Wood Fence",
	texture = "default_fence_wood.png",
	inventory_image = "default_fence_overlay.png^default_wood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_overlay.png^default_wood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	material = "default:wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = default.node_sound_wood_defaults()
})

default.register_fence("default:fence_acacia_wood", {
	description = "Acacia Wood Fence",
	texture = "default_fence_acacia_wood.png",
	inventory_image = "default_fence_overlay.png^default_acacia_wood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_overlay.png^default_acacia_wood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	material = "default:acacia_wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = default.node_sound_wood_defaults()
})

default.register_fence("default:fence_junglewood", {
	description = "Jungle Wood Fence",
	texture = "default_fence_junglewood.png",
	inventory_image = "default_fence_overlay.png^default_junglewood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_overlay.png^default_junglewood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	material = "default:junglewood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = default.node_sound_wood_defaults()
})

default.register_fence("default:fence_pine_wood", {
	description = "Pine Wood Fence",
	texture = "default_fence_pine_wood.png",
	inventory_image = "default_fence_overlay.png^default_pine_wood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_overlay.png^default_pine_wood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	material = "default:pine_wood",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults()
})

default.register_fence("default:fence_aspen_wood", {
	description = "Aspen Wood Fence",
	texture = "default_fence_aspen_wood.png",
	inventory_image = "default_fence_overlay.png^default_aspen_wood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_overlay.png^default_aspen_wood.png^" ..
				"default_fence_overlay.png^[makealpha:255,126,126",
	material = "default:aspen_wood",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults()
})

default.register_fence_rail("default:fence_rail_wood", {
	description = "Apple Wood Fence Rail",
	texture = "default_fence_rail_wood.png",
	inventory_image = "default_fence_rail_overlay.png^default_wood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_rail_overlay.png^default_wood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	material = "default:wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = default.node_sound_wood_defaults()
})

default.register_fence_rail("default:fence_rail_acacia_wood", {
	description = "Acacia Wood Fence Rail",
	texture = "default_fence_rail_acacia_wood.png",
	inventory_image = "default_fence_rail_overlay.png^default_acacia_wood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_rail_overlay.png^default_acacia_wood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	material = "default:acacia_wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = default.node_sound_wood_defaults()
})

default.register_fence_rail("default:fence_rail_junglewood", {
	description = "Jungle Wood Fence Rail",
	texture = "default_fence_rail_junglewood.png",
	inventory_image = "default_fence_rail_overlay.png^default_junglewood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_rail_overlay.png^default_junglewood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	material = "default:junglewood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = default.node_sound_wood_defaults()
})

default.register_fence_rail("default:fence_rail_pine_wood", {
	description = "Pine Wood Fence Rail",
	texture = "default_fence_rail_pine_wood.png",
	inventory_image = "default_fence_rail_overlay.png^default_pine_wood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_rail_overlay.png^default_pine_wood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	material = "default:pine_wood",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3},
	sounds = default.node_sound_wood_defaults()
})

default.register_fence_rail("default:fence_rail_aspen_wood", {
	description = "Aspen Wood Fence Rail",
	texture = "default_fence_rail_aspen_wood.png",
	inventory_image = "default_fence_rail_overlay.png^default_aspen_wood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	wield_image = "default_fence_rail_overlay.png^default_aspen_wood.png^" ..
				"default_fence_rail_overlay.png^[makealpha:255,126,126",
	material = "default:aspen_wood",
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = default.node_sound_wood_defaults()
})
--]]

minetest.register_node(":default:glass", {
	description = "Glass",
	drawtype = "glasslike_framed_optional",
	tiles = {"default_glass.png", "default_glass_detail.png"},
	use_texture_alpha="clip",
	paramtype = "light",
	paramtype2 = "glasslikeliquidlevel",
	sunlight_propagates = true,
	is_ground_content = false,
	groups = {cracky = 3, },
	sounds = default.node_sound_glass_defaults(),
})

minetest.register_node(":default:obsidian_glass", {
	description = "Obsidian Glass",
	drawtype="glasslike",
	tiles = {"default_obsidian_glass.png^(default_cloud.png^[multiply:black^[opacity:192)"},
	use_texture_alpha="blend",
	paramtype = "light",
	sunlight_propagates = true,
	sounds = default.node_sound_glass_defaults(),
	groups = {cracky = 1, level=2, protected=1},
})


minetest.register_node(":default:brick", {
	description = "Brick Block",
	tiles = { "darkage_stone_brick.png^[multiply:tan"},
	is_ground_content = false,
	groups = {cracky = 1, yatpm=1},
	sounds = default.node_sound_stone_defaults(),
	drop="stairs:stair_brick 4",
})

--
-- Misc
--

minetest.register_node(":default:cloud", {
	description = "Cloud",
	tiles = {"default_cloud.png"},
	is_ground_content = false,
	sounds = default.node_sound_defaults(),
	groups = {not_in_creative_inventory = 1},
})

--
-- register trees for leafdecay
--
default.register_leafdecay({
	trunks = {"default:tree"},
	leaves = {"default:leaves", "default:apple"},
	radius = 2,
})

default.register_leafdecay({
	trunks = {"default:jungletree"},
	leaves = {"default:jungleleaves"},
	radius = 2,
})

default.register_leafdecay({
	trunks = {"default:pine_tree"},
	leaves = {"default:pine_needles"},
	radius = 2,
})

default.register_leafdecay({
	trunks = {"default:acacia_tree"},
	leaves = {"default:acacia_leaves"},
	radius = 2,
})

default.register_leafdecay({
	trunks = {"default:aspen_tree"},
	leaves = {"default:aspen_leaves"},
	radius = 2,
})

default.register_leafdecay({
	trunks = {"default:bush_stem"},
	leaves = {"default:bush_leaves"},
	radius = 1,
})

default.register_leafdecay({
	trunks = {"default:acacia_bush_stem"},
	leaves = {"default:acacia_bush_leaves"},
	radius = 1,
})

default.register_leafdecay({
	trunks = {"default:pine_bush_stem"},
	leaves = {"default:pine_bush_needles"},
	radius = 1,
})
default.register_leafdecay({
	trunks = {"default:palm_tree"},
	leaves = {"default:palm_leaves"},
	radius = 4,
})

minetest.register_node(":default:basalt_brick", {
	description = "Basalt Brick",
	tiles = {"darkage_basalt_brick.png"},
	groups = {cracky=2, level=4},
	sounds = default.node_sound_stone_defaults(),
	is_ground_content = false,
})

minetest.register_node(":default:basalt", {
	description = "Basalt",
	tiles = {"darkage_basalt.png"},
	is_ground_content = true,
	drop="",
	groups = {cracky=2, stone=1, level=1},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node(":default:basalt_cobble", {
	description = "Basalt Cobble",
	tiles = {"darkage_basalt_cobble.png"},
	groups = {cracky=3, yatpm=1},
	sounds = default.node_sound_stone_defaults(),
	is_ground_content = false,
	drop="stairs:stair_basalt_cobble 4",
	on_place = minetest.rotate_node
})

