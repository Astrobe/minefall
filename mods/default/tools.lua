-- mods/default/tools.lua

-- The hand
minetest.register_item(":", {
	type = "none",
	wield_image = "wieldhand.png",
	wield_scale = {x=1,y=1,z=2.5},
	range=3,
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level = 0,
		groupcaps = {
			crumbly = {times={[1]=6, [2]=4, [3]=2.00}, uses=0, maxlevel=1},
			snappy = {times={[2]=4, [3]=1}, uses=0, maxlevel=1},
			choppy = {times={[2]=6, [3]=4}, uses=0, maxlevel=1},
			oddly_breakable_by_hand = {times={[1]=3.50,[2]=2.00,[3]=0.70}, uses=0}
		},
		damage_groups = {fleshy=1},
	},
})

--
-- Picks
--


minetest.register_tool(":default:pick_bronze", {
	description = "Bronze Pickaxe",
	inventory_image = "default_tool_bronzepick.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			cracky = {times={[1]=4.50, [2]=1.80, [3]=0.90}, uses=180, maxlevel=2},
		},
		damage_groups = {fleshy=2},
		punch_attack_uses = 256,
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool(":default:pick_steel", {
	description = "Steel Pickaxe",
	inventory_image = "default_tool_steelpick.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=2,
		groupcaps={
			cracky = {times={[1]=4.50, [2]=1.80, [3]=0.90}, uses=450, maxlevel=2},
		},
		damage_groups = {fleshy=2},
		punch_attack_uses = 512,
	},
	sound = {breaks = "default_tool_breaks"},
})

--
-- Shovels
--


-- For shovels we don't decrease much dig time because otherwise players
-- may dig too much by accident, and since most crumbly blocks are falling blocks,
-- this can be annoying at times.
minetest.register_tool(":default:shovel_bronze", {
	description = "Bronze Shovel",
	inventory_image = "default_tool_bronzeshovel.png",
	wield_image = "default_tool_bronzeshovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.1,
		max_drop_level=1,
		groupcaps={
			crumbly = {times={[1]=2, [2]=1.0, [3]=0.5}, uses=60, maxlevel=2},
		},
		damage_groups = {fleshy=2},
		punch_attack_uses = 256,
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool(":default:shovel_steel", {
	description = "Steel Shovel",
	inventory_image = "default_tool_steelshovel.png",
	wield_image = "default_tool_steelshovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.1,
		max_drop_level=1,
		groupcaps={
			crumbly = {times={[1]=2, [2]=1, [3]=0.5}, uses=150, maxlevel=2},
		},
		damage_groups = {fleshy=2},
		punch_attack_uses = 512,
	},
	sound = {breaks = "default_tool_breaks"},
})

--
-- Axes
--

-- Axes have good damage and good stopping power, but compared to swords there durability/cost is lower and their DPS is lower

minetest.register_tool(":default:axe_bronze", {
	description = "Bronze Axe",
	inventory_image = "default_tool_bronzeaxe.png",
	tool_capabilities = {
		full_punch_interval = 1.2,
		max_drop_level=1,
		groupcaps={
			choppy={times={[1]=2.75, [2]=1.70, [3]=1.15}, uses=90, maxlevel=2},
		},
		damage_groups = {fleshy=4},
		punch_attack_uses = 256,
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool(":default:axe_steel", {
	description = "Steel Axe",
	inventory_image = "default_tool_steelaxe.png",
	tool_capabilities = {
		full_punch_interval = 1.2,
		max_drop_level=1,
		groupcaps={
			choppy={times={[1]=2.75, [2]=1.70, [3]=1.15}, uses=225, maxlevel=2},
		},
		damage_groups = {fleshy=6},
		punch_attack_uses = 512,
	},
	sound = {breaks = "default_tool_breaks"},
})

--
-- Swords
--


minetest.register_tool(":default:sword_bronze", {
	description = "Bronze Sword",
	inventory_image = "default_tool_bronzesword.png",
	tool_capabilities = {
		full_punch_interval = 0.75,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=2.5, [2]=1.20, [3]=0.35}, uses=100, maxlevel=1},
		},
		damage_groups = {fleshy=4},
		punch_attack_uses = 256,
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool(":default:sword_steel", {
	description = "Steel Sword",
	inventory_image = "default_tool_steelsword.png",
	tool_capabilities = {
		full_punch_interval = 0.75,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=2.5, [2]=1.20, [3]=0.35}, uses=100, maxlevel=2},
		},
		damage_groups = {fleshy=6},
		punch_attack_uses = 512,
	},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_tool(":default:key", {
	description = "Key",
	inventory_image = "default_key.png",
	groups = {key = 1, not_in_creative_inventory = 1},
	stack_max = 1,
	on_place = function(itemstack, placer, pointed_thing)
		local under = pointed_thing.under
		local node = minetest.get_node(under)
		local def = minetest.registered_nodes[node.name]
		if def and def.on_rightclick and
				not (placer and placer:is_player() and
				placer:get_player_control().sneak) then
			return def.on_rightclick(under, node, placer, itemstack,
				pointed_thing) or itemstack
		end
		if pointed_thing.type ~= "node" then
			return itemstack
		end

		local pos = pointed_thing.under
		node = minetest.get_node(pos)

		if not node or node.name == "ignore" then
			return itemstack
		end

		local ndef = minetest.registered_nodes[node.name]
		if not ndef then
			return itemstack
		end

		local on_key_use = ndef.on_key_use
		if on_key_use then
			on_key_use(pos, placer, itemstack)
		end

		return itemstack
	end
})
