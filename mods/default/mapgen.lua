--
-- Aliases for map generators
--

minetest.register_alias("mapgen_stone", "default:stone")
minetest.register_alias("mapgen_dirt", "default:dirt")
minetest.register_alias("mapgen_dirt_with_grass", "default:dirt_with_grass")
minetest.register_alias("mapgen_sand", "default:sand")
minetest.register_alias("mapgen_rainforest_swamp", "default:water_source")
minetest.register_alias("mapgen_water_source", "default:water_source")
minetest.register_alias("mapgen_river_water_source", "default:river_water_source")
minetest.register_alias("mapgen_lava_source", "default:lava_source")
minetest.register_alias("mapgen_gravel", "default:gravel")
minetest.register_alias("mapgen_desert_stone", "default:desert_stone")
minetest.register_alias("mapgen_desert_sand", "default:desert_sand")
minetest.register_alias("mapgen_dirt_with_snow", "default:dirt_with_snow")
minetest.register_alias("mapgen_snowblock", "default:snowblock")
minetest.register_alias("mapgen_ice", "default:ice")
minetest.register_alias("mapgen_sandstone", "default:sandstone")

minetest.register_alias("mapgen:palm_tree", "default:palm_tree")
minetest.register_alias("mapgen:palm_leaves", "default:palm_leaves")

-- Flora

minetest.register_alias("mapgen_tree", "default:tree")
minetest.register_alias("mapgen_leaves", "default:leaves")
minetest.register_alias("mapgen_apple", "default:apple")
minetest.register_alias("mapgen_jungletree", "default:jungletree")
minetest.register_alias("mapgen_jungleleaves", "default:jungleleaves")
minetest.register_alias("mapgen_junglegrass", "default:junglegrass")
minetest.register_alias("mapgen_pine_tree", "default:pine_tree")
minetest.register_alias("mapgen_pine_needles", "default:pine_needles")

--
-- Register ores
--


local seed=PseudoRandom(0) -- perlin noise methods use the world seed anyway. Even if not, 0 is a perfectly fine random number.

function default.register_ores()

	-- ORES must come first.
	
	-- stone blob with ores
	minetest.register_ore({
		ore_type        = "blob",
		ore             = "default:lava_source",
		wherein         = {"default:basalt"},
		clust_scarcity  = 16 * 16 * 16,
		clust_size      = 12,
		y_max           = 0,
		y_min           = -999,
		noise_threshold = 0.0,
		noise_params    = {
			offset = 0.1,
			scale = 0.2,
			spread = {x = 5, y = 5, z = 5},
			seed = seed:next(),
			octaves = 1,
			persist = 0.0
		},
	})
	
	minetest.register_ore({
		ore_type        = "blob",
		ore             = "default:stone",
		wherein         = {"default:limestone", "default:basalt"},
		clust_scarcity  = 8 * 8 * 8,
		clust_size      = 6,
		y_min           = -5000,
		y_max           = 31000,
		noise_threshold = 0.0,
		noise_params    = {
			offset = 0.5,
			scale = 0.2,
			spread = {x = 5, y = 5, z = 5},
			seed = seed:next(),
			octaves = 1,
			persist = 0.0
		},
	})

	minetest.register_ore({
		ore_type        = "blob",
		ore             = "default:gravel",
		wherein         = {"default:dirt_with_grass", "default:dry_dirt", "default:dirt_with_dry_grass", "default:dirt_with_coniferous_litter", "default:permafrost", "default:dirt_with_rainforest_litter"},
		clust_scarcity  = 24 * 24 * 24,
		clust_size      = 6,
		y_max           = 999,
		y_min           = 0,
		noise_threshold = 0.0,
		noise_params    = {
			offset = 0.5,
			scale = 0.2,
			spread = {x = 5, y = 5, z = 5},
			seed = seed:next(),
			octaves = 1,
			persist = 0.0
		},
	})
	minetest.register_ore
	{
		ore_type        = "blob",
		ore             = "mf_decor:slate",
		wherein         = "default:stone",
		clust_scarcity  = 16 * 16 * 16,
		clust_size      = 3,
		y_min           = -999,
		y_max           = 999,
		noise_threshold = 0.0,
		noise_params    = {
			offset = 0.5,
			scale = 0.2,
			spread = {x = 5, y = 5, z = 5},
			seed = seed:next(),
			octaves = 1,
			persist = 0.0
		},
		biomes= {"taiga"}
	}
	-- Scatter ores

	-- Coal

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "default:stone_with_coal",
		wherein        = "default:stone",
		clust_scarcity = 12 * 12 * 12,
		clust_num_ores = 8,
		clust_size     = 3,
		y_max          = 999,
		y_min          = -5000,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "default:stone_with_tin",
		wherein        = {"default:stone"},
		clust_scarcity = 12 * 12 * 12,
		clust_num_ores = 4,
		clust_size     = 3,
		y_max          = 999,
		y_min          = -5000,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "default:desert_stone_with_tin",
		wherein        = "default:desert_stone",
		clust_scarcity = 12 * 12 * 12,
		clust_num_ores = 5,
		clust_size     = 4,
		y_max          = 999,
		y_min          = -5000,
	})
end

--
-- BIOMES
-- This is basically a 3x3 biome grid centered on 16, 48 and 80 for both H and T,
-- with little offsets here and ther to avoid direct "contact" of limestone with
-- desert stone.

local tree_limit=60

function default.register_biomes()
	local under_limit=-60
	local shore_limit=-15

	-- Taiga
	minetest.register_biome({
		name = "taiga",
		node_top = "default:dirt_with_snow",
		depth_top = 1,
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = tree_limit,
		y_min = 2,
		heat_point = 16,
		humidity_point = 80,
		vertical_blend = 4,
		node_dungeon = "default:stone_block",
		node_dungeon_stair = "stairs:stair_stone_block",
	})

	minetest.register_biome({
		name = "taiga_high",
		node_top = "default:snowblock",
		depth_top = 1,
		node_filler="default:cave_ice",
		depth_filler = 1,
		y_max = 999,
		y_min = tree_limit,
		heat_point = 16,
		humidity_point = 80,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})

	minetest.register_biome({
		name = "taiga_shore",
		node_top = "default:gravel",
		depth_top = 1,
		node_water_top = "default:cave_ice",
		depth_water_top = 1,
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = 2,
		y_min = under_limit,
		heat_point = 16,
		humidity_point = 80,
		node_dungeon_alt = "default:mossycobble",
		node_dungeon_stair = "stairs:stair_mossycobble",
	})

	-- Grassland
	-- is actually a cold (dry) biome, contrary to other "green" biomes.
	-- that's why it sits on stone instead of limestone.
	minetest.register_biome({
		name = "grassland",
		node_top = "default:dirt_with_grass",
		depth_top = 1,
		node_stone= "default:stone",
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = tree_limit,
		y_min = 2,
		heat_point = 17,
		humidity_point = 17,
		vertical_blend = 4,
		node_dungeon = "default:stone_block",
		node_dungeon_stair = "stairs:stair_stone_block",
	})

	minetest.register_biome({
		name = "grassland_high",
		-- it is a freezing dry biome. No opinion on what should be node_top 
		-- node_top = "default:snowblock",
		-- depth_top = 1,
		node_stone= "default:stone",
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = 999,
		y_min = tree_limit,
		heat_point = 17,
		humidity_point = 17,
		node_dungeon = "default:stone_block",
		node_dungeon_stair = "stairs:stair_stone_block",
	})

	minetest.register_biome({
		name = "grassland_dunes",
		node_top = "default:gravel",
		depth_top = 1,
		node_stone= "default:stone",
		node_riverbed = "default:gravel",
		depth_riverbed = 1,
		y_max = 2,
		y_min = shore_limit,
		heat_point = 17,
		humidity_point = 17,
		node_dungeon = "default:stone_block",
		node_dungeon_stair = "stairs:stair_stone_block",
	})

	-- Coniferous forest

	minetest.register_biome({
		name = "coniferous_forest",
		node_top = "default:dirt_with_coniferous_litter",
		depth_top = 1,
		node_stone = "default:stone",
		node_riverbed = "default:gravel",
		depth_riverbed = 1,
		y_max = tree_limit,
		y_min = 2,
		heat_point = 16,
		humidity_point = 48,
		node_dungeon = "default:stone_block",
		node_dungeon_stair = "stairs:stair_stone_block",
		vertical_blend = 4,
	})

	minetest.register_biome({
		name = "coniferous_forest_high",
		node_top="default:snowblock",
		depth_top = 1,
		node_filler="default:cave_ice",
		depth_filler = 1,
		y_max = 999,
		y_min = tree_limit,
		heat_point = 16,
		humidity_point = 48,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})

	minetest.register_biome({
		name = "coniferous_forest_dunes",
		node_top = "default:gravel",
		depth_top = 1,
		node_stone = "default:stone",
		node_riverbed = "default:sand",
		depth_riverbed = 1,
		y_max = 2,
		y_min = shore_limit,
		heat_point = 16,
		humidity_point = 48,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})

	-- Deciduous forest

	minetest.register_biome({
		name = "deciduous_forest",
		node_stone = "default:limestone",
		node_top = "default:dirt_with_grass",
		depth_top = 1,
		node_riverbed = "default:gravel",
		depth_riverbed = 2,
		y_max = tree_limit,
		y_min = 2,
		heat_point = 48,
		humidity_point = 48,
		vertical_blend = 4,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})

	minetest.register_biome({
		name = "deciduous_forest_high",
		node_stone = "default:limestone",
		node_top = "default:gravel",
		depth_top = 1,
		y_max = 999,
		y_min = tree_limit,
		heat_point = 48,
		humidity_point = 48,
		vertical_blend = 4,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})

	minetest.register_biome({
		name = "deciduous_forest_shore",
		node_stone = "default:limestone",
		node_top = "default:gravel",
		depth_top = 1,
		node_riverbed = "default:gravel",
		depth_riverbed = 2,
		y_max = 2,
		y_min = shore_limit,
		heat_point = 48,
		humidity_point = 48,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})

	-- Desert

	minetest.register_biome({
		name = "desert",
		node_top = "default:desert_sand",
		depth_top = 1,
		node_stone = "default:desert_stone",
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = tree_limit,
		y_min = 3,
		vertical_blend = 4,
		heat_point = 81,
		humidity_point = 15,
		node_dungeon = "default:desert_stone_block",
		node_dungeon_stair = "stairs:stair_desert_stone_block",
	})

	minetest.register_biome({
		name = "desert_high",
		node_stone = "default:desert_stone",
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = 999,
		y_min = tree_limit,
		vertical_blend = 4,
		heat_point = 81,
		humidity_point = 15,
		node_dungeon = "default:desert_cobble",
		node_dungeon_stair = "stairs:stair_desert_cobble",
	})

	minetest.register_biome({
		name = "desert_ocean",
		node_top = "default:sand",
		depth_top = 1,
		node_stone = "default:desert_stone",
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = 3,
		y_min = shore_limit,
		heat_point = 81,
		humidity_point = 15,
		node_dungeon = "default:desert_cobble",
		node_dungeon_stair = "stairs:stair_desert_cobble",
	})

	-- Dirt desert. Not sandstone at all. Recycled that one, didn't change the name.
	minetest.register_biome({
		name = "sandstone_desert",
		node_top = "default:dry_dirt",
		depth_top = 1,
		node_stone = "default:stone",
		node_riverbed = "default:sand",
		depth_riverbed = 1,
		y_max = tree_limit,
		y_min = 3,
		heat_point = 49,
		humidity_point = 16,
		node_dungeon = "default:stone_block",
		node_dungeon_stair = "stairs:stair_stone_block",
		vertical_blend=4,
	})

	minetest.register_biome({
		name = "sandstone_desert_high",
		-- node_top = "default:gravel",
		-- depth_top = 1,
		node_stone = "default:stone",
		y_max = 999,
		y_min = tree_limit,
		heat_point = 49,
		humidity_point = 16,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})

	minetest.register_biome({
		name = "sandstone_desert_shore",
		node_top = "default:sand",
		depth_top = 1,
		node_stone = "default:stone",
		node_riverbed = "default:sand",
		depth_riverbed = 1,
		y_max = 3,
		y_min = shore_limit,
		heat_point = 49,
		humidity_point = 16,
		node_dungeon_alt = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})

	-- Swamp

	minetest.register_biome({
		name = "swamp",
		node_top = "default:dirt_with_grass",
		depth_top = 1,
		node_stone = "default:limestone",
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = tree_limit,
		y_min = 2,
		heat_point = 48,
		humidity_point = 80,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
		vertical_blend = 4,
	})

	minetest.register_biome({
		name = "swamp_high",
		node_top = "default:gravel",
		depth_top = 1,
		node_stone = "default:limestone",
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = 999,
		y_min = tree_limit,
		heat_point = 48,
		humidity_point = 80,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})

	minetest.register_biome({
		name = "swamp_shore",
		node_stone = "default:limestone",
		node_top = "default:gravel",
		depth_top = 1,
		node_riverbed = "default:dirt",
		depth_riverbed = 2,
		y_max = 2,
		y_min = shore_limit,
		heat_point = 48,
		humidity_point = 80,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})

	-- Savanna

	minetest.register_biome({
		name = "savanna",
		node_stone = "default:stone",
		node_top = "default:dirt_with_dry_grass",
		depth_top = 1,
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = tree_limit,
		y_min = 3,
		heat_point = 80,
		humidity_point = 47,
		node_dungeon = "default:stone_block",
		node_dungeon_stair = "stairs:stair_stone_block",
		vertical_blend = 4,
	})

	minetest.register_biome({
		name = "savanna_high",
		node_stone = "default:stone",
		node_top = "default:gravel",
		depth_top = 1,
		y_max = 999,
		y_min = tree_limit,
		heat_point = 80,
		humidity_point = 47,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})

	minetest.register_biome({
		name = "savanna_shore",
		node_stone = "default:stone",
		node_top = "default:sand",
		depth_top = 1,
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = 3,
		y_min = shore_limit,
		heat_point = 80,
		humidity_point = 47,
		node_dungeon = "default:cobble",
		node_dungeon_stair = "stairs:stair_cobble",
	})


	-- Rainforest

	minetest.register_biome({
		name = "rainforest",
		node_top = "default:dirt_with_rainforest_litter",
		depth_top = 1,
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = tree_limit,
		y_min = 1,
		heat_point = 80,
		humidity_point = 80,
		node_dungeon = "default:stone_block",
		node_dungeon_stair = "stairs:stair_stone_block",
		vertical_blend = 4,
	})

	minetest.register_biome({
		name = "rainforest_high",
		node_top = "default:gravel",
		depth_top = 1,
		y_max = 999,
		y_min = tree_limit,
		heat_point = 80,
		humidity_point = 80,
		node_dungeon = "default:cobble",
		node_dungeon_alt = "default:mossycobble",
		node_dungeon_stair = "stairs:stair_mossycobble",
	})

	minetest.register_biome({
		name = "rainforest_swamp",
		node_top = "default:sand",
		depth_top = 1,
		node_riverbed = "default:sand",
		depth_riverbed = 2,
		y_max = 0,
		y_min = shore_limit,
		heat_point = 80,
		humidity_point = 80,
		node_dungeon = "default:cobble",
		node_dungeon_alt = "default:mossycobble",
		node_dungeon_stair = "stairs:stair_mossycobble",
	})

	-- oceans

	minetest.register_biome({
		name = "ocean",
		node_stone = "default:stone",
		--node_top="default:coral_brown",
		--depth_top=1,
		y_max = shore_limit,
		y_min = under_limit,
		heat_point = 80,
		humidity_point = 80,
		node_dungeon = "default:mossycobble",
		node_dungeon_stair = "stairs:stair_mossycobble",
		vertical_blend = 4,
	})
	
	-- Underground

	minetest.register_biome({
		name = "underground",
		node_stone = "default:basalt",
		y_max = -100,
		y_min = -300,
		heat_point = 50,
		humidity_point = 50,
		node_dungeon = "default:basalt_brick",
		node_dungeon_stair = "stairs:stair_basalt_cobble",
		vertical_blend = 4,
	})
end


--
-- Register decorations
--
local function register_snow_grass_decoration(offset, scale, length)
	minetest.register_decoration({
		deco_type = "simple",
		place_on = {"default:dirt_with_snow",},
		sidelen = 80,
		fill_ratio = 0.025,

		biomes = { "taiga"},
		y_min = 1,
		y_max = 31000,
		decoration = "default:snow_grass_" .. length,
	})
end

local function register_grass_decoration(offset, scale, length)
	minetest.register_decoration({
		name = "default:grass_" .. length,
		deco_type = "simple",
		place_on = {"default:dirt_with_grass"},
		sidelen = 16,
		noise_params = {
			offset = offset,
			scale = scale,
			spread = {x = 200, y = 200, z = 200},
			seed = seed:next(),
			octaves = 3,
			persist = 0.6
		},
		y_max = 31000,
		y_min = 1,
		decoration = "default:grass_" .. length,
	})
end

local function register_dry_grass_decoration(offset, scale, length)
	minetest.register_decoration({
		name = "default:dry_grass_" .. length,
		deco_type = "simple",
		place_on = {"default:dirt_with_dry_grass"},
		sidelen = 16,
		noise_params = {
			offset = offset,
			scale = scale,
			spread = {x = 200, y = 200, z = 200},
			seed = seed:next(),
			octaves = 3,
			persist = 0.6
		},
		y_max = 31000,
		y_min = 1,
		decoration = "default:dry_grass_" .. length,
	})
end

local function register_fern_decoration(length)
	minetest.register_decoration({
		name = "default:fern_" .. length,
		deco_type = "simple",
		place_on = {"default:dirt_with_coniferous_litter"},
		sidelen = 16,
		noise_params = {
			offset = 0,
			scale = 0.2,
			spread = {x = 100, y = 100, z = 100},
			seed = seed:next(),
			octaves = 3,
			persist = 0.7
		},
		y_max = 31000,
		y_min = 6,
		decoration = "default:fern_" .. length,
	})
end

function default.register_decorations()

	-- Apparently bushes force-place their stem, which is bad for us because
	-- our trees don't replace the dirt below them. Better have a bush without stem than
	-- a tree with a stem as its base.
	-- Bush

	local path=minetest.get_modpath("mf_default")
	minetest.register_decoration({
		name = "default:bush",
		deco_type = "schematic",
		place_on = {"default:dirt_with_grass"},
		sidelen = 16,
		noise_params = {
			offset = -0.004,
			scale = 0.01,
			spread = {x = 100, y = 100, z = 100},
			seed = seed:next(),
			octaves = 3,
			persist = 0.7,
		},
		biomes = {"deciduous_forest", "swamp", "grassland"},
		y_max = tree_limit,
		y_min = 1,
		schematic = path .. "/schematics/bush.mts",
		flags = "place_center_x, place_center_z",
	})

	-- Blueberry bush

	minetest.register_decoration({
		name = "default:blueberry_bush",
		deco_type = "schematic",
		place_on = {"default:dirt_with_grass"},
		sidelen = 16,
		noise_params = {
			offset = -0.004,
			scale = 0.01,
			spread = {x = 100, y = 100, z = 100},
			seed = seed:next(),
			octaves = 3,
			persist = 0.7,
		},
		biomes = {"swamp", "deciduous_forest"},
		y_max = tree_limit,
		y_min = 1,
		place_offset_y = 1,
		schematic = path .. "/schematics/blueberry_bush.mts",
		flags = "place_center_x, place_center_z",
	})

	-- Acacia bush

	minetest.register_decoration({
		name = "default:acacia_bush",
		deco_type = "schematic",
		place_on = {"default:dirt_with_dry_grass"},
		sidelen = 16,
		noise_params = {
			offset = -0.004,
			scale = 0.01,
			spread = {x = 100, y = 100, z = 100},
			seed = seed:next(),
			octaves = 3,
			persist = 0.7,
		},
		y_max = tree_limit,
		y_min = 1,
		schematic = path .. "/schematics/acacia_bush.mts",
		flags = "place_center_x, place_center_z",
	})

	-- Pine bush

	minetest.register_decoration({
		name = "default:pine_bush",
		deco_type = "schematic",
		place_on = {"default:dirt_with_coniferous_litter", "default:dirt_with_snow"},
		sidelen = 16,
		noise_params = {
			offset = -0.004,
			scale = 0.01,
			spread = {x = 100, y = 100, z = 100},
			seed = seed:next(),
			octaves = 3,
			persist = 0.7,
		},
		biomes = {"taiga", "coniferous_forest"},
		y_max = tree_limit,
		y_min = 4,
		schematic = path .. "/schematics/pine_bush.mts",
		flags = "place_center_x, place_center_z",
	})
	minetest.register_decoration({
		name = "default:apple_tree",
		deco_type = "schematic",
		place_on = {"default:dirt_with_grass"},
		sidelen = 16,
		noise_params = {
			offset = 0.003,
			scale = 0.004,
			spread = {x = 250, y = 250, z = 250},
			seed = seed:next(),
			octaves = 3,
			persist = 0.66
		},
		biomes = {"swamp"},
		y_max = tree_limit,
		y_min = 1,
		schematic = path .. "/schematics/apple_tree.mts",
		flags = "place_center_x, place_center_z",
		rotation = "random",
		place_offset_y = 1,
	})

	minetest.register_decoration({
		name = "default:apple_log",
		deco_type = "schematic",
		place_on = {"default:dirt_with_grass"},
		place_offset_y = 1,
		sidelen = 16,
		noise_params = {
			offset = 0.002,
			scale = 0.0007,
			spread = {x = 250, y = 250, z = 250},
			seed = seed:next(),
			octaves = 3,
			persist = 0.66
		},
		biomes = {"swamp"},
		y_max = tree_limit,
		y_min = 1,
		schematic = path .. "/schematics/apple_log.mts",
		flags = "place_center_x",
		rotation = "random",
		spawn_by = "default:dirt_with_grass",
		num_spawn_by = 8,
	})


	-- Jungle tree and log

	minetest.register_decoration({
		name = "default:jungle_tree",
		deco_type = "schematic",
		place_on = {"default:dirt_with_rainforest_litter", "default:dirt"},
		sidelen = 80,
		fill_ratio = 0.02,
		biomes = {"rainforest", "rainforest_shore"},
		y_max = tree_limit,
		y_min = -1,
		schematic = path .. "/schematics/jungle_tree.mts",
		flags = "place_center_x, place_center_z",
		rotation = "random",
		place_offset_y = 1,
	})

	minetest.register_decoration({
		name = "default:jungle_log",
		deco_type = "schematic",
		place_on = {"default:dirt_with_rainforest_litter"},
		place_offset_y = 1,
		sidelen = 80,
		fill_ratio = 0.008,
		biomes = {"rainforest"},
		y_max = tree_limit,
		y_min = 1,
		schematic = path .. "/schematics/jungle_log.mts",
		flags = "place_center_x",
		rotation = "random",
		spawn_by = "default:dirt_with_rainforest_litter",
		num_spawn_by = 8,
	})

	-- Taiga and temperate coniferous forest pine tree, small pine tree and log

	minetest.register_decoration({
		name = "default:pine_tree",
		deco_type = "schematic",
		place_on = {"default:dirt_with_snow", "default:dirt_with_coniferous_litter"},
		sidelen = 80,
		fill_ratio = 0.01,
		biomes = {"taiga", "coniferous_forest"},
		y_max = 31000,
		y_min = 4,
		schematic = path .. "/schematics/pine_tree.mts",
		flags = "place_center_x, place_center_z",
		place_offset_y = 1,
	})


	minetest.register_decoration({
		name = "default:pine_log",
		deco_type = "schematic",
		place_on = {"default:dirt_with_snow", "default:dirt_with_coniferous_litter"},
		place_offset_y = 1,
		sidelen = 80,
		fill_ratio = 0.002,
		biomes = {"taiga", "coniferous_forest"},
		y_max = tree_limit,
		y_min = 4,
		schematic = path .. "/schematics/pine_log.mts",
		flags = "place_center_x",
		rotation = "random",
		spawn_by = {"default:dirt_with_snow", "default:dirt_with_coniferous_litter"},
		num_spawn_by = 8,
	})

	-- Acacia tree and log

	minetest.register_decoration({
		name = "default:acacia_tree",
		deco_type = "schematic",
		place_on = {"default:dirt_with_dry_grass"},
		sidelen = 16,
		noise_params = {
			offset = 0.003,
			scale = 0.004,
			spread = {x = 250, y = 250, z = 250},
			seed = seed:next(),
			octaves = 3,
			persist = 0.66
		},
		biomes = {"savanna"},
		y_max = tree_limit,
		y_min = 1,
		schematic = path .. "/schematics/acacia_tree.mts",
		flags = "place_center_x, place_center_z",
		rotation = "random",
		place_offset_y = 1,
	})

	minetest.register_decoration({
		name = "default:acacia_log",
		deco_type = "schematic",
		place_on = {"default:dirt_with_dry_grass"},
		place_offset_y = 1,
		sidelen = 16,
		noise_params = {
			offset = 0.004,
			scale = 0.001,
			spread = {x = 250, y = 250, z = 250},
			seed = seed:next(),
			octaves = 3,
			persist = 0.66
		},
		biomes = {"savanna"},
		y_max = tree_limit,
		y_min = 1,
		schematic = path .. "/schematics/acacia_log.mts",
		flags = "place_center_x",
		rotation = "random",
		spawn_by = "default:dirt_with_dry_grass",
		num_spawn_by = 8,
	})

	-- Aspen tree and log

	minetest.register_decoration({
		name = "default:aspen_tree",
		deco_type = "schematic",
		place_on = {"default:dirt_with_grass"},
		sidelen = 80,
		fill_ratio = 0.01,
		biomes = {"deciduous_forest", "swamp"},
		y_max = tree_limit,
		y_min = 1,
		schematic = path .. "/schematics/aspen_tree.mts",
		flags = "place_center_x, place_center_z",
		place_offset_y = 1,
	})

	minetest.register_decoration({
		name = "default:aspen_log",
		deco_type = "schematic",
		place_on = {"default:dirt_with_grass"},
		place_offset_y = 1,
		sidelen = 16,
		noise_params = {
			offset = 0.003,
			scale = -0.0008,
			spread = {x = 250, y = 250, z = 250},
			seed = seed:next(),
			octaves = 3,
			persist = 0.66
		},
		biomes = {"deciduous_forest", "swamp"},
		y_max = tree_limit,
		y_min = 1,
		schematic = path .. "/schematics/aspen_log.mts",
		flags = "place_center_x",
		rotation = "random",
		spawn_by = "default:dirt_with_grass",
		num_spawn_by = 8,
	})
	
	-- Palm tree
	minetest.register_decoration({
		deco_type = "schematic",
		place_on = {"default:sand"},
		sidelen = 16,
		noise_params = {
			offset = 0.004,
			scale = 0.004,
			spread = {x = 250, y = 250, z = 250},
			seed = seed:next(),
			octaves = 3,
			persist = 0.66
		},
		y_min = 1,
		y_max = tree_limit,
		schematic = path.."/schematics/palmtree.mts",
		replacements = {["mapgen:palm_leaves_coconut"] = "default:palm_leaves"},
		flags = "place_center_x, place_center_z",
		place_offset_y = 1,
	})
	

	-- Large cactus

	minetest.register_decoration({
		name = "default:large_cactus",
		deco_type = "schematic",
		place_on = {"default:desert_sand"},
		sidelen = 16,
		noise_params = {
			offset = 0.001, -- -0.0003,
			scale = 0.0009,
			spread = {x = 200, y = 200, z = 200},
			seed = seed:next(),
			octaves = 3,
			persist = 0.6
		},
		-- biomes = {"desert"},
		y_max = tree_limit,
		y_min = 4,
		schematic = path .. "/schematics/large_cactus.mts",
		flags = "place_center_x, place_center_z",
		rotation = "random",
		place_offset_y = 1,
	})



	register_grass_decoration(-0.03,  0.09,  5)
	register_snow_grass_decoration(0,  0.1,  5)
	register_dry_grass_decoration(0.01, 0.05,  5)
	register_fern_decoration(3)

	-- Junglegrass

	minetest.register_decoration({
		name = "default:junglegrass",
		deco_type = "simple",
		place_on = {"default:dirt_with_rainforest_litter"},
		sidelen = 80,
		fill_ratio = 0.1,
		biomes = {"rainforest"},
		y_max = 31000,
		y_min = 1,
		decoration = "default:junglegrass",
	})

	-- Dry shrub

	minetest.register_decoration({
		name = "default:dry_shrub",
		deco_type = "simple",
		place_on = {"default:dry_dirt"},
		sidelen = 16,
		noise_params = {
			offset = 0,
			scale = 0.02,
			spread = {x = 200, y = 200, z = 200},
			seed = seed:next(),
			octaves = 3,
			persist = 0.7
		},
		y_max = tree_limit,
		decoration = "default:dry_shrub",
		param2 = 4,
	})

	-- Marram grass

	minetest.register_decoration({
		name = "default:marram_grass",
		deco_type = "simple",
		place_on = {"default:sand"},
		sidelen = 16,
		noise_params = {
			offset = 0,
			scale = 0.2,
			spread = {x = 100, y = 100, z = 100},
			seed = seed:next(),
			octaves = 3,
			persist = 0.7,
		},
		y_max = 6,
		y_min = 2,
		decoration = {
			"default:marram_grass_3",
		},
	})

	-- Coral reef

	minetest.register_decoration({
		name = "default:corals",
		deco_type = "simple",
		place_on = {"default:sand"},
		place_offset_y = -1,
		sidelen = 4,
		noise_params = {
			offset = -2,
			scale = 4,
			spread = {x = 50, y = 50, z = 50},
			seed = seed:next(),
			octaves = 3,
			persist = 0.7,
		},
		biomes = {
			"desert",
			"savanna_shore",
			"rainforest_ocean",
			"sandstone_desert_shore",
		},
		y_max = -5,
		y_min = -15,
		flags = "force_placement",
		decoration = {"default:coral2", "default:coral2", "default:coral"},
	})

	minetest.register_decoration({
		deco_type = "simple",
		place_on = "default:dirt_with_grass",
		sidelen = 20,
		fill_ratio = 0.01,
		decoration = "default:giantgrass",
		height = 1,
		biomes="swamp",
	})

	minetest.register_decoration({
		deco_type = "simple",
		place_on = "default:gravel",
		sidelen = 16,
		fill_ratio = 0.1,
		y_max = 999,
		y_min = 10,
		decoration = "default:river_water_source",
		height = 1,
		biomes="swamp",
	})
	
end



-- Get setting or default
-- Make global for mods to use to register floatland biomes
default.mgv7_floatland_level = minetest.get_mapgen_setting("mgv7_floatland_level") or 1280
default.mgv7_shadow_limit = minetest.get_mapgen_setting("mgv7_shadow_limit") or 1024

minetest.clear_registered_biomes()
minetest.clear_registered_ores()
minetest.clear_registered_decorations()

default.register_biomes()
default.register_ores()
default.register_decorations()
