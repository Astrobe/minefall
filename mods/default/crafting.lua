-- mods/default/crafting.lua
function minetest.craft9from(inp, outp)
	minetest.register_craft {
		output=outp,
		type="shapeless",
		recipe= { inp, inp, inp,  inp, inp, inp,  inp, inp, inp }
	}
end
		
minetest.register_craft({
	output = 'default:wood 4',
	recipe = {
		{'default:tree'},
	}
})

minetest.register_craft({
	output = 'default:junglewood 4',
	recipe = {
		{'default:jungletree'},
	}
})

minetest.register_craft({
	output = 'default:pine_wood 4',
	recipe = {
		{'default:pine_tree'},
	}
})

minetest.register_craft({
	output = 'default:acacia_wood 4',
	recipe = {
		{'default:acacia_tree'},
	}
})

minetest.register_craft({
	output = 'default:aspen_wood 4',
	recipe = {
		{'default:aspen_tree'},
	}
})


minetest.register_craft({
	output = 'default:palm_wood 4',
	recipe = {
		{"default:palm_tree"},
	}
})

minetest.register_craft({
	output = 'default:sign_wall_steel',
	recipe = {
		{'', 'default:skeleton_key', ''},
		{'', 'default:tin_ingot', ''},
		{'', 'group:stick', ''},
	}
})

minetest.register_craft({
	output = 'default:pick_steel',
	recipe = {
		{'default:steel_ingot', 'default:steel_ingot', 'default:steel_ingot'},
		{'', 'group:stick', ''},
		{'', 'group:stick', ''},
	}
})

minetest.register_craft({
	output = 'default:pick_bronze',
	recipe = {
		{'default:bronze_ingot', 'default:bronze_ingot', 'default:bronze_ingot'},
		{'', 'group:stick', ''},
		{'', 'group:stick', ''},
	}
})

minetest.register_craft({
	output = 'default:shovel_steel',
	recipe = {
		{'default:steel_ingot'},
		{'group:stick'},
		{'group:stick'},
	}
})

minetest.register_craft({
	output = 'default:shovel_bronze',
	recipe = {
		{'default:bronze_ingot'},
		{'group:stick'},
		{'group:stick'},
	}
})

-- Axes
-- Recipes face left to match appearence in textures and inventory

minetest.register_craft({
	output = 'default:axe_steel',
	recipe = {
		{'default:steel_ingot', 'default:steel_ingot'},
		{'default:steel_ingot', 'group:stick'},
		{'', 'group:stick'},
	}
})

minetest.register_craft({
	output = 'default:axe_bronze',
	recipe = {
		{'default:bronze_ingot', 'default:bronze_ingot'},
		{'default:bronze_ingot', 'group:stick'},
		{'', 'group:stick'},
	}
})

minetest.register_craft({
	output = 'default:sword_steel',
	recipe = {
		{'default:steel_ingot'},
		{'default:steel_ingot'},
		{'group:stick'},
	}
})

minetest.register_craft({
	output = 'default:sword_bronze',
	recipe = {
		{'default:bronze_ingot'},
		{'default:bronze_ingot'},
		{'group:stick'},
	}
})

minetest.register_craft({
	output = 'default:skeleton_key',
	recipe = {
		{'default:gold_ingot'},
	}
})

minetest.register_craft( {
	type = "shapeless",
	output = "default:chest_locked",
	recipe = { "default:chest", "default:skeleton_key"}
})


minetest.register_craft({
	output = 'default:steelblock',
	recipe = {
		{'default:steel_ingot', 'default:steel_ingot', 'default:steel_ingot'},
		{'default:steel_ingot', 'default:steel_ingot', 'default:steel_ingot'},
		{'default:steel_ingot', 'default:steel_ingot', 'default:steel_ingot'},
	}
})

minetest.register_craft({
	output = 'default:steel_ingot 9',
	recipe = {
		{'default:steelblock'},
	}
})

minetest.register_craft({
	output = 'default:copperblock',
	recipe = {
		{'default:copper_ingot', 'default:copper_ingot', 'default:copper_ingot'},
		{'default:copper_ingot', 'default:copper_ingot', 'default:copper_ingot'},
		{'default:copper_ingot', 'default:copper_ingot', 'default:copper_ingot'},
	}
})
minetest.register_craft({
	output = 'default:copper_ingot 9',
	recipe = {
		{'default:copperblock'},
	}
})

minetest.register_craft({
	output = "default:tinblock",
	recipe = {
		{"default:tin_ingot", "default:tin_ingot", "default:tin_ingot"},
		{"default:tin_ingot", "default:tin_ingot", "default:tin_ingot"},
		{"default:tin_ingot", "default:tin_ingot", "default:tin_ingot"},
	}
})

minetest.register_craft({
	output = "default:tin_ingot 9",
	recipe = {
		{"default:tinblock"},
	}
})

minetest.register_craft({
	output = "default:bronze_ingot 4",
	type="shapeless",
	recipe =
		{"default:copper_ingot", "default:tin_ingot", "default:copper_ingot", "default:copper_ingot"},
})

minetest.register_craft({
	output = 'default:bronzeblock',
	recipe = {
		{'default:bronze_ingot', 'default:bronze_ingot', 'default:bronze_ingot'},
		{'default:bronze_ingot', 'default:bronze_ingot', 'default:bronze_ingot'},
		{'default:bronze_ingot', 'default:bronze_ingot', 'default:bronze_ingot'},
	}
})

minetest.register_craft({
	output = 'default:bronze_ingot 9',
	recipe = {
		{'default:bronzeblock'},
	}
})

minetest.register_craft({
	output = 'default:goldblock',
	recipe = {
		{'default:gold_ingot', 'default:gold_ingot', 'default:gold_ingot'},
		{'default:gold_ingot', 'default:gold_ingot', 'default:gold_ingot'},
		{'default:gold_ingot', 'default:gold_ingot', 'default:gold_ingot'},
	}
})

minetest.register_craft({
	output = 'default:gold_ingot 9',
	recipe = {
		{'default:goldblock'},
	}
})

minetest.register_craft({
	output = 'default:book',
	recipe = {
		{'default:paper', 'default:paper', ''},
		{'default:paper', 'default:paper', ''},
		{'default:paper', 'default:paper', ''},
	}
})

minetest.register_craft({
	output = 'default:book',
	type="shapeless",
	recipe = { "default:mese_crystal_fragment", "default:book_written" }
})


minetest.register_craft({
	output = 'default:mese',
	recipe = {
		{'default:mese_crystal', 'default:mese_crystal', 'default:mese_crystal'},
		{'default:mese_crystal', 'default:mese_crystal', 'default:mese_crystal'},
		{'default:mese_crystal', 'default:mese_crystal', 'default:mese_crystal'},
	}
})

minetest.register_craft({
	output = 'default:mese_crystal 9',
	recipe = {
		{'default:mese'},
	}
})

minetest.register_craft({
	output = 'default:mese_crystal_fragment 9',
	recipe = {
		{'default:mese_crystal'},
	}
})

minetest.craft9from("default:mese_crystal_fragment", "default:mese_crystal");

minetest.register_craft {
	output="default:acacia_bush_stem",
	type="cooking",
	recipe="default:cactus",
	burntime=5,
}
--
-- Cooking recipes
--

minetest.register_craft({
	type = "cooking",
	output = "default:steel_ingot",
	recipe = "default:iron_lump",
	cooktime = 100,
})

minetest.register_craft({
	type = "cooking",
	output = "default:copper_ingot",
	recipe = "default:copper_lump",
	cooktime = 80,
})

minetest.register_craft({
	type = "cooking",
	output = "default:tin_ingot",
	recipe = "default:tin_lump",
	cooktime = 70,
})

minetest.register_craft({
	type = "cooking",
	output = "default:gold_ingot",
	recipe = "default:gold_lump",
	cooktime = 60,
})


minetest.register_craft({
	type = 'cooking',
	output = 'default:gold_ingot',
	recipe = 'default:skeleton_key',
	cooktime = 5,
})

minetest.register_craft({
	type = 'cooking',
	output = 'default:gold_ingot',
	recipe = 'default:key',
	cooktime = 5,
})


--
-- Fuels
--

-- Burn times determined by color.
minetest.register_craft({
	type = "fuel",
	recipe = "default:aspen_tree",
	burntime = 20, })

minetest.register_craft({
	type = "fuel",
	recipe = "default:pine_tree",
	burntime = 24,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:palm_tree",
	burntime = 28, })

minetest.register_craft({
	type = "fuel",
	recipe = "default:tree",
	burntime = 32,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:acacia_tree",
	burntime = 36,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:jungletree",
	burntime = 40,
})


-- Support use of group:wood, includes default:wood which has the same burn time
minetest.register_craft({
	type = "fuel",
	recipe = "default:wood",
	burntime = 8,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:palm_wood",
	burntime = 7,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:acacia_wood",
	burntime = 9,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:pine_wood",
	burntime = 6,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:aspen_wood",
	burntime = 5,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:junglewood",
	burntime = 10,
})

minetest.register_craft({
	type = "fuel",
	recipe = "group:stick",
	burntime = 6,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:coal_lump",
	burntime = 41,
})

minetest.register_craft({
	type = "fuel",
	recipe = "default:coalblock",
	burntime = 430,
})

local _=function (item, mat)
	minetest.register_craft {
		output= item,
		recipe = { {mat, mat, mat}, {mat, "group:stick", mat}, {mat, mat, mat} }
	}
end
_("default:bronze_fence", "default:bronze_ingot")
_("default:tin_fence 2", "default:tin_ingot")
_("default:steel_fence 4", "default:steel_ingot")
_("default:gold_fence 8", "default:gold_ingot")

minetest.craft9from("default:obsidian_shard", "default:obsidian_glass")

minetest.register_craft {
	output="default:cobble",
	type="cooking",
	recipe="default:mossycobble",
	cooktime=7,
}

