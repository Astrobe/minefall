-- Author: Astrobe
-- licence: CC-BY-SA 4.0
-- Version 1.0 


minetest.override_item("dungeon_crates:crate",
{
	after_place_node=function(pos, placer, itemstack, pointed_thing)
		if not placer then return false end
		local name=placer:get_player_name()
		if not name then return false end
		local reward=1
		rp.award(name, reward) 
		minetest.chat_send_player(name, minetest.colorize("green", string.format("[Minefall] +%d RP for placing a crate (you have now %d)", reward,rp.get_player_count(name))))
		return false
	end,
	on_dig=function(pos, node, digger)
		if not digger then return minetest.node_dig(pos, node, digger) end
		local name=digger:get_player_name()
		local tax=-1
		if rp.get_player_count(name) > 1 then
			rp.award(name, tax) 
		else
			minetest.chat_send_player(name, "You don't have enough RP to take crates.")
			return nil
		end
		minetest.chat_send_player(name, minetest.colorize("orange", string.format("[Minefall] %d RP for taking a crate (you have now %d)", tax, rp.get_player_count(name))))
		return minetest.node_dig(pos, node, digger)
	end
})

--[[
-- This provides one less reason to repopulate crates in ruins and
-- dungeons, unfortunately.
local c="dungeon_crates:crate"
minetest.register_craft {
	type="shapeless",
	output="rp:coin",
	recipe={c,c,c, c,c,c, c,c,c}
}
]]

minetest.override_item("default:obsidian_shard", {
	liquids_pointable=true,
	on_use=function(itemstack, player, pointed_thing)
		if not player or player:get_attach() or not pointed_thing then return end
		if pointed_thing.type ~="node" then return end
		local pname=player:get_player_name()
		local pos=pointed_thing.under
		local node=minetest.get_node(pos)
		-- if bones.get_mode_for(player)~="drop" then return itemstack end
		if minetest.is_protected(pos, pname) or
			minetest.get_item_group(node.name, "nopicker")~=0 then
			minetest.chat_send_player(pname, "This block cannot be picked.")
			return
		end


		--[[
		local inv=player:get_inventory()
		if inv:room_for_item("main", node.name) then
			inv:add_item("main", node.name)
			minetest.remove_node(pos)
			itemstack:take_item()
		else
			minetest.chat_send_player(pname, "But your inventory is full !")
		end
		return itemstack
		]]
		minetest.add_item(pos, node.name)
		minetest.remove_node(pos)
		itemstack:take_item()
		return itemstack
	end
})

minetest.override_item("more_mese:mese_crystal_fragment_green", {
	on_use=function(itemstack, player, pointed_thing)
		local pos=pointed_thing.under
		if not pos or minetest.get_node(pos).name ~= "dungeon_crates:crate" then return end
		minetest.remove_node(pos)
		local items=minetest.get_node_drops("dungeon_crates:crate", nil)
		minetest.add_particlespawner({
			amount = 100,
			time = 0.25,
			pos=pos,
			radius=0.75,
			attract={kind="point", origin=pos, strength=2},
			node={name="dungeon_crates:crate"},
			glow=14,
			collisiondetection=false,
		})
		pos.y=pos.y-0.6
		for _, item in ipairs(items) do minetest.add_item(pos, item) end -- we know it's just one item though.
		minetest.sound_play("default_place_node_hard", {pos=pos, max_hear_distance=8}, true)
		itemstack:take_item()
		return itemstack
	end
})

--[[
minetest.register_craft{
	output="dungeon_crates:crate",
	type="shapeless",
	recipe={"more_mese:mese_crystal_fragment_green","craft_table:simple"}
}
]]

