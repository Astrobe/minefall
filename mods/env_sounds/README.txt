This is the env_sounds mod that comes with MTG, augmented with furnace and fire sounds.
You'll need to make the corresponding changes in default and fire mods (basically remove their sound handling code).

Author: Astrobe
License (code): MIT

Original README below.

============================================

Minetest Game mod: env_sounds
=============================
See license.txt for license information.

Authors of source code
----------------------
paramat (MIT)

Authors of media (sounds)
-------------------------
Yuval (CC0 1.0)
https://freesound.org/people/Yuval/sounds/197023/
  env_sounds_water.*.ogg

Halion (CC0 1.0)
https://freesound.org/people/Halion/sounds/17785/
  env_sounds_lava.*.ogg
