--[[
	Mod Craft Table for Minetest
	Copyright (C) 2019 BrunoMine (https://github.com/BrunoMine)
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
	
	Simple craft table
  ]]

-- Craft Table
minetest.register_node("craft_table:simple", {
	description = "Craft Table",
	tiles = {
		"xdecor_workbench_top.png","xdecor_workbench_top.png",
		"xdecor_workbench_front.png"
		--[[ "xdecor_workbench_sides.png", "xdecor_workbench_sides.png",
		"xdecor_workbench_front.png", "xdecor_workbench_front.png"]]
	},
	paramtype2 = "facedir",
	groups = {oddly_breakable_by_hand=2},
	legacy_facedir_simple = true,
	is_ground_content = false,
	sounds = default.node_sound_wood_defaults(),
	on_rightclick = craft_table.on_rightclick,
})

-- Change recipes from other mods
if minetest.registered_nodes["xdecor:workbench"] then
	-- Remove old recipe
	minetest.clear_craft({output = 'xdecor:workbench'})
	-- Register new recipe
	minetest.register_craft({ 
		output = 'xdecor:workbench',
		recipe = {
			{'', 'group:wood', ''},
			{'default:steel_ingot', 'craft_table:craft_table', 'default:steel_ingot'},
			{'', 'group:wood', ''},
		}
	})
end

minetest.register_craft({ 
	output = 'default:chest',
	type="shapeless",
	recipe = {
		"craft_table:simple", "group:wood"
	}
})
minetest.register_craft({
	output = 'default:furnace',
	type="shapeless",
	recipe = {
		"craft_table:simple", "default:cobble"
	}
})
--[[
minetest.register_craft({ 
	output = 'craft_table:simple',
	type="shapeless",
	recipe = {
		"default:chest", "default:flint"
	}
})
]]
