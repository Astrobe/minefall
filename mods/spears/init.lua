minetest.register_tool("mf_spears:spear_bronze", {
	description = "Bronze spear",
	inventory_image = "spears_spear_bronze.png",
	wield_image = "spears_spear_bronze.png^[transform4",
	range=4,
	tool_capabilities = {
		full_punch_interval = 0.5,
		max_drop_level=1,
		groupcaps={ },
		damage_groups = {fleshy=2},
		punch_attack_uses = 256,
	},
	sound = {breaks = "default_tool_breaks"},
})
minetest.register_tool("mf_spears:spear_steel", {
	description = "Steel spear",
	inventory_image = "spears_spear_steel.png",
	wield_image = "spears_spear_steel.png^[transform4",
	range=4,
	tool_capabilities = {
		full_punch_interval = 0.5,
		max_drop_level=1,
		groupcaps={ },
		damage_groups = {fleshy=3},
		punch_attack_uses = 512,
		},
	sound = {breaks = "default_tool_breaks"},
})

minetest.register_craft({
	output = "mf_spears:spear_bronze",
	recipe = {
		{ "", "","default:bronze_ingot"},
		{"","group:stick",""},
		{"group:stick", "",""},
	}
})

minetest.register_craft({
	output = "mf_spears:spear_steel",
	recipe = {
		{ "", "","default:steel_ingot"},
		{"","group:stick",""},
		{"group:stick", "",""},
	}
})
