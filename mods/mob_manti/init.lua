mobs:register_mob("mob_manti:mini_manticore", {
	type = "monster",
	passive = false,
	attacks_monsters=false,
	damage = 2,
	reach = 2,
	attack_type = "dogfight",
	hp_max = 25,
	collisionbox = {-0.4, -0.25, -0.4, 0.4, 0.9, 0.4},
	visual = "mesh",
	mesh = "manticore.b3d",
	textures = {
		{"manticore.png"},
	},
	blood_amount = 80,
	blood_texture = "horror_blood_effect.png",
	visual_size = {x=1.5, y=1.5},
	makes_footstep_sound = true,
	walk_velocity = 2,
	run_velocity = 3.75,
	floats=1,
	jump = true,
	jump_height=6,
	env_dmg= { ["default:ice"]=2 },
	view_range = 24,
	animation = {
		speed_normal = 10,
		speed_run = 20,
		walk_start = 1,
		walk_end = 11,
		stand_start = 11,
		stand_end = 26,
		run_start = 1,
		run_end = 11,
		punch_start = 11,
		punch_end = 26,
	},

	env_alt = {
		["default:junglegrass"]={ "default:junglesapling", 30},
		["default:dry_grass_5"]={ "default:acacia_bush_sapling", 30},
		["default:acacia_bush_sapling"]={"default:acacia_sapling", 10},
		["default:dry_shrub"]={ "default:large_cactus_seedling", 10},
		["default:grass_5"]={ "default:blueberry_bush_sapling", 30},
		["farming:wheat"]={"air",4},
		["farming:cotton"]={"air",4},
		["default:marram_grass_3"]={ "default:palmtree_sapling", 20},
	},

	drops = {
		{name = "farming:seed_cotton", chance=2},
		{name = "farming:seed_cotton", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
	},
})

mobs:register_spawn("mob_manti:mini_manticore", {"default:dry_grass_5", "default:dry_shrub", "default:junglegrass" }, 20, 0, 750, 3, 999)

