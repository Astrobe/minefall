exchange_shop = {}
exchange_shop.storage_size = 5 * 4
exchange_shop.shopname = "exchange_shop:shop"

local modpath = minetest.get_modpath("exchange_shop")

-- Currency migrate options
exchange_shop.migrate = {
	use_lbm = false,
	-- ^ Runs once on each unique loaded mapblock
	on_interact = true
	-- ^ Converts shop nodes "on the fly"
}


minetest.register_alias("bitchange:shop", "exchange_shop:shop")
dofile(modpath .. "/shop_functions.lua")
dofile(modpath .. "/shop.lua")

