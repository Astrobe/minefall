This is a slightly modified version of the original mod. AFAIR, I changed /channel into /chat.

Copyright: Astrobe
License: CC0

Original README below.

---

channels
========

License: WTFPL


This modification for Minetest adds a channel feature.
You can join and leave channels to create:
- Teamchats
- Silence because nobody else in the chat
- Ignoring people
- Annoy people which think, you are evil because you don't answer

How to use
----------

There is one chat command to manage everything.


Online players in your channel:  /channel online

Join or switch your channel:     /channel set <channel>

Leave the current channel:       /channel leave

