mobs:register_mob("mob_whale:whale", {
	type = "animal",
	passive = true,
	-- runaway=true,
	-- runaway_from={"monster"},
	reach = 1,
	damage = 2,
	attack_type = "dogfight",
	hp_min = 999,
	hp_max = 999,
	collisionbox = {-3, -1.5, -3, 3, 1.5, 3},
	visual = "mesh",
	mesh = "whale.b3d",
	textures = {
		{"mobs_m_whale.png"},
	},
	rotate = 180,
	visual_size = {x=1.5, y=0.9, z=0.9},
	makes_footstep_sound = false,
	walk_velocity = 3,
	walk_chance = 90,
	run_velocity = 3,
	jump = false,	
	stepheight = 1.5,
	fall_damage = 0,
	fall_speed = -6,
	fly = true,
	fly_in = "air",
	water_damage = 0,
	view_range = 10,
	env_alt={
		-- colbox is so big that sometimes whales get stuck in nodes
		["default:snowblock"]={"air", 1},
		["default:cave_ice"]={"air", 1}
	},
	sounds = {
		random = "whale_1",
		distance = 64,
	},
	animation = {
		speed_normal = 0.1,
		speed_run = 0.5,
		walk_start = 2,
		walk_end = 28,
		stand_start = 30,
		stand_end = 50,
		run_start = 2,
		run_end = 28,

	},
	on_rightclick = function(self, clicker)
		-- make sure player is clicking
		if not clicker or not clicker:is_player() then
			return
		end
		if self.driver and clicker == self.driver then
			mobs.detach(clicker, {x = 0, y = 3, z = 0})
			self.lifespan=300
			--clicker:set_properties { zoom_fov=0 }
		elseif not self.driver then
		-- we need to "undo" the rescale of the whale, as the attached entity inherit it.
		self.driver_scale = {x = 2/3, y = 1, z=1}
			self.driver_attach_at= {x=0, y=14, z=-10}
			self.driver_eye_offset = {x = 0, y = 15, z = 20}
			self.player_rotation = {x = 0, y = 180, z = 0}
			mobs.attach(self, clicker)
			--clicker:set_properties { zoom_fov=29 }
		end
	end,
	do_punch=function(self, hitter)
		if not hitter or not hitter:is_player() then
			return
		end
		if self.driver then
			self.object:set_yaw(hitter:get_look_horizontal()-self.rotate)
			mobs.force_walk(self, hitter:get_pos())
			return false
		end
	end
})

mobs:spawn{
	name="mob_whale:whale",
	nodes={"default:snowblock"},
	interval= 61,
	chance= 3000,
	active_object_count=2,
	min_height=60, -- upper_limit in mapgen
	max_height=999,
	min_light=14, -- avoid spawning in caves or weird places
	--day_toggle=false,
}

mobs:register_egg("mob_whale:whale", "Whale (right-click the ground to use it)", "default_stone.png", 1, true)
