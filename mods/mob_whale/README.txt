Whale mob extracted from the Farlands MT Game.
The "riding" property doesn't use Mob Redo's "mount" feature, but on a hack
inserted in the code of Mob Redo. The whale will "tend" to go in the direction
the player is looking.

Author: Astrobe
License for code: Same as Minetest v5.
License for the rest: see license.txt (Farland's original license).

