-- mf_lamps
-- Author: Astrobe
-- License (code): MIT
-- License (textures): CC BY-SA 3.0 (mese post light, extracted from MTG)

minetest.register_node(":default:meselamp", {
	description = "Mese Lamp",
	drawtype = "glasslike",
	tiles = {"default_meselamp.png"},
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	groups = {cracky = 2},
	sounds = default.node_sound_glass_defaults(),
	light_source = 14,
})

minetest.register_craft {
	output = 'default:meselamp',
	type="shapeless",
	recipe = {"group:lamp", "group:lamp", "default:glass"},
}

local post_texture = "default_gold_block.png^default_mese_post_light_side.png^[makealpha:0,0,0"

minetest.register_node("mf_lamps:gold",
{
	description="Gold lamp",
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{-2 / 16, -8 / 16, -2 / 16, 2 / 16, 8 / 16, 2 / 16},
		},
	},
	paramtype = "light",
	paramtype2="wallmounted",
	tiles = {"default_gold_block.png", "default_gold_block.png", post_texture, post_texture, post_texture, post_texture},
	use_texture_alpha = "opaque",
	light_source = 10,
	sunlight_propagates = true,
	is_ground_content = false,
	groups = {dig_immediate = 3, lamp=1, attached_node=1, protected=1},
	sounds = default.node_sound_wood_defaults(),

})

minetest.register_craft({
	output = "mf_lamps:gold",
	type="shapeless",
	recipe = { "flowers:mushroom_red", "default:gold_ingot", "default:paper"}
})

function default.register_mesepost(name, def)
	minetest.register_craft({
		output = name,
		type="shapeless",
		recipe = { "flowers:mushroom_red", "default:tin_ingot", "default:paper", def.material}
	})

	local post_texture = def.texture .. "^default_mese_post_light_side.png^[makealpha:0,0,0"
	-- Allow almost everything to be overridden
	local default_fields = {
		-- wield_image = post_texture,
		drawtype = "nodebox",
		node_box = {
			type = "fixed",
			fixed = {
				{-2 / 16, -8 / 16, -2 / 16, 2 / 16, 8 / 16, 2 / 16},
			},
		},
		paramtype = "light",
		paramtype2="wallmounted",
		tiles = {def.texture, def.texture, post_texture, post_texture, post_texture, post_texture},
		use_texture_alpha = "opaque",
		light_source = 10,
		sunlight_propagates = true,
		is_ground_content = false,
		groups = {dig_immediate = 3, flammable = 2, lamp=1, attached_node=1},
		sounds = default.node_sound_wood_defaults(),
	}
	for k, v in pairs(default_fields) do
		if def[k] == nil then
			def[k] = v
		end
	end

	def.texture = nil
	def.material = nil

	minetest.register_node(name, def)
end


default.register_mesepost("mf_lamps:apple_wood", {
	description = "Apple wood lamp",
	texture = "default_wood.png",
	material = "stairs:stair_wood",
})

default.register_mesepost("mf_lamps:acacia_wood", {
	description = "Acacia wood lamp",
	texture = "default_acacia_wood.png",
	material = "stairs:stair_acacia_wood",
})

default.register_mesepost("mf_lamps:junglewood", {
	description = "Jungle wood lamp",
	texture = "default_junglewood.png",
	material = "stairs:stair_junglewood",
})

default.register_mesepost("mf_lamps:pine_wood", {
	description = "Pine wood lamp",
	texture = "default_pine_wood.png",
	material = "stairs:stair_pine_wood",
})

default.register_mesepost("mf_lamps:aspen_wood", {
	description = "Aspen wood lamp",
	texture = "default_aspen_wood.png",
	material = "stairs:stair_aspen_wood",
})

default.register_mesepost("mf_lamps:palm_wood", {
	description = "Palmtree wood lamp",
	texture = "mapgen_palm_wood.png",
	material = "stairs:stair_palm_wood",
})
   
minetest.register_node(":mf_decor:tinlamp", {
	description = "Tin Lamp",
	tiles = {"default_meselamp.png^(default_tin_block.png^[mask:td_mask.png)"},
	paramtype = "light",
	sunlight_propagates = true,
	is_ground_content = false,
	groups = {cracky = 2, protected=1},
	sounds = default.node_sound_metal_defaults(),
	light_source = 12,
})

minetest.register_craft {
	output = 'mf_decor:tinlamp',
	type="shapeless",
	recipe = {
		"default:tin_ingot", "default:tin_ingot", "default:tin_ingot", "default:tin_ingot", "group:lamp",
	}
}

minetest.register_node("mf_lamps:glowpaint", {
	description = "Glowing pasta.\nOnce placed, one can only erase it.",
	--tiles = {"magic_wand_noogberry_orange.png"},
	tiles = {"default_snowball.png^[multiply:khaki"},
	--inventory_image="default_snowball.png",
	-- wield_image="default_snowvall.png",
	drawtype= "signlike",
	groups={attached_node=1},
	paramtype = "light",
	paramtype2="wallmounted",
	sunlight_propagates = true,
	walkable=false,
	buildable_to=true,
	pointable=false,
	light_source = 5,
	drop="",
})

minetest.register_craft {
	output = 'mf_lamps:glowpaint 4',
	type="shapeless",
	recipe = { "flowers:mushroom_red", "flowers:mushroom_red", "farming:flour" }
}

