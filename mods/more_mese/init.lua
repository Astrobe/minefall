-- Author: Astrobe
-- Licence: same as Minetest v5.0.

minetest.register_craftitem("more_mese:mese_crystal_red", {
	description = "Red Mese Crystal.\nCan be traded for Greater Armor.",
	inventory_image = "default_mese_crystal_white.png^[multiply:red",
})

minetest.register_craftitem("more_mese:mese_crystal_fragment_red", {
	description = "Red Mese Crystal Fragment\nDropped by Abyss spiders (-300 m).",
	inventory_image = "default_mese_crystal_fragment_white.png^[multiply:red",
})

minetest.register_craftitem("more_mese:mese_crystal_green", {
	description = "Green Mese Crystal",
	inventory_image = "default_mese_crystal_white.png^[multiply:green",
})

minetest.register_craftitem("more_mese:mese_crystal_fragment_green", {
	description = "Green Mese Crystal Fragment\nDropped by giant green wasps.\nOpens lootboxes for free.",
	inventory_image = "default_mese_crystal_fragment_white.png^[multiply:green",
})

minetest.register_craftitem("more_mese:mese_crystal_grey", {
	description = "Grey Mese Crystal",
	inventory_image = "default_mese_crystal_white.png^[multiply:grey",
})

minetest.register_craftitem("more_mese:mese_crystal_fragment_grey", {
	description = "Grey Mese Crystal Fragment\n Dropped by rock crabs (oceans).\nGive to trader to teleport to your second waypoint.",
	inventory_image = "default_mese_crystal_fragment_white.png^[multiply:grey",
})

minetest.register_craftitem("more_mese:mese_crystal_cyan", {
	description = "Blue Mese Crystal",
	inventory_image = "default_mese_crystal_white.png^[multiply:cyan",
})

minetest.register_craftitem("more_mese:mese_crystal_fragment_cyan", {
	description = "Blue Mese Crystal Fragment\nDropped by ice spiders (snowy locations).",
	inventory_image = "default_mese_crystal_fragment_white.png^[multiply:cyan",
})

minetest.register_craftitem("more_mese:mese_crystal_orange", {
	description = "Orange Mese Crystal",
	inventory_image = "default_mese_crystal_white.png^[multiply:orange",
})

minetest.register_craftitem("more_mese:mese_crystal_fragment_orange", {
	description = "Orange Mese Crystal Fragment. Give to trader to get a whale egg.\nDropped by the orange wasp (deserts).",
	inventory_image = "default_mese_crystal_fragment_white.png^[multiply:orange",
})

local function _(inp, outp)
	minetest.register_craft({
		output = outp,
		recipe = {
		{inp,inp,inp},
		{inp,inp,inp},
		{inp,inp,inp},
		}
		})
	minetest.register_craft({
			output = inp.." 9",
			recipe = {
			{outp},
			}
			})

	end
_("more_mese:mese_crystal_fragment_red", "more_mese:mese_crystal_red")
_("more_mese:mese_crystal_fragment_green", "more_mese:mese_crystal_green")
_("more_mese:mese_crystal_fragment_grey", "more_mese:mese_crystal_grey")
_("more_mese:mese_crystal_fragment_cyan", "more_mese:mese_crystal_cyan")
_("more_mese:mese_crystal_fragment_orange", "more_mese:mese_crystal_orange")
