anchorstone = {}

local S = minetest.get_translator()

local glass_sound = default.node_sound_glass_defaults()

local preserved_source_key = "preserved_source"
local anchorstone_name = "anchorstone:anchorstone"
local anchorstone_displaced_name = "anchorstone:displaced"


local preserve_anchorstone_meta = function(pos, oldnode, oldmeta, drops)
	local preserved_source = oldmeta[preserved_source_key]
	if preserved_source == nil then
		preserved_source = minetest.pos_to_string(pos)
	end
	for index, itemstack in pairs(drops) do
		if itemstack:get_name() == anchorstone_displaced_name then
			local itemstack_meta = itemstack:get_meta()
			itemstack_meta:set_string(preserved_source_key, preserved_source)
			itemstack_meta:set_string("description", oldmeta["description"])
		end
	end
end

minetest.register_node(anchorstone_name, {
	description = S("Inactive Dungeon beacon.\nPick up with a orange mese crystal and place it somewhere else."),
	drawtype = "glasslike",
	tiles = {"dungeon_beacon.png^[noalpha"},
	--use_texture_alpha = "blend",
	is_ground_content = true,
	node_box = {type="regular"},
	sounds = glass_sound,
	groups = { nopicker=1, noblast=1},
	drop = anchorstone_displaced_name,
	preserve_metadata = preserve_anchorstone_meta,
	on_rightclick=function(pos, node, player, obj)
		if player==nil or obj==nil then return obj end
		if obj:get_name()=="more_mese:mese_crystal_orange" and not minetest.is_protected(pos, player:get_player_name()) then
			minetest.node_dig(pos, node, player)
			obj:take_item()
		end
		return obj
	end,
})


minetest.register_node(anchorstone_displaced_name, {
	description = S("Active Dungeon beacon\nFeed a grey mese fragment to teleport to its original location.\nFeed a compass to set your second waypoint to its original location."),
	drawtype = "glasslike",
	tiles = {"dungeon_beacon.png^[noalpha"},
	-- use_texture_alpha = "blend",
	light_source=7,
	is_ground_content = true,
	node_box = {type="regular"},
	sounds = glass_sound,
	groups = {cracky = 3, protected=1},
	stack_max = 1,

	preserve_metadata = preserve_anchorstone_meta,
	
	-- Called after constructing node when node was placed using
	-- minetest.item_place_node / minetest.place_node.
	-- If return true no item is taken from itemstack.
	-- `placer` may be any valid ObjectRef or nil.
	after_place_node = function(pos, placer, itemstack, pointed_thing)
		local item_meta = itemstack:get_meta()
		local preserved_source = item_meta:get(preserved_source_key)
		if preserved_source and minetest.string_to_pos(preserved_source) then
			local node_meta = minetest.get_meta(pos)
			node_meta:set_string(preserved_source_key, preserved_source)
			node_meta:mark_as_private(preserved_source_key)
		else
			-- metadata was lost somehow, alas.
			minetest.set_node(pos, {name = anchorstone_name})
		end
	end,
	
	on_rightclick=function(pos, node, player, obj)
		if player==nil then return obj end
		local name=player:get_player_name()
		if not mf.clan_ok(name, pos) then return obj end
		local offer=obj:get_name()
		if offer=="more_mese:mese_crystal_fragment_grey" or (offer=="default:mese_crystal_fragment" and mf.is_keymaster(name)) then
			local meta=minetest.get_meta(pos)
			if not meta then return false end
			local text=meta:get(preserved_source_key)
			if not text then return false end
			local target=minetest.string_to_pos(text)
			if not target then return false end
			target.y=target.y+1
			player:set_pos(target)
			minetest.sound_play("nether_portal_teleport", {max_hear_distance=16}, true)
			obj:take_item()
		elseif offer=="compass:compass" then
			local meta=minetest.get_meta(pos)
			if not meta then return false end
			local text=meta:get(preserved_source_key)
			if not text then return false end
			local target=minetest.string_to_pos(text)
			if not target then return false end
			compass.set_wp2(name, target)
			obj:take_item()
		end
		return obj
	end,
})

minetest.register_craft {
	type="shapeless",
	output=anchorstone_name,
	recipe={"more_mese:mese_crystal_red", anchorstone_displaced_name}
}
