skins= { skins= {} }

minetest.register_on_joinplayer(function(player)
	local name=player:get_player_name()
	skins.skins[name]="mobs_npc"..((string.len(name)+string.byte(name)*2)%10+1)
end)

minetest.register_chatcommand("skin", {
	params="",
	description="Information about skins",
	func=function(name, param)
		return nil, "The length of the player name determines the skin of the character. Names with an odd number of letters give a female skin."
	end
})

