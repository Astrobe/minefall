-- mf_airtank
-- Author: Astrobe
-- license (code): MIT
-- Textures: taken from Facedeer's "Airtanks" mod, see license.txt


minetest.register_tool("mf_airtank:airtank",
{
	description="Airtank\nCreates a lit air bubble in water. ~60 uses.\nGet: from trader, for a mese crystal.",
	inventory_image="airtanks_airtank_two.png",
	on_use=function(stack, user)
		if user then
			local pos=user:get_pos()
			pos.y=pos.y+1
			if minetest.get_node(pos).name=="default:water_source" then
				minetest.set_node(pos, {name="wands:lit_water"})
			end
			stack:add_wear_by_uses(64)
		end
		return stack
	end
})
