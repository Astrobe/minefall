Assets come from "mob_spider2".

Author: Astrobe
Licences (code): Same as Minetest v5.
Licences (model, textures): see original readme below (note: mobs_spider_blue.png modified by me, same license).

----------

# mob_spider2
Spider mob for Minetest

Licensed as CC by SA 3.0  
For more details:  
http://creativecommons.org/licenses/by-sa/3.0/  
  
Sound taken from Mobs Redo.
  
Animations:  
  
walking: 1 - 21  
attacking: 25 - 45  
  
Static positions at frame:  
  
0 - rest position  
  
  
Preview:
![Image Spider mob](https://raw.githubusercontent.com/AspireMint/mob_spider2/master/preview.png)
