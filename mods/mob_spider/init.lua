mobs:register_arrow("mob_spider:lava_arrow", {
	visual = "sprite",
	visual_size = {x = 1, y = 1},
	textures = {"magic_wand_noogberry.png"},
	velocity = 9,
	glow=5,

	hit_player = function(self, player)
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 2},
		}, nil)
	end,

	hit_mob = function(self, player)
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 2},
		}, nil)
	end,
	hit_node=function(self)
		local p=minetest.find_node_near(self.lastpos or self.object:getpos(), 1, {name="air"})
		if not p then return nil end
		minetest.add_entity(p, "mob_spider:lava_ling")
	end,
})

mobs:register_mob("mob_spider:lava", {
	type = "monster",
	passive = false,
	attack_type = "shoot",
	arrow="mob_spider:lava_arrow",
	shoot_interval=1.5,
	reach = 2.5,
	damage = 3,
	hp_max = 60,
	collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.1, 0.5},
	visual = "mesh",
	mesh = "spider2.b3d",
	textures = {
		{"spider2-2x-robot-mese.png"},
	},
	visual_size = {x = 0.75, y = 1},
	makes_footstep_sound = false,
	sounds = {
		attack="mobs_spider",
	},
	walk_velocity = 1,
	run_velocity = 3,
	jump = true,
	jump_height=9,
	view_range = 20,
	--floats = 1,
	drops={
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "more_mese:mese_crystal_fragment_red", chance=2},
	},
	env_dmg= { ["default:ice"]=0, ["smoke:smog"]=0, ["fire:basic_flame"]=0, ["mobs:cobweb"]=0 },
	water_damage = 0,
	fire_damage = 0,
	animation = {
		speed_normal = 15,
		speed_run = 20,
		stand_start = 1,
		stand_end = 1,
		walk_start = 20,
		walk_end = 40,
		run_start = 20,
		run_end = 40,
		punch_start = 50,
		punch_end = 90,
	},
})


mobs:spawn({
	name = "mob_spider:lava",
	nodes = {"abyss:glowstone", "dungeon_crates:crate"},
	chance = 1000,
	active_object_count = 4,
	max_height = -300,
})

mobs:register_mob("mob_spider:lava_ling", {
	type = "monster",
	passive = false,
	attack_type = "shoot",
	arrow="mobs_crab:tp_arrow",
	shoot_interval=2.5,
	reach = 2.5,
	damage = 2,
	hp_max = 15,
	collisionbox = {-0.4, -0.25, -0.4, 0.4, 0.25, 0.4},
	visual = "mesh",
	mesh = "spider2.b3d",
	textures = {
		{"spider2-2x-robot-mese.png"},
	},
	visual_size = {x = 0.5, y = 0.5},
	makes_footstep_sound = false,
	sounds = {
		attack="mobs_spider",
	},
	walk_velocity = 1,
	run_velocity = 3,
	jump = true,
	jump_height=9,
	view_range = 20,
	--floats = 1,
	drops = { {name = "default:mese_crystal_fragment", chance=2}, },
	env_dmg= { ["default:ice"]=0, ["smoke:smog"]=0, ["fire:basic_flame"]=0, ["mobs:cobweb"]=0 },
	water_damage = 0,
	fire_damage = 0,
	animation = {
		speed_normal = 15,
		speed_run = 20,
		stand_start = 1,
		stand_end = 1,
		walk_start = 20,
		walk_end = 40,
		run_start = 20,
		run_end = 40,
		punch_start = 50,
		punch_end = 90,
	},
})

mobs:register_mob("mob_spider:orange_big", {
	group_attack = true,
	type = "monster",
	passive = false,
	attack_type = "shoot",
	arrow="mob_spider:net_arrow",
	shoot_interval=2.5,
	shoot_offset=0,
	dogshoot_switch=5,
	attack_animals=false,
	reach = 2.25,
	damage = 3,
	hp_max = 40,
	collisionbox = {-0.5, -0.9, -0.5, 0.5, 0, 0.5},
	visual = "mesh",
	mesh = "spider2.b3d",
	textures = {
		{"mob_spider_orange.png"},
	},
	visual_size = {x = 0.75, y = 2},
	makes_footstep_sound = false,
	sounds = {
		-- random = "mobs_spider",
		attack = "mobs_spider",
	},
	walk_velocity = 1,
	run_velocity = 2.75,
	jump = true,
	jump_height=6,
	view_range = 20,
	floats = 0,
	env_dmg= { ["default:ice"]=2, ["mobs:cobweb"]=-0.5 },
	water_damage = 1,
	cobweb_damage=-0.5,
	animation = {
		speed_normal = 15,
		speed_run = 20,--15,
		stand_start = 1,
		stand_end = 1,
		walk_start = 20,
		walk_end = 40,
		run_start = 20,
		run_end = 40,
		punch_start = 50,
		punch_end = 90,
	},
	drops = {
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
		{name="farming:string", chance=3}
	},
	env_alt= {
		["air"]={"mobs:cobweb", 30},
	},

})
mobs:register_mob("mob_spider:orange", {
	group_attack = true,
	type = "monster",
	passive = false,
	attack_type = "dogfight",
	suicidal=true,
	attack_animals=false,
	reach = 2,
	damage = 2,
	hp_max = 10,
	collisionbox = {-0.4, -0.25, -0.4, 0.4, 0.25, 0.4},
	visual = "mesh",
	mesh = "spider2.b3d",
	textures = {
		{"mob_spider_orange.png"},
	},
	visual_size = {x = 0.5, y = 0.5},
	makes_footstep_sound = false,
	sounds = {
		-- random = "mobs_spider",
		attack = "mobs_spider",
	},
	walk_velocity = 1,
	run_velocity = 3.5,
	jump = true,
	jump_height=6,
	view_range = 15,
	floats = 0,
	env_dmg= { ["default:ice"]=2, ["mobs:cobweb"]=-0.5 },
	water_damage = 1,
	animation = {
		speed_normal = 15,
		speed_run = 20,--15,
		stand_start = 1,
		stand_end = 1,
		walk_start = 20,
		walk_end = 40,
		run_start = 20,
		run_end = 40,
		punch_start = 50,
		punch_end = 90,
	},
	drops = { {name = "default:mese_crystal_fragment", chance=4}, },
})

--[[
local tpeffect=function(pos)
	local radius=4
	local gravity=4
	minetest.add_particlespawner{
		amount = 20,
		time = 0.25,
		minpos = pos,
		maxpos = pos,
		minvel = {x = -radius, y = -radius, z = -radius},
		maxvel = {x = radius, y = radius, z = radius},
		minacc = {x = 0, y = gravity, z = 0},
		maxacc = {x = 0, y = gravity, z = 0},
		minexptime = 0.1,
		maxexptime = 1,
		minsize = 1,
		maxsize = 2,
		texture = "default_ice.png",
		glow=4,
	}
end
--]]
mobs:register_arrow("mob_spider:snow_arrow", {
	visual = "sprite",
	visual_size = {x = 3, y = 3},
	textures = {"default_snowball.png"},
	velocity = 12,
	glow=1,

	hit_player = function(self, player)
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 2},
		}, nil)
	end,

	hit_mob = function(self, player)
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 2},
		}, nil)
	end,
	   --[[
	hit_mob = function(self, player)
		local p=minetest.find_node_near(player:getpos(),1, {"air"})
		if p and mobs:count_mobs(p, "mob_spider:ice_mini")<4 then
			minetest.env:add_entity(p, "mob_spider:ice_mini")
		end
	end,
	]]

	hit_node=function(self, pos, node)
		-- if minetest.find_node_near(self.object:getpos(), 1, {name="ignore"}) then return end
		--[[
		local p=minetest.find_node_near(pos,1, {"air"})
		if p and mobs:count_mobs(pos, "mob_spider:ice_mini")<4 then
			minetest.add_entity(p, "mob_spider:ice_mini")
		end
		]]
	end,
})

--[[
mobs:register_mob("mob_spider:ice_mini", {
	group_attack = true,
	type = "monster",
	suicidal=true,
	passive = false,
	attack_type = "dogfight",
	reach = 2,
	damage = 2,
	hp_max = 10,
	collisionbox = {-0.4, -0.25, -0.4, 0.4, 0.25, 0.4},
	visual = "mesh",
	mesh = "spider2.b3d",
	textures = {
		{"mob_spider_snowy.png"},
	},
	visual_size = {x = 0.5, y = 0.5},
	makes_footstep_sound = false,
	sounds = {
		attack = "mobs_spider",
	},
	walk_velocity = 1,
	run_velocity = 3,
	jump = true,
	jump_height=6,
	view_range = 20,
	floats = 1,
	drops = { {name = "default:mese_crystal_fragment", chance=4} },
	water_damage = -0.1,
	lava_damage = 4,
	animation = {
		speed_normal = 15,
		speed_run = 20,--15,
		stand_start = 1,
		stand_end = 1,
		walk_start = 20,
		walk_end = 40,
		run_start = 20,
		run_end = 40,
		punch_start = 50,
		punch_end = 90,
	},
})
--]]

mobs:register_mob("mob_spider:ice", {
	group_attack = true,
	type = "monster",
	passive = false,
	attack_type = "shoot",
	arrow="mob_spider:snow_arrow",
	shoot_interval=1.5,
	shoot_offset=-0.5,
	dogshoot_switch=10,
	reach = 2.5,
	damage = 3,
	hp_max = 40,
	collisionbox = {-0.5, -0.9, -0.5, 0.5, 0, 0.5},
	visual = "mesh",
	mesh = "spider2.b3d",
	textures = {
		{"mob_spider_snowy.png"},
	},
	visual_size = {x = 0.75, y = 2},
	makes_footstep_sound = false,
	sounds = {
		-- random = "mobs_spider",
		attack = "mobs_spider",
	},
	walk_velocity = 1,
	run_velocity = 3.5,
	jump = true,
	jump_height=6,
	view_range = 20,
	floats = 1,
	drops = {
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "more_mese:mese_crystal_fragment_cyan", chance=2},
	},
	water_damage=-0.1,
	animation = {
		speed_normal = 15,
		speed_run = 20,--15,
		stand_start = 1,
		stand_end = 1,
		walk_start = 20,
		walk_end = 40,
		run_start = 20,
		run_end = 40,
		punch_start = 50,
		punch_end = 90,
	},
	env_alt = {
		["default:snow_grass_5"]={ "default:pine_bush_sapling", 30},
		["default:pine_bush_sapling"]={ "default:pine_sapling", 10},
	},
})

--[[
mobs:register_mob("mob_spider:grey", {
	group_attack = true,
	type = "monster",
	passive = false,
	attack_type = "shoot",
	arrow="mob_spider:net_arrow",
	shoot_interval=2,
	shoot_offset=0,
	dogshoot_switch=5,
	group_attack=true,
	reach = 2.25,
	damage = 3,
	hp_min = 40,
	hp_max = 40,
	collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.1, 0.5},
	visual = "mesh",
	mesh = "spider2.b3d",
	textures = {
		{"mob_spider_grey.png"},
	},
	visual_size = {x = 0.75, y = 1},
	makes_footstep_sound = false,
	sounds = {
		-- random = "mobs_spider",
		attack = "mobs_spider",
	},
	walk_velocity = 1,
	run_velocity = 2,
	jump = true,
	jump_height=7,
	view_range = 20,
	floats = 0,
	lava_damage = 2,
	env_dmg= { ["default:ice"]=2, ["mobs:cobweb"]=-0.5 },
	water_damage = 1,
	animation = {
		speed_normal = 15,
		speed_run = 20,--15,
		stand_start = 1,
		stand_end = 1,
		walk_start = 20,
		walk_end = 40,
		run_start = 20,
		run_end = 40,
		punch_start = 50,
		punch_end = 90,
	},
	knock_back=0,
	env_alt= { ["air"]={ "mobs:cobweb", 60}},
	drops = {
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
		{name="farming:string", chance=10},
	},
})

mobs:register_mob("mob_spider:small_grey", {
	group_attack = true,
	type = "monster",
	passive = false,
	attack_type = "dogfight",
	attack_animals=false,
	suicidal=true,
	reach = 2,
	damage = 2,
	hp_max = 10,
	collisionbox = {-0.4, -0.25, -0.4, 0.4, 0.25, 0.4},
	visual = "mesh",
	mesh = "spider2.b3d",
	textures = {
		{"mob_spider_grey.png"},
	},
	visual_size = {x = 0.5, y = 0.5},
	makes_footstep_sound = false,
	sounds = {
		attack = "mobs_spider",
	},
	walk_velocity = 1,
	run_velocity = 3,
	jump = true,
	jump_height=5,
	view_range = 15,
	floats = 0,
	env_dmg= { ["default:ice"]=2, ["mobs:cobweb"]=-0.5 },
	water_damage = 1,
	fire_damage=8,
	animation = {
		speed_normal = 15,
		speed_run = 20,--15,
		stand_start = 1,
		stand_end = 1,
		walk_start = 20,
		walk_end = 40,
		run_start = 20,
		run_end = 40,
		punch_start = 50,
		punch_end = 90,
	},
	drops = { {name = "default:mese_crystal_fragment", chance=4}, },
})
]]

--[[
mobs:register_mob("mob_spider:blue", {
	type = "monster",
	passive = false,
	attack_type = "dogfight",
	group_attack=true,
	attack_animals=false,
	reach = 2,
	damage = 2,
	--hp_max = 40,
	hp_max = 10,
	collisionbox = {-0.4, -0.25, -0.4, 0.4, 0.25, 0.4},
	visual_size = {x = 0.5, y = 0.5},
	visual = "mesh",
	mesh = "spider2.b3d",
	textures = {
		{"mob_spider_blue.png"},
	},
	makes_footstep_sound = false,
	sounds = {
		attack = "mobs_spider",
	},
	walk_velocity = 2,
	run_velocity = 3,
	--fall_speed=-3,
	jump = true,
	view_range = 24,
	floats = 1,
	water_damage=-0.5,
	--air_damage=1,
	animation = {
		speed_normal = 15,
		speed_run = 20,--15,
		stand_start = 1,
		stand_end = 1,
		walk_start = 20,
		walk_end = 40,
		run_start = 20,
		run_end = 40,
		punch_start = 50,
		punch_end = 90,
	},
	drops = {
		{name = "default:mese_crystal_fragment", chance=4},
	},
	env_alt = {
		["default:water_source"]= {"mobs_butterfly:lit_air", 20},
	},
})
]]

-- cobweb
minetest.register_node(":mobs:cobweb", {
	description = "Cobweb",
	drawtype = "firelike",
	visual_scale = 1.2,
	tiles = {"mobs_cobweb.png"},
	inventory_image = "mobs_cobweb.png",
	paramtype = "light",
	sunlight_propagates = true,
	damage_per_second=1,
	move_resistance=7,
	walkable = false,
	groups = {snappy = 1, flammable=2},
	sounds = default.node_sound_leaves_defaults(),
	floodable = true,
	drop = { max_items=1, items= { {items={"farming:string"}, rarity=50}}}, 
})

--[[
local c="mobs:cobweb"
minetest.register_craft
{
	type="shapeless",
	output="farming:string",
	recipe={c,c,c, c,c,c, c,c,c}
}

minetest.register_craft({
	type = "fuel",
	recipe = "mobs:cobweb",
	burntime = 1,
})
--]]

mobs:register_arrow("mob_spider:net_arrow", {
	visual = "sprite",
	visual_size = {x = 2, y = 2},
	textures = {"farming_string.png"},
	velocity = 12,

	hit_player = function(self, player)
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 2},
		}, nil)
	end,

	hit_mob = function(self, player)
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 2},
		}, nil)
	end,
	hit_node=function(self)
		local p=minetest.find_node_near(self.lastpos or self.object:get_pos(), 1, {name="air"})
		if not p then return nil end
		--[[
		if math.random(1,2)==1 or mobs:count_mobs(p, "mob_spider:orange")>5 then minetest.set_node(p, {name="mobs:cobweb"})
		else minetest.add_entity(p, "mob_spider:orange") end
		]]
		--minetest.set_node(p, {name="mobs:cobweb"})
		minetest.add_entity(p, "mob_spider:orange")
	end,
})

mobs:spawn({
	name = "mob_spider:orange_big",
	nodes = {"group:ore", "anchorstone:anchorstone"},
	min_light = 0,
	max_light = 15,
	chance = 8,
	active_object_count = 3,
	min_height = -999,
	max_height = 999,
	interval=17,
})

mobs:spawn({
	name = "mob_spider:orange",
	nodes = {"mobs:cobweb"},
	min_light = 1,
	max_light = 15,
	chance = 8,
	active_object_count = 3,
	min_height = -999,
	max_height = 999,
	interval=23,
})

mobs:spawn({
	name = "mob_spider:ice",
	nodes = {"default:snow_grass_5"},
	min_light = 0,
	max_light = 15,
	chance = 1000,
	active_object_count = 3,
	min_height = -10,
	max_height = 999,
	interval=29,
})

