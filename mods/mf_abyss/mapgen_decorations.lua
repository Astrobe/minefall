--[[

  Register decorations for Nether mapgen

  Copyright (C) 2020 Treer

  Permission to use, copy, modify, and/or distribute this software for
  any purpose with or without fee is hereby granted, provided that the
  above copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
  WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR
  BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES
  OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
  SOFTWARE.

]]--

local _  = {name = "air",                     prob = 0}
local G  = {name = "abyss:glowstone",        prob = 255, force_place = true}
local N  = {name = "abyss:rack",             prob = 255}


-- =================
--    Stalactites
-- =================

local schematic_GreaterStalactite = {
    size = {x = 3, y = 21, z = 3},
    data = { -- note that data is upside down

        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, G, _,
        _, G, _,
        _, G, _,
        _, G, _,
        G, N, N,
        N, N, N,
        N, N, N,
        _, N, _,
        _, _, _,
        _, _, _,

        _, G, _,  -- ypos 2, prob 85% (218/255)
        _, G, _,  -- ypos 3, prob 85% (218/255)
        _, G, _,  -- ypos 4, prob 85% (218/255)
        _, G, _,  -- ypos 5, prob 85% (218/255)
        _, G, _,  -- ypos 6, prob 85% (218/255)
        _, G, _,  -- ypos 7, prob 85% (218/255)
        _, G, _,  -- ypos 8, prob 85% (218/255)
        _, G, N,  -- ypos 9, prob 50% (128/256) to make half of stalactites asymmetric
        _, G, N,  -- ypos 10, prob 50% (128/256) to make half of stalactites asymmetric
        _, G, N,  -- ypos 11, prob 50% (128/256) to make half of stalactites asymmetric
        _, G, N,  -- ypos 12, prob 50% (128/256) to make half of stalactites asymmetric
        G, G, N,  -- ypos 13, prob 75% (192/256)
        G, N, N,  -- ypos 14, prob 75% (192/256)
        G, N, N,  -- ypos 15, prob 100%
        N, N, N,  -- ypos 16, prob 100%
        N, N, N,  -- ypos 17, prob 100%
        N, N, N,  -- ypos 18, prob 100%
        N, N, N,  -- ypos 19, prob 75% (192/256)
        N, N, N,  -- ypos 20, prob 85% (218/255)
        _, N, N,  -- ypos 21, prob 50% (128/256) to make half of stalactites asymmetric
        _, N, _,  -- ypos 22, prob 100%

        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, _, _,
        _, G, _,
        _, G, _,
        _, G, _,
        _, G, _,
        _, G, _,
        G, G, G,
        N, N, G,
        N, N, N,
        _, N, _,
        _, N, _,
        _, _, _,

    },
    -- Y-slice probabilities do not function correctly for ceiling schematic
    -- decorations because they are inverted, so ypos numbers have been inverted
    -- to match, and a larger offset in place_offset_y should be used (e.g. -3).
    yslice_prob = {
        {ypos = 19, prob = 64},
        {ypos = 14, prob = 64},
        {ypos = 13, prob = 64},
        {ypos = 12, prob = 64},
        {ypos = 11, prob = 128},
        {ypos = 10, prob = 128},
        {ypos =  9, prob = 128},
        {ypos =  8, prob = 218},
        {ypos =  7, prob = 218},
        {ypos =  6, prob = 218},
        {ypos =  5, prob = 218},
        {ypos =  4, prob = 218},
        {ypos =  3, prob = 218},
        {ypos =  2, prob = 218},
        {ypos =  1, prob = 218},
        {ypos =  0, prob = 218}
    }
}



-- A stalagmite is an upsidedown stalactite, so
-- use the GreaterStalactite to create a ToweringStalagmite schematic
local schematic_ToweringStalagmite = {
    size = schematic_GreaterStalactite.size,
    data = {},
    yslice_prob = {}
}
local array_length = #schematic_GreaterStalactite.data + 1
for i, node in ipairs(schematic_GreaterStalactite.data) do
    schematic_ToweringStalagmite.data[array_length - i] = node
end
y_size = schematic_GreaterStalactite.size.y
for i, node in ipairs(schematic_GreaterStalactite.yslice_prob) do
    schematic_ToweringStalagmite.yslice_prob[i] = {
        -- we can safely lower the prob. to gain more variance because floor based schematics
        -- don't have the bug where missing lines moves them away from the surface
	-- Nope, that's buggy either way; slices don't "tetris" at all. Adjusted probs to have
	-- an ascending order -- Astrobe
        prob = schematic_GreaterStalactite.yslice_prob[i].prob - 20,
        ypos = y_size - 1 - schematic_GreaterStalactite.yslice_prob[i].ypos
    }
end

minetest.register_decoration({
    name = "Deep-netherrack towering stalagmite",
    deco_type = "schematic",
    place_on = "abyss:rack",
    sidelen = 80,
    fill_ratio = 0.005,
    --y_max = abyss.DEPTH_CEILING,
    --y_min = abyss.DEPTH_FLOOR,
    schematic = schematic_ToweringStalagmite,
    flags = "place_center_x,place_center_z,force_placement, all_floors",
    place_offset_y=-2,
    biomes={"abyss"},
})
