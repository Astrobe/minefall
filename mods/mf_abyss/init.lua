-- Author: Astrobe. License: same as below.
-- Derived from:
--[[

  Nether mod for minetest

  Copyright (C) 2013 PilzAdam

  Permission to use, copy, modify, and/or distribute this software for
  any purpose with or without fee is hereby granted, provided that the
  above copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
  WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR
  BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES
  OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
  SOFTWARE.

]]--
local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

dofile(modpath .. "/nodes.lua")
dofile(modpath .. "/mapgen_decorations.lua")

minetest.register_biome({
	name = "abyss",
	node_stone = "abyss:rack",
	y_max = -300, -- abyss.DEPTH_CEILING, 
	y_min = -500, -- abyss.DEPTH_FLOOR,
	heat_point = 50,
	humidity_point = 50,
	node_dungeon = "abyss:brick",
	node_dungeon_alt = "air",
	-- node_dungeon_stair = "stairs:stair_abyss_brick",
	vertical_blend = 4,
})

