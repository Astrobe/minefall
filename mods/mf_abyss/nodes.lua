--[[

  Nether mod for minetest

  Copyright (C) 2013 PilzAdam

  Permission to use, copy, modify, and/or distribute this software for
  any purpose with or without fee is hereby granted, provided that the
  above copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
  WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR
  BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES
  OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
  SOFTWARE.

]]--

minetest.register_node("abyss:rack", {
	description = "Abyssite",
	tiles = {"tms_amazonite.png^[transformR90"},
	is_ground_content = true,
	groups = {cracky = 1, level = 2, stone=1},
	sounds = default.node_sound_stone_defaults(),
	light_source=1,
	drop="stairs:stair_abyss_cobble",
})

--[[
for _, model in ipairs
	{
		"default:copperblock",
		"default:tinblock",
		"default:bronzeblock",
		"default:steelblock",
		"default:goldblock",
	}
	do
	local name=string.gsub(model, ":", "_")
	minetest.register_alias("abyss:mimic_"..name, "abyss:brick")
end
]]

-- for ruins
minetest.register_node("abyss:abyssite_brick", {
	description = "Abyss Stone brick",
	tiles = {"tms_amazonite_brick.png"},
	is_ground_content = true,
	groups = {cracky = 1, level=4},
	sounds = default.node_sound_stone_defaults(),
})

-- Deep Netherrack, converted to abyssite cobble
minetest.register_node("abyss:cobble", {
	description = "Abyssite cobble",
	tiles = {"tms_amazonite_cobble.png"},
	is_ground_content = true,
	groups = {cracky = 2, level = 1, yatpm=1},
	sounds = default.node_sound_stone_defaults(),
	drop="stairs:stair_abyss_cobble 4",
})

minetest.register_node("abyss:glowstone", {
	description = "Glowstone",
	tiles = {"nether_glowstone_deep.png"},
	is_ground_content = true,
	light_source = 10,
	paramtype = "light",
	groups = {cracky = 3, stone=1},
	sounds = default.node_sound_glass_defaults(),
	drop="",
})

minetest.register_craft {
	output="default:gold_lump",
	type="cooking",
	recipe="abyss:glowstone",
	cooktime=30,
}

stairs.register_stair(
	"abyss_cobble",
	"abyss:cobble",
	{cracky = 2, level = 2},
	{"nether_rack_deep.png"},
	"Abyssite cobble stair",
	minetest.registered_nodes["abyss:cobble"].sounds
)

for _, model in ipairs
	{
		"default:stonebrick",
		"default:stone_block",
		"default:desert_stonebrick",
		"default:desert_stone_block",
		"default:sandstonebrick",
		"default:sandstone_block",
		"default:desert_sandstone_brick",
		"default:desert_sandstone_block",
		"default:obsidianbrick",
		"default:obsidian_block",
		"default:basalt_brick",
		"default:silver_sandstone_block",
		"default:silver_sandstonebrick",
		"default:brick",
		"abyss:abyssite_brick",
		"mf_decor:stone_brick",
	}
	do
	local name=string.gsub(model, ":", "_")
	minetest.register_node("abyss:mimic_"..name,
	{
		description="Mimic block (active)", -- should not appear in inventory, though.
		tiles=minetest.registered_nodes[model].tiles,
		groups={ cracky=1, level=2},
		sounds = default.node_sound_stone_defaults(),
		drop="abyss:brick",
	})
end
	
minetest.register_node("abyss:brick", {
	description = "Abyss dungeon brick.",
	tiles = {"nether_brick_deep.png"},
	is_ground_content = true,
	groups = {cracky = 1, level = 2},
	sounds = default.node_sound_stone_defaults(),
	on_place=function(itemstack, placer, pointed)
		if not placer then return nil end
		if pointed.type=="node" then
			local model=minetest.get_node(pointed.under).name
			local mimic="abyss:mimic_"..model.gsub(model, ":", "_")
			local place
			if minetest.registered_nodes[mimic] then
				place=mimic
			elseif string.match(model, "^abyss:mimic_") then
				place=model
			else
				minetest.set_node(pointed.above, {name="abyss:brick"})
			end
			minetest.set_node(pointed.above, {name=place or "abyss:brick"})
			itemstack:take_item(1)
			return itemstack
		end
		return nil
	end
})
