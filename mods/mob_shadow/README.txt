Shadow mob extracted from "Horror" mod by D00Med.
Note: this mod doesn't spawn from an ABM as other mobs, it is "summoned" with
a wand. It is used as an allied NPC, not a monster.

Author: Astrobe
License (code): Same as Minetest v5.
License (textures, model): see license.txt


