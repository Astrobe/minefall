
--[[

Copyright (C) 2016 - Auke Kok <sofar@foo-projects.org>

"lightning" is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation; either version 2.1
of the license, or (at your option) any later version.

--]]

lightning = {}

lightning.interval_low = 1 -- 17
lightning.interval_high = 30 -- 503
lightning.range_h = 100
lightning.range_v = 50
lightning.size = 100
-- disable this to stop lightning mod from striking
lightning.auto = false

local rng = PcgRandom(32321123312123)

local ps = {}
local ttl = 1

		
local revertsky = function()
	if ttl == 0 then
		return
	end
	ttl = ttl - 1
	if ttl > 0 then
		return
	end

	for key, entry in pairs(ps) do
		entry.sky.type="regular"
		entry.p:set_sky(entry.sky)
	end

	ps = {}
end

minetest.register_globalstep(revertsky)

-- select a random strike point, midpoint
local function choose_pos(pos)
	if not pos then
		local playerlist = minetest.get_connected_players()
		local playercount = table.getn(playerlist)

		-- nobody on
		if playercount == 0 then
			return nil, nil
		end

		local r = rng:next(1, playercount)
		local randomplayer = playerlist[r]
		pos = randomplayer:getpos()
	end
	pos.x = math.floor(pos.x - (lightning.range_h / 2) + rng:next(1, lightning.range_h))
	pos.y = pos.y + (lightning.range_v / 2)
	pos.z = math.floor(pos.z - (lightning.range_h / 2) + rng:next(1, lightning.range_h))

	local b, pos2 = minetest.line_of_sight(pos, {x = pos.x, y = pos.y - lightning.range_v, z = pos.z}, 1)

	-- nothing but air found
	if b then
		return nil, nil
	end

	local n = minetest.get_node({x = pos2.x, y = pos2.y - 1/2, z = pos2.z})
	if n.name == "air" or n.name == "ignore" then
		return nil, nil
	end

	return pos, pos2
end

-- lightning strike API
-- * pos: optional, if not given a random pos will be chosen
-- * returns: bool - success if a strike happened
lightning.strike = function(pos)
	if lightning.auto then
		minetest.after(rng:next(lightning.interval_low, lightning.interval_high), lightning.strike)
	end

	local pos2
	pos, pos2 = choose_pos(pos)

	if not pos --[[or minetest.is_protected(pos, "")]] then
		return false
	end

	minetest.add_particlespawner({
		amount = 1,
		time = 0.2,
		-- make it hit the top of a block exactly with the bottom
		minpos = {x = pos2.x, y = pos2.y + (lightning.size / 2) + 1/2, z = pos2.z },
		maxpos = {x = pos2.x, y = pos2.y + (lightning.size / 2) + 1/2, z = pos2.z },
		minvel = {x = 0, y = 0, z = 0},
		maxvel = {x = 0, y = 0, z = 0},
		minacc = {x = 0, y = 0, z = 0},
		maxacc = {x = 0, y = 0, z = 0},
		minexptime = 0.2,
		maxexptime = 0.2,
		minsize = lightning.size * 10,
		maxsize = lightning.size * 10,
		collisiondetection = true,
		vertical = true,
		-- to make it appear hitting the node that will get set on fire, make sure
		-- to make the texture lightning bolt hit exactly in the middle of the
		-- texture (e.g. 127/128 on a 256x wide texture)
		texture = "lightning_lightning_" .. rng:next(1,3) .. ".png",
		-- 0.4.15+
		glow = 14,
	})

	minetest.sound_play("lightning_thunder", { pos = pos, gain = 10, max_hear_distance = 64 })

	local playerlist = minetest.get_connected_players()
	for i = 1, #playerlist do
		local player = playerlist[i]
		if vector.distance(player:get_pos(), pos)<100 then
			local sky=player:get_sky(true)
			local name = player:get_player_name()
			if ps[name] == nil then
				-- We use the fact that when not in "regular" mode, base_color is ignored
				-- We could maybe even forego the backup of the sky.
				ps[name] = {p = player, sky = sky}
				sky.type="plain" 
				sky.base_color=0xFFFFFF
				player:set_sky(sky)
			end

		end
	end

	-- trigger revert of skybox
	ttl = 5
	if minetest.is_protected(pos2,"") then return end

	local n = minetest.get_node(pos2)

	if minetest.get_item_group(n.name, "sand") > 0 then
		-- contrary to other effects this one is nice to us,
		-- given than we cannot craft glass. Players can go to sandy places
		-- when they see a storm and look for glass nodes.
		minetest.set_node(pos2, { name = "default:glass"})
		return
	elseif minetest.get_item_group(n.name, "leaves")>0 then
		-- test before flammable nodes
		pos2.y=pos2.y+1
		minetest.add_entity(pos2, "mob_spider:orange_big")
		return
	elseif minetest.get_item_group(n.name, "flammable") > 0 then
		minetest.set_node(pos2, { name = "fire:basic_flame"})
		return
	elseif minetest.get_item_group(n.name, "water") > 0 then
		if rng:next(1,100) == 1 then
			pos2.y=pos2.y+1
			minetest.set_node(pos2, {name="default:water_source"}) 
		end
		return
	end

end

-- if other mods disable auto lightning during initialization, don't trigger the first lightning.
minetest.after(5, function(dtime)
	if lightning.auto then
		minetest.after(rng:next(lightning.interval_low,
			lightning.interval_high), lightning.strike)
	end
end)
