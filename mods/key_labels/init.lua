-- Author: Astrobe
-- licence: CC-BY-SA 4.0
-- Version 1.1 
-- 1.0 -> 1.1: removed "Key to" prefix, limit text to 40 char.

-- If a player punches a sign node with a key,
-- the text of the sign is copied into the description of the key.
-- Only "Key to " is prepended to the sign's original text.

minetest.override_item("default:sign_wall_steel", {
	on_punch=function(pos, node, puncher, pointed_thing)
		local item=puncher:get_wielded_item()
		if item:get_name()=="default:key" then
			local key_meta=item:get_meta()
			local sign_text=minetest.get_meta(pos):get_string("text")
			key_meta:set_string("description", string.sub(sign_text, 1, 40))
			puncher:set_wielded_item(item)
			--[[
		else
			minetest.node_dig(pos, node, puncher)
			--]]
		end
	end
})

