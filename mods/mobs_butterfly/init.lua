

-- local variables
local l_skins = {
	{"bf1.png^bf2.png^bf3.png^bf4.png^bf5.png"},
	{"(bf1.png^[colorize:orange)^(bf2.png^[colorize:violet)^(bf3.png^[colorize:yellow)^(bf4.png^[colorize:cyan)^(bf5.png^[colorize:black)"},
	{"(bf1.png^[colorize:magenta)^(bf2.png^[colorize:green)^(bf3.png^[colorize:red)^(bf4.png^[colorize:blue)^(bf5.png^[colorize:white)"},
	{"(bf1.png^[colorize:yellow)^(bf2.png^[colorize:cyan)^(bf3.png^[colorize:green)^(bf4.png^[colorize:violet)^(bf5.png^[colorize:darkgray)"},
	{"(bf1.png^[colorize:pink)^(bf2.png^[colorize:white)^(bf3.png^[colorize:blue)^(bf4.png^[colorize:orange)^(bf5.png^[colorize:gray)"},
	{"(bf1.png^[colorize:darkgreen)^(bf2.png^[colorize:brown)^(bf3.png^[colorize:black)^(bf4.png^[colorize:darkgray)^(bf5.png^[colorize:red)"}
}

minetest.register_node("mobs_butterfly:lit_air",
{
	description="Air lit by butterflies",
	drawtype= "airlike",
	paramtype="light",
	light_source=7,
	walkable=false,
	pointable=false,
	diggable=false,
	floodable=false,
	buildable_to=true,
	drop="",

	on_construct=function(pos)
		local timer=minetest.get_node_timer(pos)
		timer:start(math.random(1,4))
	end,

	on_timer=function(pos)
		minetest.swap_node(pos, {name="air"})
	end

})

-- Butterfly
mobs:register_mob("mobs_butterfly:butterfly", {
	type = "animal",
	passive = true,
	runaway=true,
	runaway_from={"player", "monster"},
	hp_min = 1,
	hp_max = 2,
	collisionbox = {-0.3, 0, -0.3, 0.3, 0.6, 0.3},
	visual = "mesh",
	mesh = "mobf_butterfly.x",
	textures = l_skins,
	walk_chance=90,
	walk_velocity = 2,
	run_velocity = 2.5,
	fall_speed = 0,
	-- stepheight = 3,
	fly = true,
	regen=0.9,
	water_damage = 1,
	view_range = 10,
	glow=3,
	animation = {
		speed_normal = 15,
		speed_run = 30,
		stand_start = 1, stand_end = 90,
		walk_start = 1,	walk_end = 90,
		run_start = 1,	run_end = 90,
	},


	env_alt = {
		["air"]= {"mobs_butterfly:lit_air", 30},
		["flowers:dandelion_yellow"]={"default:aspen_sapling", 10},
		["flowers:tulip_black"]={"default:pine_sapling", 10},
		["flowers:geranium"]={"default:sapling", 10},
		["flowers:rose"]={"default:bush_sapling", 10},
		["flowers:tulip"]={"default:acacia_bush_sapling", 10},
		["flowers:viola"]={"default:blueberry_bush_sapling", 10},
		["flowers:chrysanthemum_green"]={"default:pine_bush_sapling", 10},
		["flowers:dandelion_white"]={"default:giantgrass", 10},
		["default:grass_5"]={ "farming:wheat", 10},
		["default:dry_grass_5"]={ "farming:cotton", 10},
	},
})
--name, nodes, neighbors, min_light, max_light, interval, chance, active_object_count, min_height, max_height
mobs:spawn {
	name = "mobs_butterfly:butterfly",
	nodes={"group:flower"},
	min_light = 0,
	max_light = 3,
	chance = 100,
	min_height = -999,
	max_height = 999,
	interval= 181,
} 

mobs:register_egg("mobs_butterfly:butterfly", "Butterfly", "default_cloud.png", 1)

