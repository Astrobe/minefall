-- Derives from the mod of the same name by Sapier.
--
-- Author: Astrobe
-- License: same as original (see license.txt).
--

if mobs.mod and mobs.mod == "redo" then

local SPRITE_VERSION = false	-- set to true to use upright sprites instead of meshes

-- local variables
	local l_cc_hand			= 0
	local l_cc_net			= 100
	local l_anims = {
		speed_normal = 24,		speed_run = 24,
		stand_start = 1,		stand_end = 80,
		walk_start = 81,		walk_end = 155,
		run_start = 81,			run_end = 155
	}
	local l_visual = "mesh"
	local l_visual_size = {x=.75, y=.75}
	local l_clown_mesh = "animal_clownfish.b3d"
	local l_trop_mesh = "fish_blue_white.b3d"	
	local l_clown_textures = {
		{"clownfish.png"},
		{"clownfish2.png"}
	}
	local l_trop_textures = {
		{"fish.png"},
		{"fish2.png"},
		{"fish3.png"}
	}

	if SPRITE_VERSION then
		l_visual = "upright_sprite"
		l_visual_size = {x=.5, y=.5}
		l_clown_mesh = nil
		l_trop_mesh = nil		
		l_clown_textures = {{"animal_clownfish_clownfish_item.png"}}
		l_trop_textures = {{"animal_fish_blue_white_fish_blue_white_item.png"}}
	end

-- Clownfish
	mobs:register_mob("mobs_fish:clownfish", {
		type = "animal",
		passive = true,
		hp_min = 1,
		hp_max = 4,
		collisionbox = {-0.25, -0.25, -0.25, 0.25, 0.25, 0.25},
		rotate = 270,
		visual = l_visual,
		mesh = l_clown_mesh,
		textures = l_clown_textures,
		visual_size = l_visual_size,
		makes_footstep_sound = false,
		stepheight = 0.1,
		runaway=true,
		runaway_from={"player", "monster"},
		run_velocity=3,
		fly = true,
		fly_in = "default:water_source",
		fall_speed = 0,
		view_range = 10,
		water_damage = -1,
		env_dmg={["air"]=2},
		animation = l_anims,
	})
	--name, nodes, neighbours, minlight, maxlight, interval, chance, active_object_count, min_height, max_height
	mobs:spawn_specific("mobs_fish:clownfish", "default:coral", nil, 0, 20, 29, 80, 4, -15, -5)
	mobs:register_egg("mobs_fish:clownfish", "Fish", "animal_fish_blue_white_fish_blue_white_item.png", 0)

-- Tropical fish
--[[
mobs:register_mob("mobs_fish:tropical", {
	type = "animal",
	passive = true,
	hp_min = 1,
	hp_max = 4,
	collisionbox = {-0.25, -0.25, -0.25, 0.25, 0.25, 0.25},
	rotate = 270,
	visual = l_visual,
	mesh = l_trop_mesh,
	textures = l_trop_textures,
	visual_size = l_visual_size,
	makes_footstep_sound = false,
	stepheight = 0.1,
	runaway=true,
	runaway_from={"player", "monster"},
	run_velocity=3,
	fly = true,
	fly_in = "default:water_source",
	--fall_speed = 0,
	view_range = 11,
	water_damage = 0,
	lava_damage = 5,
	animation = l_anims,
	on_rightclick = function(self, clicker)
		mobs:capture_mob(self, clicker, l_cc_hand, l_cc_net, 100, true, nil)
	end
})
--name, nodes, neighbours, minlight, maxlight, interval, chance, active_object_count, min_height, max_height
mobs:spawn_specific("mobs_fish:tropical", l_spawn_in, "default:sand_with_kelp", 5, 20, 31, 9, 4, -10, 0)
mobs:register_egg("mobs_fish:tropical", "Tropical fish", "animal_fish_blue_white_fish_blue_white_item.png", 0)
minetest.register_craft{
	type="cooking",
	output="mobs_fish:fried_fish",
	recipe= "mobs_fish:tropical_set",
	cooktime=30
}
]]
minetest.register_craft{
	type="cooking",
	output="mobs_fish:fried_fish",
	recipe= "mobs_fish:clownfish_set",
	cooktime=13
}

minetest.register_craftitem("mobs_fish:fried_fish", {
	description = "Cooked fish",
	inventory_image = "salmon_cooked.png",
	on_use = minetest.item_eat(5),
	groups={heal=5}
})
end
