--Wasps!


mobs:register_arrow("mob_wasp:sting", {
	   visual = "sprite",
	   visual_size = {x = 4, y = 4},
	   textures = {"dmobs_sting.png^[transform1"},
	   velocity = 10,

	   hit_player = function(self, player)
	      player:punch(self.object, 1.0, {
		 full_punch_interval = 1.0,
		 damage_groups = {fleshy = 3},
	      }, nil)
	   end,
	   
	   hit_mob = function(self, player)
	      player:punch(self.object, 1.0, {
		 full_punch_interval = 1.0,
		 damage_groups = {fleshy = 3},
	      }, nil)
	   end,

	   hit_node = function(self, pos)
		   local p=minetest.find_node_near(self.lastpos or pos,1, {"air"}, true)
		   if p and mobs:count_mobs(pos, "mob_wasp:wasp")<6 then
			   minetest.add_entity(p, "mob_wasp:wasp")
		   end
	   end,
	})


local function spawn_smoke(pos)
	local p=pos
	while p do
		p=minetest.find_node_near(pos, 2, {"air"})
		if p then smoke.add_smog(p) end
	end
end


mobs:register_arrow("mob_wasp:sting_smoke", {
	   visual = "sprite",
	   visual_size = {x = 4, y = 4},
	   textures = {"horror_shadow.png"},
	   velocity = 10,

	   hit_player = function(self, player)
	      player:punch(self.object, 1.0, {
		 full_punch_interval = 1.0,
		 damage_groups = {fleshy = 2},
	      }, nil)
	      spawn_smoke(player:get_pos())
	   end,
	   
	   hit_mob = function(self, player)
	      player:punch(self.object, 1.0, {
		 full_punch_interval = 1.0,
		 damage_groups = {fleshy = 2},
	      }, nil)
	      spawn_smoke(player:get_pos())
	   end,

	   hit_node = function(self, pos)
	      spawn_smoke(pos)
	   end,
	})

mobs:register_mob("mob_wasp:wasp", {
	type = "monster",
	reach = 2,
	damage = 1,
	attack_type = "dogfight",
	suicidal=true,
	hp_max = 3,
	collisionbox = {-0.1, -0.1, -0.1, 0.1, 0.5, 0.1},
	fly = true,
	fly_in={"air"},
	floats=1,
	fall_speed = 0,
	visual = "mesh",
	mesh = "wasp.b3d",
	textures = {
		{"dmobs_wasp.png^[multiply:green"},
	},
	sounds = {
		-- war_cry = "wasp",
		attack = "wasp",
	},
	blood_texture = "mobs_blood.png",
	visual_size = {x=0.6, y=0.6},
	makes_footstep_sound = false,
	walk_velocity = 2,
	run_velocity = 4,
	jump = true,
	env_dmg={["smoke:smog"]=-0.5, ["default:ice"]=2 },
	water_damage = 0.5,
	view_range = 20,
	animation = {
		speed_normal = 15,
		speed_run = 30,
		walk_start = 1,
		walk_end = 5,
		stand_start = 1,
		stand_end = 5,
		run_start = 1,
		run_end = 5,
		punch_start = 6,
		punch_end = 15,
	},
	-- drops = { {name = "default:mese_crystal_fragment", chance=4}, },

})
-- mobs:register_egg("mob_wasp:wasp", "Wasp", "dmobs_sting.png", 1)


-- mobs:register_egg("mob_wasp:wasp", "Wasp", "dmobs_wasp_bg.png", 1)

mobs:register_mob("mob_wasp:wasp_leader", {
	type = "monster",
	reach = 2.5,
	damage = 3,
	attacks_monsters=false,
	attack_type = "shoot",
	shoot_interval = 2,
	arrow = "mob_wasp:sting_smoke",
	shoot_offset = 0,
	hp_min = 40,
	hp_max = 40,
	collisionbox = {-0.4, -0.1, -0.4, 0.4, 2.5, 0.4},
	fly = true,
	fly_in={"air"},
	floats=1,
	fall_speed = 0,
	visual = "mesh",
	mesh = "wasp.b3d",
	textures = {
		{"dmobs_wasp.png^[multiply:yellow"}, -- to make the wings yellow; like the other colorized wasps.
	},
	sounds = {
		-- war_cry = "wasp",
		shoot_attack = "wasp",
	},
	blood_texture = "mobs_blood.png",
	visual_size = {x=3.5, y=3},
	makes_footstep_sound = false,
	walk_velocity = 1,
	run_velocity = 3.25,
	jump = true,
	env_dmg={["smoke:smog"]=-0.5, ["default:ice"]=2 },
	water_damage = 0.5,
	view_range = 24,
	animation = {
		speed_normal = 6,
		speed_run = 15,
		walk_start = 1,
		walk_end = 5,
		stand_start = 1,
		stand_end = 5,
		run_start = 1,
		run_end = 5,
		shoot_start = 6,
		shoot_end = 15,
	},
	drops = {
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "farming:seed_wheat", chance=2},
		{name = "farming:seed_cotton", chance=2},
		{name = "mob_bee:honey", chance=2},
	},
	--[[
	do_custom=function(self)
		-- Switches between dogfight and shooting
		if self.state~="attack" then return end
		if math.random(1,3)==1 then
			self.attack_type="dogfight"
		else
			self.attack_type="shoot"
		end
	end
	]]
})

mobs:register_mob("mob_wasp:wasp_leader_red", { -- orange actually.
	type = "monster",
	reach = 2.5,
	damage = 3,
	attacks_monsters=false,
	attack_type = "magic",
	shoot_interval = 1.5,
	arrow = "mob_wasp:sting",
	shoot_offset = 0,
	dogshoot_switch=5,
	hp_max = 50,
	regen=0.2,
	collisionbox = {-0.4, -0.1, -0.4, 0.4, 2, 0.4},
	fly = true,
	fly_in={"air"},
	floats=1,
	fall_speed = 0,
	visual = "mesh",
	mesh = "wasp.b3d",
	textures = {
		{"dmobs_wasp.png^[multiply:orange"},
	},
	sounds = {
		-- war_cry = "wasp",
		shoot_attack = "wasp",
	},
	blood_texture = "mobs_blood.png",
	visual_size = {x=3.5, y=3.5},
	makes_footstep_sound = false,
	walk_velocity = 3,
	run_velocity = 3.25,
	jump = true,
	env_dmg={["smoke:smog"]=-0.5, ["default:ice"]=2 },
	water_damage = 0.5,
	view_range = 24,
	animation = {
		speed_normal = 6,
		speed_run = 15,
		walk_start = 1,
		walk_end = 5,
		stand_start = 1,
		stand_end = 5,
		run_start = 1,
		run_end = 5,
		shoot_start = 6,
		shoot_end = 15,
	},
	drops={
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "more_mese:mese_crystal_fragment_orange", chance=2},
		{name = "more_mese:mese_crystal_fragment_orange", chance=2},
	},
	--[[
	do_custom=function(self)
		if self.state~="attack" then return end
		if math.random(1,2)==1 then
			self.arrow="mob_wasp:sting_smoke"
		else
			self.arrow="mob_wasp:sting"
		end
	end
	]]
})

mobs:register_mob("mob_wasp:wasp_leader_smokey", {
	type = "monster",
	reach = 2.5,
	damage = 3,
	attacks_monsters=false,
	attack_type = "shoot",
	arrow = "mob_wasp:sting",
	shoot_interval = 2,
	shoot_offset = -0.5,
	dogshoot_switch=5,
	hp_max = 50,
	collisionbox = {-0.4, -0.1, -0.4, 0.4, 2.5, 0.4},
	fly = true,
	fly_in={"air"},
	floats=1,
	fall_speed = 0,
	visual = "mesh",
	mesh = "wasp.b3d",
	textures = {
		{"dmobs_wasp.png^[multiply:green"},
	},
	sounds = {
		-- war_cry = "wasp",
		shoot_attack = "wasp",
	},
	blood_texture = "mobs_blood.png",
	visual_size = {x=3.5, y=3.5},
	makes_footstep_sound = false,
	walk_velocity = 1,
	run_velocity = 3.25,
	jump = true,
	env_dmg={["smoke:smog"]=-0.5, ["default:ice"]=2 },
	water_damage = 0.5,
	view_range = 24,
	animation = {
		speed_normal = 6,
		speed_run = 15,
		walk_start = 1,
		walk_end = 5,
		stand_start = 1,
		stand_end = 5,
		run_start = 1,
		run_end = 5,
		shoot_start = 6,
		shoot_end = 15,
	},
	drops={
		{name = "default:mese_crystal_fragment", chance=2},
		{name = "more_mese:mese_crystal_fragment_green", chance=2},
		{name = "more_mese:mese_crystal_fragment_green", chance=2},
	},
})

mobs:spawn_specific("mob_wasp:wasp_leader", {"air"}, {"default:stone_with_tin"}, 0, 15, 59, 200, 2, 0, 200, nil)
mobs:spawn_specific("mob_wasp:wasp_leader_smokey", {"air"}, {"default:stone_with_coal"}, 0, 15, 61, 200, 2, 0, 200, nil)
mobs:spawn_specific("mob_wasp:wasp_leader_red", {"air"}, {"default:desert_stone_with_tin"}, 1, 15, 59, 200, 2, 0, 200, nil)

