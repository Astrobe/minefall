-- This is a slightly modified version of my own published mod.
--
-- Author (Code): Astrobe
-- licence: CC-BY-SA 4.0
-- Version 3 
--
-- Textures from SmallJoker's "bitchange" mod, CC0.

local reputations=minetest.get_mod_storage()

rp={}

-- Award 'amount' of RP to player 'name'.
-- Amount can be negative

function rp.get_player_count(name)
	return reputations:get_int(name)
end

function rp.award(name, amount )
	local current=reputations:get_int(name)
	if not current then return end
	local new=current+amount
	reputations:set_int(name, new)
	local privs=minetest.get_player_privs(name)
	if new<=0 and privs.interact==true then
		privs.interact=nil
		minetest.set_player_privs(name, privs)
		minetest.chat_send_player(name, minetest.colorize("red","No more RPs. Reverting to guest status."))
	end
	if new>0 and not privs.interact==true then
		privs.interact=true
		minetest.set_player_privs(name, privs)
		minetest.chat_send_player(name, minetest.colorize("lightgreen","Gest status lifted. You can now interact with the world."))
	end
	local player=minetest.get_player_by_name(name)
	if player and bones.get_mode_for(player)=="drop" then armor:set_player_armor(player) end
end


-- minetest.register_privilege("rep", {"give/take reputation points", false})


minetest.register_chatcommand("rep", {
	description="Give or view reputation points (RPs). If a negative number of RPs is given, the amount is subtracted from the player's RPs (bad reputation).\n In all cases, the points are taken from your own RPs.",
	params="<name> <number> (give RPs) or <name> (view RPs)",
	func=function(name, param)
		local receiver, amount=string.match(param,"^([%a%d_-]+) (-?%d+)")
		if not amount then
			receiver=string.match(param,"[%a%d_-]+")
		end
		if not receiver then
			 minetest.chat_send_player(name, string.format("Your current RPs: %d",(reputations:get_int(name) or 0)))
			return
		end
		if not minetest.player_exists(receiver) then
			minetest.chat_send_player(name, "Who?")
			return
		end
		if receiver and amount then
			local n=math.floor(tonumber(amount))
			local admin=minetest.check_player_privs(name, "give")
			if minetest.check_player_privs(name, "give") then
				minetest.chat_send_player(name, minetest.colorize("yellow","You gifted "..n.." RPs to ".. receiver))
			elseif rp.get_player_count(name) > math.abs(n) then
				rp.award(name, -math.abs(n))
				minetest.chat_send_player(name, minetest.colorize("yellow","You gave "..n.." RPs to ".. receiver))
			else
				minetest.chat_send_player(name,"You don't have enough RPs")
				return
			end
			rp.award(receiver, n)
			return
		end
		if receiver then
			local rps=reputations:get_int(receiver)
			minetest.chat_send_player(name, string.format("%s has %d RPs", receiver, rps))
		end
	end})

minetest.register_on_dieplayer(function(player, reason)
	local name=player:get_player_name()
	if minetest.check_player_privs(name, "interact") and (vector.distance(player:get_pos(), beacon.home(name)) > 100) then
		rp.award(name, -1)
		minetest.chat_send_player(name, minetest.colorize("red","You lost 1 RP"))
	end
end) 

--[[
minetest.register_craftitem("rp:coin",
{
	description="RP Coin (gives 1 RP on use)",
	inventory_image="bitchange_minecoin.png",
	on_use=function(itemstack,  user, pointed_thing)
		if not user or not user:is_player() then return nil end
		local name=user:get_player_name()
		rp.award(name, 1)
		minetest.chat_send_player(name, string.format("Gained 1 RP (now: %d)", reputations:get_int(name) or 0))
		return itemstack
	end
})
]]
