-- Melt.lua
-- Author: Astrobe
-- Licence: MIT

-- This should be compatible with MTG (or any game using the default mod), although the threshold for "freezing" (35) is
-- tuned for Minefall (but I believe I used Snowdrift's threshold, which was made for MTG).

-- ice melts outside of freezing biomes

local schedule_melting= function(pos)
	-- 50 is tree_limit in mapgen and 35 is the temp. for snowy biomes.
	if pos.y>=50 or minetest.get_heat(pos)<=35 then return end
	minetest.get_node_timer(pos):start(math.random(600, 900))
end

minetest.override_item("default:ice",
{
	on_timer=minetest.remove_node,
	on_construct=schedule_melting,
})

minetest.override_item("default:snowblock",
{
	on_timer=minetest.remove_node,
	on_construct=schedule_melting,
})

