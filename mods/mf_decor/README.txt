This is a mix of the Darkage mod and the X-Decor mod.

Author (code): Astrobe
License (code): MIT
License and credits (textures):
	For X-decor textures, see README1.md and LICENCE
	for darkage textures, see README2.md
	"castle_street_light.png" texture borrowed from "Castle Lighting" by Facedeer, MIT licence.
	toilet_top.png by Astrobe, CC BY-SA 3.0. Based on MTG's
default_chest_top.png. 

