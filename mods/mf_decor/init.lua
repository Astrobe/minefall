default.register_chest("cabinet",
{
		description="Cabinet",
	tiles = {
		"xdecor_cabinet_sides.png", "xdecor_cabinet_sides.png",
		"xdecor_cabinet_sides.png", "xdecor_cabinet_sides.png",
		"xdecor_cabinet_sides.png","xdecor_cabinet_front.png" 
	},
	sounds = default.node_sound_wood_defaults(),
	sound_open = "default_chest_open",
	groups = {oddly_breakable_by_hand = 2, flammable=3, furniture=1, protected=1},
})

minetest.register_craft({
	type='shapeless',
	output = 'default:cabinet',
	recipe = { "default:chest_locked", "default:bronzeblock"}
})


mf_doors.register_trapdoor("mf_decor:trapdoor", {
	description = "Paper frame door",
	tile_front = "xdecor_itemframe.png",
	tile_side = "xdecor_itemframe.png",
	gain_open = 0.06,
	gain_close = 0.13,
	groups = {oddly_breakable_by_hand = 2, flammable = 2, door = 1},
})

minetest.register_craft({
	output = "mf_decor:trapdoor",
	type="shapeless",
	recipe = {
		"mf_doors:trapdoor", "mf_decor:wood_frame"
	}
})

minetest.register_node("mf_decor:slate_cobble", {
		description = "Slate Cobble",
		tiles = {"darkage_slate_cobble.png^[transform1"},
		--tiles = {"darkage_slate_cobble.png"},
		groups = {cracky=3},
		sounds = default.node_sound_stone_defaults(),
		drop="stairs:stair_slate_cobble 4",
		})

stairs.register_stair_and_slab("slate_cobble",
	"mf_decor:slate_cobble",
	true,
	{"darkage_slate_cobble.png"},
	"Slate Cobble Stair",
	"Slate Cobble Slab",
	default.node_sound_stone_defaults()
)

minetest.register_node("mf_decor:slate", {
		description = "Slate",
		tiles = {"darkage_slate.png"},
		groups = {cracky=2, stone=1},
		sounds = default.node_sound_stone_defaults(),
		drop="mf_decor:slate_cobble",
		})



minetest.register_node("mf_decor:ors_cobble", {
		description = "Terracotta cobble",
		--tiles = {"darkage_ors_cobble.png^[multiply:orange"},
		tiles = {"default_cobble.png^[brighten^[multiply:#E2725B^[transform1"},
		groups = {cracky=3},
		sounds = default.node_sound_stone_defaults(),
		drop="stairs:stair_ors_cobble 4",
		})

stairs.register_stair_and_slab("ors_cobble", "mf_decor:ors_cobble",
	true,
	{"default_cobble.png^[brighten^[multiply:#E2725B^[transform1"},
	"Terracotta Cobble Stair",
	"Terracotta Cobble Slab",
	default.node_sound_stone_defaults()
)


minetest.register_node("mf_decor:stone_brick", {
	description = "Stone Brick",
	tiles = {"darkage_stone_brick.png"},
	groups = {cracky=1},
	sounds = default.node_sound_stone_defaults(),
	drop="stairs:stair_stone_brick 4"
})

stairs.register_stair_and_slab("stone_brick", "mf_decor:stone_brick",
	true,
	{"darkage_stone_brick.png"},
	"Stone Brick Stair",
	"Stone Brick Slab",
	default.node_sound_stone_defaults()
)


minetest.register_node("mf_decor:schist", {
	description = "Aggregate",
	tiles = {"default_clay.png"},
	groups = {cracky=2, },
	sounds = default.node_sound_stone_defaults(),
	drop="stairs:stair_schist 4",
})

stairs.register_stair_and_slab("schist", "mf_decor:schist",
	true,
	{"darkage_schist.png"},
	"Aggregate Stair",
	"Aggregate Slab",
	default.node_sound_stone_defaults()
)

minetest.register_node("mf_decor:adobe", {
	description = "Adobe",
	tiles = {"default_desert_sandstone.png"},
	groups = {cracky=3},
	sounds = default.node_sound_stone_defaults(),
	drop="stairs:stair_adobe 4",
})

stairs.register_stair_and_slab("adobe", "mf_decor:adobe",
	true,
	{"default_desert_sandstone.png"},
	"Adobe Stair",
	"Adobe Slab",
	default.node_sound_stone_defaults()
)


minetest.register_node("mf_decor:lamp2", {
	description = "Obsidian lamp",
	tiles = {"xdecor_iron_lightbox.png"},
	paramtype = "light",
	light_source = 10,
	groups = { cracky=1, protected=1, furniture=1},
	sounds = default.node_sound_glass_defaults(),
})

minetest.register_craft({
	output = "mf_decor:lamp2",
	type="shapeless",
	recipe= {"mf_decor:paper_lamp", "default:obsidian_glass" },
})

minetest.register_node("mf_decor:glass", {
	description = "Medieval Glass",
	drawtype = "glasslike",
	tiles = {"darkage_glass.png"},
	paramtype = "light",
	sunlight_propagates = true,
	groups = { cracky=2 },
	sounds = default.node_sound_glass_defaults(),
})


minetest.register_node("mf_decor:wood_frame", {
	description = "Paper screen",
	tiles = {"xdecor_itemframe.png"},
	paramtype = "light",
	sunlight_propagates = true,
	groups = {oddly_breakable_by_hand=3, flammable=1},
	sounds = default.node_sound_wood_defaults()
})

minetest.register_craft({
	output = "mf_decor:wood_frame",
	type="shapeless",
	recipe = {"default:paper","default:paper","default:paper","group:wood"}
})

minetest.register_node("mf_decor:paper_lamp", {
	description = "Paper lamp",
	tiles = {"xdecor_itemframe.png^[hardlight:xdecor_itemframe.png"},
	paramtype = "light",
	sunlight_propagates = true,
	groups = {oddly_breakable_by_hand=3, flammable=1},
	sounds = default.node_sound_wood_defaults(),
	light_source = 10,
})

minetest.register_craft({
	output = "mf_decor:paper_lamp",
	type="shapeless",
	recipe = {"mf_decor:wood_frame","group:lamp"}
})

minetest.register_craft({
	output="default:sandstone",
	type="cooking",
	recipe="group:sand",
	cooktime=7,
})


minetest.register_craft({
	output="mf_decor:schist",
	type="cooking",
	recipe="default:gravel",
	cooktime=7,
})

minetest.register_craft({
	output="mf_decor:ors_cobble",
	type="cooking",
	recipe="default:dirt",
	cooktime=7,
})

minetest.register_craft({
	output = "mf_decor:adobe 2",
	type="shapeless",
	recipe= {"default:dirt", "farming:straw" },
})

minetest.register_node("mf_decor:toilet", {
	description="Toilet",
	tiles={"toilet_top.png", "default_chest_side.png"},
	drawtype="nodebox",
	node_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, 0, 0.5},
	},
	paramtype = "light",
	groups= {choppy=3, protected=1, furniture=1},
	sounds=default.node_sound_wood_defaults(),
})

minetest.register_craft({
	output = "mf_decor:toilet",
	type="shapeless",
	recipe= {
		"default:chest_locked",
		"craft_table:simple", "craft_table:simple", "craft_table:simple", "craft_table:simple",
		"craft_table:simple", "craft_table:simple", "craft_table:simple", "craft_table:simple",
	},
})
