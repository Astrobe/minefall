
mobs:register_mob("mob_beetle:beetle", {
	type = "monster",
	passive = false,
	reach = 2,
	damage = 2,
	attack_type = "dogfight",
	hp_max = 30,
	collisionbox = {-0.40,0,-0.40, 0.40,0.9,0.40},
	visual = "mesh",
	visual_size = {x=0.75, y=1.5},
	mesh = "beetle.b3d",
	textures = {
		{"mobs_beetle.png"},
	},
	blood_texture = "mobs_blood.png",
	makes_footstep_sound = true,
	walk_velocity = 1,
	run_velocity = 3.75,
	jump = 1,
	jump_height=7,
	env_dmg= { ["default:ice"]=2 },
	fall_damage = 0,
	view_range = 16,
	drops = {
		{name = "farming:seed_wheat", chance=2},
		{name = "farming:seed_wheat", chance=2},
		{name = "default:mese_crystal_fragment", chance=2},
	},
	animation = {
		speed_normal = 27,
		speed_run = 27,
		stand_start = 1,
		stand_end = 20,
		walk_start = 25,
		walk_end = 45,
		run_start = 25,
		run_end = 45,
		punch_start = 1,
		punch_end = 20,
	},

	env_alt = {
		["default:fern_3"]={ "default:pine_bush_sapling", 30},
		["default:pine_bush_sapling"]={ "default:pine_sapling", 10},
		["default:giantgrass"]={ "default:bush_sapling", 30},
		["default:bush_sapling"]={ "default:aspen_sapling", 10},
		["default:grass_5"]={ "default:blueberry_bush_sapling", 30},
		["farming:cotton"]={"air",4},
		["farming:wheat"]={"air",4},
	},
})


mobs:register_spawn("mob_beetle:beetle", {"default:giantgrass", "default:fern_3"}, 15, 0, 750, 3, 120)
