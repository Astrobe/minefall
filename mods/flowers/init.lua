-- Minetest 0.4 mod: default
-- See README.txt for licensing and other information.


-- Namespace for functions

flowers = {}


-- Map Generation

dofile(minetest.get_modpath("flowers") .. "/mapgen.lua")


--
-- Flowers
--

-- Aliases for original flowers mod

minetest.register_alias("flowers:flower_rose", "flowers:rose")
minetest.register_alias("flowers:flower_tulip", "flowers:tulip")
minetest.register_alias("flowers:flower_dandelion_yellow", "flowers:dandelion_yellow")
minetest.register_alias("flowers:flower_geranium", "flowers:geranium")
minetest.register_alias("flowers:flower_viola", "flowers:viola")
minetest.register_alias("flowers:flower_dandelion_white", "flowers:dandelion_white")


-- Flower registration
local random=math.random

local function add_simple_flower(name, desc, box, f_groups)
	-- Common flowers' groups
	f_groups.snappy = 3
	f_groups.flower = 1
	f_groups.flora = 1
	f_groups.attached_node = 1

	minetest.register_node("flowers:" .. name, {
		description = desc,
		drawtype = "plantlike",
		floodable=true,
		-- waving = 1,
		tiles = {"flowers_" .. name .. ".png"},
		inventory_image = "flowers_" .. name .. ".png",
		wield_image = "flowers_" .. name .. ".png",
		sunlight_propagates = true,
		paramtype = "light",
		paramtype2="meshoptions",
		walkable = false,
		buildable_to = true,
		stack_max = 99,
		groups = f_groups,
		sounds = default.node_sound_leaves_defaults(),
		selection_box = {
			type = "fixed",
			fixed = box
		},
		place_param2=1,
	})
end

flowers.datas = {
	{
		"rose",
		"Red Rose",
		{-2 / 16, -0.5, -2 / 16, 2 / 16, 5 / 16, 2 / 16},
		{color_red = 1}
	},
	{
		"tulip",
		"Orange Tulip",
		{-2 / 16, -0.5, -2 / 16, 2 / 16, 3 / 16, 2 / 16},
		{color_orange = 1}
	},
	{
		"dandelion_yellow",
		"Yellow Dandelion",
		{-4 / 16, -0.5, -4 / 16, 4 / 16, -2 / 16, 4 / 16},
		{color_yellow = 1}
	},
	{
		"chrysanthemum_green",
		"Green Chrysanthemum",
		{-4 / 16, -0.5, -4 / 16, 4 / 16, -1 / 16, 4 / 16},
		{color_green = 1}
	},
	{
		"geranium",
		"Blue Geranium",
		{-2 / 16, -0.5, -2 / 16, 2 / 16, 2 / 16, 2 / 16},
		{color_blue = 1}
	},
	{
		"viola",
		"Viola",
		{-5 / 16, -0.5, -5 / 16, 5 / 16, -1 / 16, 5 / 16},
		{color_violet = 1}
	},
	{
		"dandelion_white",
		"White Dandelion",
		{-5 / 16, -0.5, -5 / 16, 5 / 16, -2 / 16, 5 / 16},
		{color_white = 1}
	},
	{
		"tulip_black",
		"Black Tulip",
		{-2 / 16, -0.5, -2 / 16, 2 / 16, 3 / 16, 2 / 16},
		{color_black = 1}
	},
}

for _,item in pairs(flowers.datas) do
	add_simple_flower(unpack(item))
end


-- Flower spread
-- Public function to enable override by mods

function flowers.flower_spread(pos, node)
	pos.y = pos.y - 1
	local under = minetest.get_node(pos)
	local soiltype=under.name
	pos.y = pos.y + 1


	-- flora removed if not on crumbly nodes in order to prevent abusive gardening.
	if minetest.get_item_group(soiltype, "crumbly")==0 then
		minetest.remove_node(pos)
		return
	end

	local light = minetest.get_node_light(pos, 0.5)
	if not light or light < 12 then
		minetest.set_node(pos, {name="flowers:mushroom_brown"})
		return
	end

	if node.param2==1 then return end -- player-placed flowers don't spread.

	local pos0 = vector.subtract(pos, 4)
	local pos1 = vector.add(pos, 4)
	-- Testing shows that a threshold of 3 results in an appropriate maximum
	-- density of approximately 7 flora per 9x9 area.
	if #minetest.find_nodes_in_area(pos0, pos1, "group:flora") > 3 then
		return
	end

	local soils = minetest.find_nodes_in_area_under_air( pos0, pos1, soiltype)
	local num_soils = #soils
	if num_soils >= 1 then
		for si = 1, math.min(3, num_soils) do
			local soil = soils[random(num_soils)]
			local soil_name = minetest.get_node(soil).name
			local soil_above = {x = soil.x, y = soil.y + 1, z = soil.z}
			minetest.set_node(soil_above, node)
		end
	end
end

minetest.register_abm({
	label = "Flower spread",
	nodenames = "group:flora",
	interval = 37,
	chance = 300,
	action = flowers.flower_spread,
	min_y=-999, 
	max_y=60, -- tree limit in mapgen
})


--
-- Mushrooms
--

minetest.register_node("flowers:mushroom_red", {
	description = "Red Mushroom",
	tiles = {"flowers_mushroom_red.png"},
	inventory_image = "flowers_mushroom_red.png",
	wield_image = "flowers_mushroom_red.png",
	drawtype = "plantlike",
	paramtype = "light",
	light_source=8,
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	groups = {snappy = 3, attached_node = 1, flammable = 1},
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-4 / 16, -0.5, -4 / 16, 4 / 16, -1 / 16, 4 / 16},
	},
	floodable=true,
})

minetest.register_node("flowers:mushroom_brown", {
	description = "Brown Mushroom",
	tiles = {"flowers_mushroom_brown.png"},
	inventory_image = "flowers_mushroom_brown.png",
	wield_image = "flowers_mushroom_brown.png",
	drawtype = "plantlike",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	groups = {food_mushroom = 1, snappy = 3, attached_node = 1, flammable = 1, heal=1},
	sounds = default.node_sound_leaves_defaults(),
	on_use = minetest.item_eat(1),
	selection_box = {
		type = "fixed",
		fixed = {-3 / 16, -0.5, -3 / 16, 3 / 16, -2 / 16, 3 / 16},
	},
	floodable=true,
})


-- Mushroom spread
-- Brown mushrooms spread from tree group to tree group.
-- However, if they have nowhere to spread they turn into red mushrooms.

function flowers.mushroom_spread(pos, node)
	
	if minetest.get_node_light(pos)>3 then return end -- won't spread
	local below={x=pos.x, y=pos.y-1, z=pos.z}
	if minetest.get_item_group(minetest.get_node(below).name, "tree")==0 then
		return
	end

	minetest.swap_node(below, {name="default:dirt"})
	local positions = minetest.find_nodes_in_area_under_air(
		{x = pos.x - 1, y = pos.y - 1, z = pos.z - 1},
		{x = pos.x + 1, y = pos.y - 1, z = pos.z + 1},
		"group:tree")
	if #positions<1 then
	 	minetest.set_node(pos, {name="flowers:mushroom_red"})
		return
	end
	local p = positions[random(#positions)]
	p.y = p.y + 1
	minetest.set_node(p, {name = "flowers:mushroom_brown"}) 
end

minetest.register_abm({
	label = "Mushroom spread",
	nodenames = {"flowers:mushroom_brown"},
	interval = 67, -- prime number
	chance = 40,
	action = flowers.mushroom_spread,
	catch_up=true,
})

