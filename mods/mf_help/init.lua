-- mf_help
--- Code
-- Author: Astrobe
-- License: LGLP 2.1+
--- Media
-- Author: Robin Lamb
-- License: CC0
-- Part of https://opengameart.org/content/ui-sound-effects-button-clicks-user-feedback-notifications

local storage=minetest.get_mod_storage()

local function givetip(name)
	if not minetest.get_player_by_name(name) then return end
	local tips=
	{
		"Coal in stone can catch fire and explode.",
		"Bored at night? Keep the crafting and furnace work for night time.",
		"The compass can detect nearby ores.",
		"Get the news with /news.",
		"Keep track of how many lamps you have. You don't want to forget one somewhere.",
		"Catch butterflies with a net and place them in a hive to get honey. Place flowers next to the hive.",
		"You can catch fish with a net. Be aware that missing damages the net too.",
		'Check who is online with /status.',
		"Storms bring bad news. Take cover.",
		"Danger often comes from above. Look *up*.",
		"One day is one hour.",
		"Beware: seawater floods.", 
		"Whales appear during the day, near snow above 60 meters.",
		"Different woods have different burning times.",
		"Most mobs regenerate HP over time. Finish them off quickly and be careful when you chase one.",
		"Know when to run away.",
		"The rock crab, the ice spider, the orange and green giant wasps can drop colored Mese.",
		"The giant beetle and the scorpionoid drop seeds.",
		"Don't spam the sword. Wait until it comes back to rest position for another hit.",
		"The hammer is slow but has a great stun effect.",
		"You can set your home beacon by hitting a beacon with a green mese crystal. The HUD X mark will be updated accordingly.",
		"Beware not to hit your allies.",
		"There are two clans: men and women. Players can only use the beacons and void chests of their clan.",
		"Beginners: don't assume, check or ask and listen. Regulars: just because you got yourself good equipment doesn't mean you are invincible.",
		"Placing loot boxes is the only way to get RP.",
		"You can check if a block is under your protection with the screwdriver.",
		"You can open a loot box for 0 RP with a green mese crystal fragment.",
		"You can pick up most blocks (including ruins walls) with an obsidian shard.",
		"To mount/unmount whales, use right-click. To change its direction while riding, target the whale and left-click.",
		"You can right-click a block with the crafting guide to see its description.",
		"Keys can be labeled by punching a sign.",
		"Use the radar mode of the minimap to spot dungeons and caves.",
		"The light, furniture and protection attributes can be searched in the crafting guide.",
		"Eating at full health (or when there's more than enough to refill your HP bar) refills your mana for half of the extra HP value.",
		"Saplings growing next to melding will absorb it and turn into aberrant tree saplings, which grow in variant strange giant trees.",
		"If you have too many of one kind of seed, you can convert it to the other kind with red mushrooms.",
		"Stunning effects do stack, with bonus duration. Stun effects are however reduced for fleeing mobs.",
		"The teleportation wand resets your momentum; teleporting can be way to break a fall.",
		"You can right-click a trader with an empty hand to move it out of your way",
	}
	minetest.sound_play("ding", {gain=1, to_player=name}, true)
	minetest.chat_send_player(name, minetest.colorize("lightgreen",tips[math.random(1,#tips)]))
	minetest.after(3600, givetip, name)
end

local help_msg=
{
	"#1 priority: stay alive. Collect food for healing. Look for apples, mushrooms or blueberries.\nStay in green grass areas and just run away if you are attacked (your crosshair turns red).\nHide (e.g. in a ruin) if you need to stop to do something. These messages can be read again in the chat log.",
	"Look for lootboxes (see inventory screen) in ruins. Dig them until you get 2 crafting tables and 6 bronze ingots.",
	"Digging lootboxes costs Reputation Points (RP), which are used to level up. You start with 20. You lose 1 RP if you die."..
	"You won't be able to dig lootboxes if you have no RP. One can earn RPs by placing lootboxes, but we are not here yet.",
	"Collect those red luminous mushrooms. They can provide light in caves, but can be used to craft more convenient lamps.\nKeep the brown mushrooms for farming, if you can.",
	"With your first 6 bronze ingots, make a pick and an axe, which can be used as a weapon.\nFor \"sticks\", use bush trunks (do collect the leaves, they'll be very useful).",
	"The axe is a second-rate weapon. It is slow, so if your attacks miss often, make sure it has returned to rest position before attacking again.",
	"For better melee weapons, there are swords and spears. Spears have the same durability as swords for the cost, deal less damage, but are faster and have longer reach.",
	"Nights are not more dangerous here. The big moon even gives good light when the sky is clear.",
	"Look for ores and coal. They can be found at any altitude. Good spots are cliffs.",
	"Ores can be found in red and grey stones.They drop a random metal lump.",
	"With a weapon, you can venture in savanna or pine forests, but still be careful. Often look around and *up* because attacks can come from the sky too.",
	"The compass, that you may have gotten from lootboxes, has a metal ore detection feature.\nYou can try it near one you have already spotted to test it.",
	"Look for cobble in ruins; you will need one block to make a furnace. The furnace allows you to turn those metal ores into usable ingots.",
	"Note that you only need 3 copper and 1 tin to make bronze",
	"Making ingots from ores takes longer and more fuel than in most other games. Some trees are better fuels than other, checkout the descriptions in the crafting guide.",
	"Start to look for a place where to build your town. The ideal place would be ruins in good shape near apple trees and water (for farming).\n",
	"Also look for coral reefs, as they are good fishing spots. Fish is important for trade.",
	"Ruins or surface dungeons are cheap first homes because repairing them costs less material than building from the ground up. Dungeons might already be \"occupied\", though.",
	"Most ruin blocks cannot be dug with normal tools, you'll need obsidian for that; but we are not here yet.",
	"If your future town is a bit far from the beacon, you can make a Scroll Of Return to set your second waypoint here.",
	"Start a mushroom farm. Mushrooms grow in nearly complete darkness, on tree logs. You'll have to renew the logs.",
	"A mushroom farm will secure a reliable source of food (and/or red mushrooms) and allow you to make sandwiches, which can be traded with NPC traders for lootboxes.",
	"When you have all the basic bronze tools and a weapon, you can go hunting for seeds and mese.",
	"Giant beetles drop wheat seeds, while scorpionoids drop cotton seeds.\nPrioritize cotton for now.",
	"Drops are not garanteed; sometimes you get nothing.",
	"Beetles lurk in pine woods and apple tree forests; scorpionoids lurk in savannas and jungles.",
	"Your first 9 mese shards should be used to make a mese wand, a ranged weapon that will make things a bit easier.",
	"Plants are sensitive to climate. They take more time to grow when outside of their usual biome.",
	"Thunderstorms can bring more trouble than you need, so if you see lightnings, look for a shelter.",
	"We are in spring now, so the rainy winter is behind us; the weather should get better until summer.",
	"When you have a bunch of seeds, start a farming plot.\nPlace it in a very well lit location next to water.",
	"Harvesting does not give back seeds. So keep hunting while things grow.",
	"NPC traders accept trades that are the only way to get important items you cannot craft. Checkout the Help tab.",
	"In order to attract traders, you need a properly lit house, with a roof, furniture, and cloth. Cloth is made from cotton.",
	"You can for instance put cloth blocks as 'carpets' right next to a bookshelf with a lamp. The \"furniture\" attribute is searchable in the crafting guide.",
	"Traders don't like crowds, they won't appear if you or another trader are near (~10 blocks).",
	"While waiting for traders to come, go fishing. Fish is the second ingredient (with bread) to make tradable sandwishes.",
	"The alternative is candy apples, for which you need to make beehives to get honey. In both case you'll need a net to catch fishes or butterflies.",
	"Yet another option to get honey is to hunt giant yellow wasps.",
	"Speaking of wasps, you should try to hunt the green ones too, for the green mese they drop.",
	"One use of green mese is to set your \"home\" beacon. You can use the initial beacon for that.",
	"Another use of green mese is to open lootboxes without losing RPs.",
	"So if you place a lootbox and open it with a green mese shard, you get 1 RP and the content of the lootbox.",
	"Accumulate RPs until you can level up (this will take a while).",
	"The upside of leveling up: you can use armor and the focus wand, your mana pool increases with the RP you have.",
	"It may be wise to make a copy of the blue book. It is not copyrighted, and it is a unique item.",
	"Fruits don't regrow forever so keep farming wheat and mushrooms to secure food. Beyond survival, eating when at full health refills your mana.",
	"You might have noticed luminuous purple blocks: it is \"Melding\"; it will expand and become an issue eventually if you don't take action.",
	"There are a couple of ways to remove it, but for now contain it by building around it.",
	"Checkout the Help tab in your inventory screen for more tips.\nGood luck and have fun!",
}

-- A pass to colorize our messages
for i,v in ipairs(help_msg) do
	help_msg[i]=minetest.colorize("lightgreen", help_msg[i])
end

function give_help(name)	
	if not minetest.get_player_by_name(name) then return end
	local n=math.max(storage:get_int(name),1)
	if not help_msg[n] then  -- done with help.
		givetip(name)
		return
	end
	minetest.sound_play("ding", {gain=1, to_player=name}, true)
	minetest.chat_send_player(name, help_msg[n])
	storage:set_int(name, n+1)
	minetest.after(120, give_help, name)
end

local function remind_guest(name)
	if not minetest.get_player_by_name(name) then return end
	if minetest.check_player_privs(name, "interact") then
		givetip(name)
		return
	end
	minetest.chat_send_player(name, minetest.colorize("lightgreen", "YOU ARE IN GUEST MODE. Players can invite you into the game, try to chat with them"))
	minetest.sound_play("ding", {gain=1, to_player=name}, true)
	minetest.after(120, remind_guest, name)
end

minetest.register_on_joinplayer(function(player)
	local name=player:get_player_name()

	if minetest.check_player_privs(name, "privs") then
		minetest.after(10, give_help, name)
	elseif not minetest.check_player_privs(name, "interact") then
		minetest.after(10, remind_guest, name)
	else
		minetest.after(10, givetip, name)
	end
end)

local no=function() return 0 end
local nocraft=minetest.create_detached_inventory("nocraft", {allow_move=no, allow_put=no, allow_take=no})
local nocraft_items=
{
	"default:bush_stem", "farming:seed_wheat", "farming:seed_cotton", "dungeon_crates:crate", "compass:compass", "mf_airtank:airtank", "default:obsidian", "",
	-- "default:mese_crystal_fragment", "more_mese:mese_crystal_fragment_green", "more_mese:mese_crystal_fragment_grey", "more_mese:mese_crystal_fragment_cyan", "more_mese:mese_crystal_fragment_red", "more_mese:mese_crystal_fragment_orange", "", "",
	-- "beacon:beacon", "3d_armor:helmet_greater","3d_armor:chestplate_greater","3d_armor:leggings_greater","3d_armor:boots_greater","", "", "",

}

nocraft:set_size("main", #nocraft_items)
nocraft:set_list("main", nocraft_items)
sfinv.register_page("help", {
	title="HELP",
	get= function(self, player, context)
		local content="label[0,0;Notable items that cannot be crafted:]list[detached:nocraft;main;0,1;8,5;]"
                .."textarea[0.3,2;8,7;;;"..
	minetest.formspec_escape(
	"\nMain differences from default Minetest gameplay:\n\n"..
	"- You need a crafting table. Crafting recipes are often different.\n"..
	"- Digging/cutting/breaking more often requires the appropriate tool (no kung-fu tree-breaking here...)\n"..
	"- Trees must be cut down from the top block with an axe.\n"..
	"- It take a lot more time (and fuel) to melt things in the furnace (making a couple of ingots can require a tree or two, depending on the wood and the metal).\n"..
	"- Seawater floods mostly like in real life. You cannot build in it.\n"..
	"- Dirt, snow and gravel nodes fall.\n"..
	"- Days are longer, plants take a lot more time to grow.\n"..
	"- Fruits regrow a limited number of times.\n"..
	"- Bookshelves are not storage, but can be right-clicked to see crafting recipes.\n"..
	"- Some blocks can only be removed with obsidian shards.\n"..

	"\nReputation points (RP):\n\n"..
	"- You lose 1 RP if you die\n"..
	"- Losing all your RPs reverts you to guest status; you don't lose your level, just the 'interact' privilege. Singleplayer cannot lose 'interact', but won't be able to dig loot boxes\n"..
	"- You can give/check RPs with the /rep command\n"..
	"- RPs allow you to level up\n"..

	"\nLevels:\n\n"..
	"- Guest: cannot interact with the world and mobs ignore the player. Another player must give them at least 1 RP in order to actually play.\n"..
	"- Beginner: keep their inventory on death. Cannot use armor and some wands. Level up by choosing a Focus (see /focus).\n"..
	"- Regular: Inventory is stored into 'bones' on death. Can use the focus wand. Can use armor. Level up by choosing a Talent (see /talent).\n"..
	"- Advanced: Inventory drops on the floor on death. Can use Void Chests. Mana pool extended by 1% of RPs.\n\n"..
	"Player characters have a random \"gift\". It can be one of: less wear on fishnet, less wear on mese wand, or better levitation uplift. The nature of the gift is not revealed to the player.\n"..
	"Penalities for dying do not apply if it is within 100 m of your home beacon.\n"..

	"\nProtection:\n\n"..
	"Search for \"protection\" in the crafting guide.\n"..
	"Protective blocks protect themselves and their neighbors against digging (by others), fire and blasts.\n"..

	"\nArmor piece bonus:\n\n"..
	"- Helmet: mana regeneration\n"..
	"- Chestplate: mana pool\n"..
	"- Leggings: jump height\n"..
	"- Boots: walk speed\n"..
	"There is a chance that the armor absorbs all of the incoming damage but 1 HP. In this case, all armor elements take damage. Each armor element you wear adds 20% chance.\n"..
	"All armor elements have a weight, which reduces jump height and walk speed (leggings and boots bonus compensate more than their own weight).\n"..

	"\nNPC trader:\n\n"..
	"Traders spawn on any cloth block near furniture (e.g. a bookshelf) sufficiently lighted (but not under the sun).\n"..

	"\nFarming:\n\n"..
	"- Craft a farming plot by mixing seeds and dirt (see crafting guide).\n"..
	"- Place the farming plot under the sun, with water next to it.\n"..
	"- Harvesting cotton or wheat does not give back seeds.\n"..

	"\nGardening\n\n"..
	"- Wild flowers have an [X] shape while garden (player placed) flowers have a [+] shape.\n"..
	"- Garden flowers don't spread and may prevent wild flowers from spreading.\n"..
	"- Trunks or branches (but not foliage) will replace blocks, even if protected.\n"..
	"- This also true for aberrant trees, including foliage.\n"..

	"\nLabeling:\n\n"..
	"Keys can be labeled with a custom text by punching a sign.\n"..

	"\nClans:\n\n"..
	"- Players belong either to the Men or Women clan, according to their skin (see /skin)"..
	"- Players of one clan cannot use beacons, D-eacons, anvils, windmills or Void Chests of the other clan.\n"..

	"\nAdditional chat commands:\n\n"..
	"/rep\n"..
	"/chat\n"..
	"/focus\n"..
	"/talent\n"..
	""
	).. "]"
		return sfinv.make_formspec(player, context, content, false, nil)
	end
})



