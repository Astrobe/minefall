-- This uses a trick: you can first define the recipes using all of the base
-- colors, and then some recipes using more specific colors for a few non-base
-- colors available. When crafting, the last recipes will be checked first.

local dyes = {
	{"white",      "White",      nil},
	{"red",        "Red",        "flowers:rose"},
	{"orange",     "Orange",     "flowers:tulip"},
	{"violet",     "Violet",     "flowers:viola"},
	{"blue",       "Blue",       "flowers:geranium"},
	{"yellow",     "Yellow",     "flowers:dandelion_yellow"},
	--{"cyan",       "Cyan",       "flowers:mushroom_red"},
	{"green",      "Green",      "flowers:chrysanthemum_green"},
	{"black",      "Black",      "flowers:tulip_black"},
	--[[
	{"grey",       "Grey",       "basecolor_grey"},
	{"green",      "Green",      "basecolor_green"},
	{"magenta",    "Magenta",    "basecolor_magenta"},
	{"brown",      "Brown",      "unicolor_dark_orange"},
	{"pink",       "Pink",       "unicolor_light_red"},
	{"dark_grey",  "Dark Grey",  "unicolor_darkgrey"},
	{"dark_green", "Dark Green", "unicolor_dark_green"},
	--]]
}

for i = 1, #dyes do
	local name, desc, craft_color_group = unpack(dyes[i])

	local clothname="cloth:"..name
	local blockname=clothname.."_block"


	local tile="wool_" .. name .. ".png"
	minetest.register_node(blockname, {
		description = desc .. " Cloth",
		tiles = {tile},
		is_ground_content = false,
		sunlight_propagates = false,
		groups = {oddly_breakable_by_hand = 2, flammable = 3, cloth=1, fall_damage_add_percent=-25, bouncy=50},
		sounds = default.node_sound_defaults(),
		paramtype2="facedir",
		drop="stairs:stair_"..name.."_block 4",
	})

	stairs.register_stair(name.."_block", blockname, true, { tile }, name.." Stair", default.node_sound_defaults()) 
	stairs.register_slab(name.."_block", blockname, true, { tile }, name.." Slab", default.node_sound_defaults()) 

	if craft_color_group then
		minetest.register_craft{
			output = blockname,
			type="shapeless",
			recipe = { craft_color_group, "cloth:white_block" }
		}
	else
		-- it is the white cloth
		minetest.register_craft{
			output = blockname,
			recipe = {
				--{"farming:string", "farming:string", "farming:string"},
				{"farming:string", "farming:string", ""},
				{"farming:string", "farming:string", ""},}
			}
	end

	--[[
	minetest.register_craft{
		output = blockname,
		recipe = {
			{"", "", "",},
			{clothname, clothname, "",},
			{clothname, clothname, "",}}
		}
	--]]
	--[[
	minetest.register_craft{
		output = clothname.." 4",
		type="shapeless",
		recipe = { blockname }
		}
	]]
			
		--[[
	minetest.register_craft{
		output = "stairs:stair_"..name.."_block",
		recipe = {
			{"", "", clothname},
			{"", clothname, "",},
			{clothname, "", ""}}
		}
		--]]
end

