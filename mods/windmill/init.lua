

mfwm = {}

mfwm.register_windmill = function( nodename, descr, animation_png, animation_png_reverse, scale, inventory_image, animation_speed, craft_material, sel_radius )

    minetest.register_node( nodename, {
	description = descr,
	drawtype = "signlike", 
        visual_scale = scale,
	tiles = {
		{name=animation_png, animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=animation_speed}},
	},
	inventory_image = inventory_image.."^[transformFX",
	wield_image     = inventory_image.."^[transformFX",
	wield_scale = {x=1, y=1, z=1},
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	selection_box = {
		type = "wallmounted",
		wall_side   = {-0.4, -sel_radius, -sel_radius, -0.2, sel_radius, sel_radius},
	},
	groups = {oddly_breakable_by_hand=1,attached_node=1, protected=1},
	legacy_wallmounted = true,

	on_rightclick=function(pos, node, player, obj)
		if player==nil then return false end
		local name=player:get_player_name()
		if not mf.clan_ok(name, pos) then return false end
		local offer=obj:get_name()
		if obj:get_name()=="farming:string" then
			minetest.add_item(pos, "glider:glider")
			obj:take_item()
		end
		return false
	end,
	after_place_node=function(pos, placer)
		if(pos.y<60) then -- this is the highland limit in mapgen.
			minetest.swap_node(pos, {name=nodename})
			minetest.chat_send_player(placer:get_player_name(), minetest.colorize("orange", "Not enough wind; place above 60m"))
		end
	end

    })
    --[[
    minetest.register_node( nodename.."stopped", {
	description = descr,
	drawtype = "signlike", 
        visual_scale = scale,
	tiles = animation_png,
	inventory_image = inventory_image.."^[transformFX",
	wield_image     = inventory_image.."^[transformFX",
	wield_scale = {x=1, y=1, z=1},
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	selection_box = {
		type = "wallmounted",
		wall_side   = {-0.4, -sel_radius, -sel_radius, -0.2, sel_radius, sel_radius},
	},
	groups = {oddly_breakable_by_hand=1,attached_node=1},
	legacy_wallmounted = true,
	drop=nodename,

    })
    ]]

end

mfwm.register_windmill( "mfwm:windmill_sails",  "Windmill.\nPlace above 60m.\nRight-click with a string to get a single-use glider.",
			"windmill_wooden_cw_with_sails.png", "windmill_wooden_ccw_with_sails.png",
			6.0, "windmill_wooden_inv.png", 1.0, "wool:white", 3 );
minetest.register_craft{
        output = "mfwm:windmill_sails",
        recipe = {
                { "group:cloth", "group:stick",                    "group:cloth" },
                { "group:stick", "beacon:beacon",       "group:stick" },
                { "group:cloth", "group:stick",                    "group:cloth" },
        }
}


--[[
windmill.register_windmill( "windmill:windmill",       "Windmill rotors",
			"windmill.png", "windmill_reverse.png",
			6.0, "windmill_4blade_inv.png", 1.0, "default:steel_ingot", 2.9 );

windmill.register_windmill( "windmill:windmill_modern", "Windmill turbine",
			"windmill_3blade_cw.png", "windmill_3blade_ccw.png",
			6.0, "windmill_3blade_inv.png", 1.0, "homedecor:plastic_sheeting", 2.9 );

windmill.register_windmill( "windmill:windmill_idle",  "Windmill idle",
			"windmill_wooden_cw.png", "windmill_wooden_ccw.png",
			6.0, "windmill_wooden_no_sails_inv.png", 2.0, "default:wood", 3 );
-- this one is smaller than the other ones
mfwm.register_windmill( "mfwm:windmill_farm", "Windmill. Right-click with a mese crystal fragment to get a single-use glider\nTo use the glider: run, jump and deploy from a high place.\nMouse-controlled",
			"windmill_farm_cw.png", "windmill_farm_ccw.png",
			3.0, "windmill_farm_inv.png", 0.5, "default:steel_ingot", 1.5 );

minetest.register_node("windmill:axis", {
	description = "Axis for mounting windmills",
	drawtype = "nodebox",
	tiles = {"default_wood.png"},
	paramtype = "light",
	paramtype2 = "facedir",
	sunlight_propagates = true,
	groups = {choppy=2,dig_immediate=3},
	node_box = {
		type = "fixed",
		fixed = {{-0.25, -0.5, -0.25, 0.25, 0.4, 0.25},
			 {-0.1,-0.1,-0.5,0.1,0.1,0.5}},
	},
	selection_box = {
		type = "fixed",
		fixed = {{-0.25, -0.5, -0.25, 0.25, 0.4, 0.25},
			 {-0.1,-0.1,-0.5,0.1,0.1,0.5}},
	},
})


minetest.register_craft({
        output = "windmill:axis",
        recipe = {
                {"default:steel_ingot", "default:stick",       "default:steel_ingot" },
        }
})
]]

