-- This mod is based on Protector Redo from TenPlusOne.
-- 
-- Author: Astrobe
-- License: MIT

-- get minetest.conf settings
protector = {}
protector.mod = "redux"
protector.flip = minetest.settings:get_bool("protector_flip") or false

-- Intllib
local S
if minetest.get_modpath("intllib") then
	S = intllib.Getter()
else
	S = function(s, a, ...) a = {a, ...}
		return s:gsub("@(%d+)", function(n)
			return a[tonumber(n)]
		end)
	end

end
protector.intllib = S

--[[
minetest.register_node("peri:protection",
{
	description="Protection block",
	tiles={"xdecor_stone_rune.png^protector_logo.png"},
	groups={ cracky=3, protected=1},
	sounds=default.node_sound_metal_defaults(),
	paramtype="light",
})

minetest.register_craft
{
	type="shapeless",
	output="peri:protection",
	recipe={"default:skeleton_key", "dungeon_crates:crate"},
}
]]

protector.can_dig = function(r, pos, digger)

	if not digger or not pos then return false end

	-- quick check
	if not minetest.find_node_near(pos, r, {"group:protected"}, true) then return true end

	-- protector_bypass privileged users can override protection
	if minetest.check_player_privs(digger, {protection_bypass = true}) then
		return true
	end

	-- full check
	local pos = minetest.find_nodes_with_meta(
		{x = pos.x - r, y = pos.y - r, z = pos.z - r},
		{x = pos.x + r, y = pos.y + r, z = pos.z + r})


	local meta, owner 
	for n = 1, #pos do
		meta = minetest.get_meta(pos[n])
		owner = meta:get_string("owner") -- or ""
		-- if owner=="" then return true end
		-- node change and digger isn't owner
		if owner~="" and owner ~= digger then
			minetest.chat_send_player(digger, S("This area is owned by @1!", owner))
			return false
		end

	end
	return true
end


local old_is_protected = minetest.is_protected

-- check for protected area, return true if protected and digger isn't on list
function minetest.is_protected(pos, digger)
	digger = digger or "" -- nil check
	-- is area protected against digger?
	if not protector.can_dig(1, pos, digger) then
		local player = minetest.get_player_by_name(digger)
		if player and player:is_player() then
			-- flip player when protection violated
			if protector.flip then
				-- yaw + 180°
				local yaw = player:get_look_horizontal() + math.pi
				--local yaw = player:get_look_yaw() + math.pi

				if yaw > 2 * math.pi then
					yaw = yaw - 2 * math.pi
				end

				player:set_look_yaw(yaw)

				-- invert pitch
				player:set_look_vertical(-player:get_look_vertical())
				-- if digging below player, move up to avoid falling through hole
				local pla_pos = player:get_pos()

				if pos.y < pla_pos.y then

					player:setpos({
						x = pla_pos.x,
						y = pla_pos.y + 0.8,
						z = pla_pos.z
					})
				end
			end
		end

		return true
	end

	-- otherwise can dig or place
	return old_is_protected(pos, digger)
end

function protector.protect_node(pos, name)
	local meta = minetest.get_meta(pos)
	meta:set_string("owner", name)
	meta:set_string("infotext", "Owned by ".. name)
end

minetest.register_on_placenode(function(pos, newnode, placer, oldnode, itemstack, pointed_thing)
	if minetest.get_item_group(newnode.name, "protected")~=0 then
		local name=placer:get_player_name() or ""
		protector.protect_node(pos, name)
	end
end)
