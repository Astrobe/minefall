-- Author: Astrobe
-- licence (code): CC-BY-SA 4.0
-- Texture from Xdecor, WTFPL license
-- Version 1.0 

-- Derived from "Dungeon Loot" by BlockMen.

dungeon_crates={loot={"dungeon_crates:crate", "air"}}

minetest.register_node("dungeon_crates:crate", {
	description = "Loot box\nPlacing one grants 1RP.\nDigging one costs 1RP or a mese crystal fragment (punch with a fragment in hand).\nFound in dungeons and ruins.\nGet: from traders, for apple candy or sandwitch.",
	tiles = { "xdecor_stone_rune.png" },
	is_ground_content = false,
	groups = {oddly_breakable_by_hand=1, nopicker=1, noblast=1 },
	sounds = default.node_sound_defaults(),
	drop = {
		max_items = 1,
		items = {
			{items = {'3d_armor:mask_cat'}, rarity = 250},
			{items = {'3d_armor:devil'}, rarity = 250},
			{items = {'mob_cats:kitten_set'}, rarity = 200},
			{items = {'wands:confusion'}, rarity = 150},
			{items = {'wands:mese'}, rarity = 50},
			{items = {'compass:compass 33'}, rarity = 6},
			{items = {'craft_table:simple'}, rarity = 3},
			{items = {'default:bronze_ingot 2'}, rarity = 1},
		}
	},
	-- light_source = 2,
})


local function place_spawner(tab)
	local n= #tab
	if tab == nil then return end
	for i=1, n do
		local pos = tab[i]
		pos.y = pos.y - 1
		local below = minetest.get_node_or_nil(pos)
		if below and below.name ~= "air" then
			pos.y = pos.y + 1
			minetest.set_node(pos, {name = dungeon_crates.loot[math.random(1, #dungeon_crates.loot)]})
		end
	end
	local t=tab[math.random(1, #tab)]
	local here=minetest.get_node_or_nil(t)
	if here and here.name~="air" then
		minetest.set_node(t, {name="anchorstone:anchorstone"});	
	end
end

minetest.set_gen_notify("dungeon")
minetest.register_on_generated(function(minp, maxp, blockseed)
	local ntf = minetest.get_mapgen_object("gennotify")
	if ntf and ntf.dungeon then
		minetest.after(0.5, place_spawner, table.copy(ntf.dungeon))
	end
end)

