-- Melding mod
-- Author: Astrobe
-- Code licence: Same as Minetest.

minetest.register_node("melding:node",
{
	description = "Melding block",
	tiles = {
		{
			name = "nether_portal_alt.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.7,
			},
		},
	},
	drawtype = "glasslike",
	groups={nopicker=1},
	paramtype = "light",
	paramtype2 = "colorfacedir",
	palette = "nether_portals_palette.png",
	post_effect_color = { a = 160, r = 128, g = 0, b = 80 },
	sunlight_propagates = false,
	use_texture_alpha = "opaque",
	walkable = false,
	diggable = false,
	pointable = false,
	buildable_to = false,
	drowning=2,
	-- move_resistance=1,
	light_source=11,
	is_ground_content = false,
	drop = "",
})

-- Here is our good old friend, miss ABM.
minetest.register_abm{
	label="melding expension",
	nodenames="melding:node",
	neighbors= "air",
	interval= 1789,
	chance=4,
	catch_up=true,
	max_y=50, -- a little tree limit in mapgen
	action=function(pos)
		--  This works well for us because find_node-near looks at the node right above first
		--  and we prefer an expand-up first behaviour.
		local p=minetest.find_node_near(pos, 1, {name="air"})
		if not p then return end
		if not minetest.is_protected(p) then
			minetest.swap_node(p, {name="melding:node"})
		end
	end,
}

--[[
minetest.register_chatcommand("cm",
{
	description="Clears melding in a ten blocks radius.",
	privs={server=true},
	func=function(name)
		local pos=minetest.get_player_by_name(name):get_pos()
		local p=minetest.find_node_near(pos, 10, {name="melding:node"})
		while p  do
			minetest.remove_node(p)
			p=minetest.find_node_near(pos, 10, {name="melding:node"})
		end
	end
})
]]
