This is based on a now old version of Snowdrift with lots of modifications.

Author: Astrobe
Licenses: same as original (see below)

---------------

snowdrift 0.5.2 by paramat
For Minetest 0.4.15 and later
Depends default

Licenses:
Source code:
MIT by paramat
Media:
Textures CC BY-SA (3.0) by paramat
Sounds CC BY (3.0) by inchadney
http://freesound.org/people/inchadney/sounds/58835/ 
