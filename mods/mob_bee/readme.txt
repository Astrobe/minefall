Bee mob extracted from mobs_animals by TenPlusOne.

Several modifications by Astrobe for Minefall.
Two major changes is that it is a "butterfly" hive (see mob_butterfly) because
I didn't like the bees of the original mod and didn't want to introduce yet
another mob type just for this, and that the hive "consumes" the butterfly
(egg) to produce honey.

What? Something to say? This is a magic world. Butterflies turn back to larva
state when captured and the produce honey until the grow back to a butterfly.
No big deal.

Author: Astrobe
License: MIT (same as original - see license.txt).
Additional "caramel apple" texture from GreenDimond's Halloween mod, MIT
license.

