
local S = mobs.intllib

-- Bee by KrupnoPavel (.b3d model by sirrobzeroone)

minetest.register_craftitem("mob_bee:honey", {
	description="Honey",
	inventory_image = "mobs_honey_inv.png^[transformFX",
	on_use=minetest.item_eat(5),
	groups={heal=5}
})

-- Sorry for the misnomer, I changed my mind (again)
minetest.register_craftitem("mob_bee:honey_cake", {
	description="Caramel apple.\nCan be traded for a loot box.",
	inventory_image = "caramel_apple.png",
	on_use=minetest.item_eat(7),
	groups={heal=7}
})

minetest.register_craft {
	output="mob_bee:honey_cake",
	--type="shapeless",
	recipe={
		{"mob_bee:honey"},
		{"default:apple"}, 
		{"group:stick"}, 
	},
}

-- Sugar sprinkles are a convenient food made from renewable ingredients.

minetest.register_craftitem("mob_bee:sprinkles", {
	description="Candies (note to be removed next release)",
	inventory_image = "sugar_sprinkles.png",
	on_use=minetest.item_eat(3),
	groups={heal=3}
})

-- beehive (when placed spawns bee)
minetest.register_node(":mobs:beehive", {
	description = "Artificial Hive",
	tiles = {"xdecor_hive_top.png", "xdecor_hive_top.png",
		 "xdecor_hive_side.png", "xdecor_hive_side.png",
		 "xdecor_hive_side.png", "xdecor_hive_front.png"},
	paramtype = "light",
	groups = {oddly_breakable_by_hand = 2, flammable = 1, protected=1, nopicker=1},
	sounds = default.node_sound_defaults(),

	paramtype2="facedir",
	on_place=minetest.rotate_node,

	on_construct = function(pos)

		local meta = minetest.get_meta(pos)

		meta:set_string("formspec", "size[8,6]"
			..default.gui_bg..default.gui_bg_img..default.gui_slots
--			.. "image[3,0.8;0.8,0.8;mobs_bee_inv.png]"
			.. "list[context;beehive;4,0.5;1,1;]"
			.. "list[current_player;main;0,2.35;8,4;]"
			.. "listring[]")

		meta:get_inventory():set_size("beehive", 1)
	end,

	after_place_node = function(pos, placer, itemstack)
		local meta = minetest.get_meta(pos)
		if placer and placer:is_player() then
			minetest.set_node(pos, {name = "mobs:beehive", param2 = 1})
			meta:set_string("owner", placer:get_player_name() or "")
			meta:set_string("infotext", "Owned by ".. meta:get_string("owner"))
		end
	end,

	allow_metadata_inventory_put = function(pos, listname, index, stack, player)
		if not default.can_interact_with_node(player, pos) then return 0 end
		if listname == "beehive" and stack:get_name()~= "mobs_butterfly:butterfly_set" then
			return 0
		end
		return 1
	end,

	allow_metadata_inventory_take = function(pos, listname, index, stack, player)
		if not default.can_interact_with_node(player, pos) then
			return 0
		end
		return 2
	end,

	can_dig = function(pos,player)

		local meta = minetest.get_meta(pos)

		-- only dig beehive if no honey inside
		return meta:get_inventory():is_empty("beehive")
	end,

})

minetest.register_craft({
	output = "mobs:beehive",
	type="shapeless",
	recipe = {
		"default:chest_locked","mobs_butterfly:butterfly_set",
	}
})

-- beehive workings
minetest.register_abm({
	nodenames = {"mobs:beehive"},
	-- tuned to have around 50% chance of getting something after 1 hour, per hive
	interval = 907, -- a prime for bit more than 15 minutes
	chance = 7,
	catch_up = true,
	action = function(pos, node)

		--[[
		-- bees only make honey during the day
		local tod = (minetest.get_timeofday() or 0) * 24000

		if tod < 5500 or tod > 18500 then
			return
		end
		]]

		-- is hive full?
		local meta = minetest.get_meta(pos)
		if not meta then return end -- for older beehives
		local inv = meta:get_inventory()
		local item = inv:get_stack("beehive", 1)
		
		if item:get_name() ~= "mobs_butterfly:butterfly_set" then return end
		
		-- no flowers no honey, nuff said!
		local p= minetest.find_node_near(pos, 1, "group:flower", false)
		if p then
			minetest.remove_node(p)
			inv:remove_item("beehive", "mobs_butterfly:butterfly_set")
			inv:add_item("beehive", "mob_bee:honey 2")
		end
	end
})
