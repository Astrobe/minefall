-- Author: Astrobe
-- licence (code): CC-BY-SA 4.0
-- Armor textures from "Alternate-3D_armor-textures" by ExTex, CC-BY-SA 3.0.
-- Armor textures from "halloween" mod by GreenDimond, MIT

local function S(text) return text end

armor:register_armor_group("energy", 50)
armor:register_armor_group("regen", 3)

armor:register_armor(":3d_armor:helmet_bronze", {
	description = S("Bronze Helmet"),
	inventory_image = "3d_armor_inv_helmet_bronze.png",
	groups = {armor_head=1, armor_heal=20, armor_use=100, physics_speed=-0.01, physics_jump=-0.05, physics_liquid_sink=0.1},
	armor_groups = { regen=-1},
	-- damage_groups = {cracky=3, snappy=2, choppy=2, crumbly=1, level=1},
})
armor:register_armor(":3d_armor:chestplate_bronze", {
	description = S("Bronze Chestplate"),
	inventory_image = "3d_armor_inv_chestplate_bronze.png",
	groups = {armor_torso=1, armor_heal=20, armor_use=160, physics_speed=-0.01, physics_jump=-0.05, physics_liquid_sink=0.1},
	armor_groups = {energy=-20},
	-- damage_groups = {cracky=3, snappy=2, choppy=2, crumbly=1, level=1},
})
armor:register_armor(":3d_armor:leggings_bronze", {
	description = S("Bronze Leggings"),
	inventory_image = "3d_armor_inv_leggings_bronze.png",
	groups = {armor_legs=1, armor_heal=20, armor_use=140, physics_speed=-0.01, physics_jump=0.30, physics_liquid_sink=0.1},
	armor_groups = {},
	-- damage_groups = {cracky=3, snappy=2, choppy=2, crumbly=1, level=1},
})
armor:register_armor(":3d_armor:boots_bronze", {
	description = S("Bronze Boots"),
	inventory_image = "3d_armor_inv_boots_bronze.png",
	groups = {armor_feet=1, armor_heal=20, armor_use=80, physics_speed=0.06, physics_jump=-0.05, physics_liquid_sink=0.1},
	armor_groups = {},
	-- damage_groups = {cracky=3, snappy=2, choppy=2, crumbly=1, level=1},
})



armor:register_armor(":3d_armor:helmet_steel", {
	description = S("Steel Helmet"),
	inventory_image = "3d_armor_inv_helmet_steel.png",
	groups = {armor_head=1, armor_heal=0, armor_use=200, physics_speed=-0.02, physics_jump=-0.07, physics_liquid_sink=0.2},
	armor_groups = {regen=-2},
	-- damage_groups = {cracky=2, snappy=3, choppy=2, crumbly=1, level=1},
})
armor:register_armor(":3d_armor:chestplate_steel", {
	description = S("Steel Chestplate"),
	inventory_image = "3d_armor_inv_chestplate_steel.png",
	groups = {armor_torso=1, armor_heal=20, armor_use=320, physics_speed=-0.02, physics_jump=-0.07, physics_liquid_sink=0.2},
	armor_groups = {energy=-40},
	-- damage_groups = {cracky=2, snappy=3, choppy=2, crumbly=1, level=1},
})
armor:register_armor(":3d_armor:leggings_steel", {
	description = S("Steel Leggings"),
	inventory_image = "3d_armor_inv_leggings_steel.png",
	groups = {armor_legs=1, armor_heal=20, armor_use=280, physics_speed=-0.02, physics_jump=0.42, physics_liquid_sink=0.2},
	armor_groups = {},
	-- damage_groups = {cracky=2, snappy=3, choppy=2, crumbly=1, level=1},
})
armor:register_armor(":3d_armor:boots_steel", {
	description = S("Steel Boots"),
	inventory_image = "3d_armor_inv_boots_steel.png",
	groups = {armor_feet=1, armor_heal=20, armor_use=160, physics_speed=0.12, physics_jump=-0.07, physics_liquid_sink=0.2},
	armor_groups = {},
	-- damage_groups = {cracky=2, snappy=3, choppy=2, crumbly=1, level=1},
})

armor:register_armor(":3d_armor:helmet_gold", {
	description = S("Gold Helmet"),
	inventory_image = "3d_armor_inv_helmet_gold.png",
	groups = {armor_head=1, armor_heal=20, armor_use=400, physics_speed=-0.03, physics_jump=-0.09, physics_liquid_sink=0.3},
	armor_groups = {regen=-3},
	-- damage_groups = {cracky=1, snappy=2, choppy=2, crumbly=3, level=1},
})
armor:register_armor(":3d_armor:chestplate_gold", {
	description = S("Gold Chestplate"),
	inventory_image = "3d_armor_inv_chestplate_gold.png",
	groups = {armor_torso=1, armor_heal=20, armor_use=640, physics_speed=-0.03, physics_jump=-0.09, physics_liquid_sink=0.3},
	armor_groups = {energy=-60},
	-- damage_groups = {cracky=1, snappy=2, choppy=2, crumbly=3, level=1},
})
armor:register_armor(":3d_armor:leggings_gold", {
	description = S("Gold Leggings"),
	inventory_image = "3d_armor_inv_leggings_gold.png",
	groups = {armor_legs=1, armor_heal=20, armor_use=560, physics_speed=-0.03, physics_jump=0.54, physics_liquid_sink=0.3},
	armor_groups = {},
	-- damage_groups = {cracky=1, snappy=2, choppy=2, crumbly=3, level=1},
})
armor:register_armor(":3d_armor:boots_gold", {
	description = S("Gold Boots"),
	inventory_image = "3d_armor_inv_boots_gold.png",
	groups = {armor_feet=1, armor_heal=20, armor_use=320, physics_speed=0.18, physics_jump=-0.09, physics_liquid_sink=0.3},
	armor_groups = {},
	-- damage_groups = {cracky=1, snappy=2, choppy=2, crumbly=3, level=1},
})

-- Special craft.
armor:register_armor(":3d_armor:helmet_greater", {
	description = S("Greater Helmet"),
	inventory_image = "3d_armor_inv_helmet_greater.png",
	groups = {armor_head=1, armor_heal=20, armor_use=100, physics_speed=-0.04, physics_jump=-0.11, physics_liquid_sink=0.4},
	armor_groups = {regen=-4},
	-- damage_groups = {cracky=2, snappy=1, level=1},
})
minetest.register_craft{ type="shapeless", output = "3d_armor:helmet_greater", recipe = { "3d_armor:helmet_steel", "more_mese:mese_crystal_red"}}

armor:register_armor(":3d_armor:chestplate_greater", {
	description = S("Greater Chestplate"),
	inventory_image = "3d_armor_inv_chestplate_greater.png",
	groups = {armor_torso=1, armor_heal=20, armor_use=100, physics_speed=-0.04, physics_jump=-0.11, physics_liquid_sink=0.4},
	armor_groups = {energy=-80},
	-- damage_groups = {cracky=2, snappy=1, level=1},
})
minetest.register_craft{ type="shapeless", output = "3d_armor:chestplate_greater", recipe = { "3d_armor:chestplate_steel", "more_mese:mese_crystal_red"}}

armor:register_armor(":3d_armor:leggings_greater", {
	description = S("Greater Leggings"),
	inventory_image = "3d_armor_inv_leggings_greater.png",
	groups = {armor_legs=1, armor_heal=20, armor_use=100, physics_speed=-0.04, physics_jump=0.66, physics_liquid_sink=0.4},
	armor_groups = {},
	-- damage_groups = {cracky=2, snappy=1, level=1},
})
minetest.register_craft{ type="shapeless", output = "3d_armor:leggings_greater", recipe = { "3d_armor:leggings_steel", "more_mese:mese_crystal_red"}}

armor:register_armor(":3d_armor:boots_greater", {
	description = S("Greater Boots"),
	inventory_image = "3d_armor_inv_boots_greater.png",
	groups = {armor_feet=1, armor_heal=20, armor_use=100, physics_speed=0.24, physics_jump=-0.11, physics_liquid_sink=0.4},
	armor_groups = {},
	-- damage_groups = {cracky=2, snappy=1, level=1},
})
minetest.register_craft{ type="shapeless", output = "3d_armor:boots_greater", recipe = { "3d_armor:boots_steel", "more_mese:mese_crystal_red"}}

-- Not craftable, lootbox rare items.
armor:register_armor(":3d_armor:mask_cat", {
	description = S("Cat mask"),
	inventory_image = "inv_mask_cat.png",
	groups = {armor_head=1, armor_heal=0, armor_use=1024, physics_speed=0, physics_jump=0, not_repaired_by_anvil=1},
	armor_groups = {regen=-4},
})

armor:register_armor(":3d_armor:devil", {
	description = S("Devil suit"),
	inventory_image = "inv_devil.png",
	groups = {armor_torso=1, armor_heal=0, armor_use=1024, physics_speed=0, physics_jump=0, not_repaired_by_anvil=1},
	armor_groups = {energy=-80},
})

local materials = {
	bronze = "default:bronze_ingot",
	steel = "default:steel_ingot",
	gold = "default:gold_ingot",
}

for k, v in pairs(materials) do
	minetest.register_craft({
		output = "3d_armor:helmet_"..k,
		recipe = {
			{v, v, v},
			{v, "", v},
			{"", "", ""},
		},
	})
	minetest.register_craft({
		output = "3d_armor:chestplate_"..k,
		recipe = {
			{v, "", v},
			{v, v, v},
			{v, v, v},
		},
	})
	minetest.register_craft({
		output = "3d_armor:leggings_"..k,
		recipe = {
			{v, v, v},
			{v, "", v},
			{v, "", v},
		},
	})
	minetest.register_craft({
		output = "3d_armor:boots_"..k,
		recipe = {
			{v, "", v},
			{v, "", v},
		},
	})
	--[[
	minetest.register_craft({
		output = "shields:shield_"..k,
		recipe = {
			{v, v, v},
			{v, v, v},
			{"", v, ""},
		},
	})
	]]
end


armor:register_on_update(function(player)
	local name=player:get_player_name()
	local mfp=mfplayers[name]
	mfp.max_energy=(player:get_armor_groups().energy or 50)/10
	if bones.get_mode_for(player)=="drop" then
		mfp.max_energy=mfp.max_energy+rp.get_player_count(name)/100
	end
	mfp.regen=(player:get_armor_groups().regen or 3)/10
	mfp.energy=math.min(mfp.energy, mfp.max_energy)
	hb.change_hudbar(player, "energy", mfplayers[name].energy, mfplayers[name].max_energy)
end)

