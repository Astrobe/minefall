-- Author: Astrobe
-- licence: CC-BY-SA 4.0
-- Version 1.0 
--


yatpm=function(pos, node, digger)
		if digger and minetest.get_item_group(minetest.get_node({x=pos.x, y=pos.y+1, z=pos.z}).name, "yatpm")~=0 then
			minetest.chat_send_player(digger:get_player_name(),"Columnar: dig from top block.")
			return nil
		else
			minetest.node_dig(pos, node,digger)
		end
	end

for name,def in pairs(minetest.registered_nodes) do
	if def.groups.yatpm then
		-- minetest.log("yatpm:"..name)
		minetest.override_item(name, { on_dig=yatpm})
	end
end

