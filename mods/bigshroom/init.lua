minetest.register_node("bigshroom:dry",
{
	description="Dry Bigshroom",
	tiles={"bigshroom_body.png"},
	groups = {choppy = 3, flammable=3, tree=1},
	sounds = default.node_sound_wood_defaults(),
	light_source=2,
})

minetest.register_node("bigshroom:body",
{
	description="Bigshroom body",
	tiles={"bigshroom_body.png"},
	groups = {choppy = 3, flammable=3, fall_damage_add_percent=-10, bouncy=5},
	sounds = default.node_sound_wood_defaults(),
	light_source=3,
	drop="bigshroom:dry",
})

minetest.register_node("bigshroom:top",
{
	description="Bigshroom top",
	tiles={"bigshroom_top.png"},
	groups = {choppy = 3, flammable=3, fall_damage_add_percent=-10, bouncy=5},
	sounds = default.node_sound_wood_defaults(),
	light_source=6,
	drop="bigshroom:dry",
})

minetest.register_craft{
	type = "fuel",
	recipe = "bigshroom:dry",
	burntime = 10, }


B={name="bigshroom:body"}
F={name="bigshroom:body", force_place=true}
T={name="bigshroom:top"}
_={name="air"}

local shroom=minetest.register_schematic{
	size={x=5, y=8, z=5},
	yslice_prob={
		{ypos=1, prob=192},
		{ypos=2, prob=128},
		{ypos=3, prob=64},
		{ypos=4, prob=32},
	},
	data={
		_, _, _, _, _,
		_, _, _, _, _,
		_, _, _, _, _,
		_, _, _, _, _,
		_, _, _, _, _,
		_, _, _, _, _,
		_, T, T, T, _,
		_, _, _, _, _,
		
		_, _, _, _, _,
		_, _, _, _, _,
		_, _, T, _, _,
		_, _, _, _, _,
		_, _, _, _, _,
		_, _, _, _, _,
		T, T, T, T, T,
		_, T, T, T, _,
		
		_, _, F, _, _,
		_, _, B, _, _,
		_, _, B, _, _,
		_, _, B, B, _,
		T, B, B, _, _,
		_, _, B, _, _,
		T, T, B, T, T,
		_, T, T, T, _,

		_, _, _, _, _,
		_, T, B, _, _,
		_, _, _, _, _,
		_, _, _, T, _,
		_, _, _, _, _,
		_, _, _, _, _,
		T, T, T, T, T,
		_, T, _, T, _,
		
		_, _, _, _, _,
		_, _, _, _, _,
		_, _, _, _, _,
		_, _, _, _, _,
		_, _, _, _, _,
		_, _, _, _, _,
		_, T, T, T, _,
		_, _, _, _, _,
		
	}
}

minetest.register_node("bigshroom:sapling", {
	description = "Mutated mushroom.\nGrowns on dirt, in the dark.",
	tiles = {"pilzmod_mushroom_sappling_brown.png"},
	inventory_image = "pilzmod_mushroom_sappling_brown.png",
	wield_image = "pilzmod_mushroom_sappling_brown.png",
	drawtype = "plantlike",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	groups = {snappy = 3, attached_node = 1, flammable = 1 },
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-3 / 16, -0.5, -3 / 16, 3 / 16, -2 / 16, 3 / 16},
	},
	floodable=true,

	on_construct = function(pos)
		default.start_grow_timer(4000, pos, "underground")
	end,

	on_timer= function(pos)
		if minetest.get_node_light(pos)>3 then return end -- won't grow
		local below=vector.new(pos.x, pos.y-1, pos.z)
		if minetest.get_item_group(minetest.get_node(below).name, "soil")==0 then
			return
		end
		minetest.place_schematic(vector.new(pos.x - 2, pos.y, pos.z - 2), shroom, "random", nil, false)
		minetest.set_node(below, {name="default:sand"})
	end

})

minetest.register_craft {
	type="shapeless",
	output="bigshroom:sapling",
	recipe={"flowers:mushroom_brown", "flowers:mushroom_red"}
}
