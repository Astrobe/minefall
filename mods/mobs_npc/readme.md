This mod is based on TenPlusOne's "mob_npc" mod.

Author: Astrobe
License (code): MIT
Additional textures:
Classic sandwich texture taken from th Sandwiches! mod by Annalysa, CC-BY-SA 3.0 UNPORTED.
mobs_trader_female.png and mobs_trader_female2.png based on "Emma" by Nelly, CC BY-SA 3.0, edited by Astrobe (same license).

