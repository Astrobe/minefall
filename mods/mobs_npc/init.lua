
local S = mobs.intllib

-- Sandwich texture extracted from Sandwiches! mod.
minetest.register_craftitem("mf_npc:sandwich", {
	description="Sandwich.\nCan be traded for a loot box.",
	inventory_image="classic_sandwich.png",
	on_use=minetest.item_eat(10),
	groups={heal=10}
})

minetest.register_craft {
	type = "shapeless",
	output = "mf_npc:sandwich",
	recipe = { "farming:bread", "mobs_fish:fried_fish"}
}

-- Npc by TenPlus1

local textures={{"mobs_npc1.png"}, {"mobs_npc2.png"}, {"mobs_npc3.png"}, {"mobs_npc4.png"}, {"mobs_npc5.png"}, {"mobs_npc6.png"}, {"mobs_npc7.png"}, {"mobs_npc8.png"}}
local animation={
		speed_normal = 15,
		speed_run = 15,
		stand_start = 0,
		stand_end = 79,
		walk_start = 168,
		walk_end = 187,
		run_start = 168,
		run_end = 187,
		punch_start = 200,
		punch_end = 219,
	}
local colbox={-0.35,0,-0.35, 0.35,1.8,0.35}

local trades= {
	["default:mese"]={'beacon:beacon'},
	["mob_bee:honey_cake"]={'dungeon_crates:crate'},
	["mf_npc:sandwich"]={'dungeon_crates:crate'},
	["default:obsidian"]={"default:obsidian_shard"},
	["default:mese_crystal"]={"mf_airtank:airtank"},
	["more_mese:mese_crystal_fragment_orange"]= {"mob_whale:whale_set"},
	--["more_mese:mese_crystal_red"]= {'3d_armor:helmet_greater', '3d_armor:chestplate_greater', '3d_armor:leggings_greater', '3d_armor:boots_greater'},
	-- ["flowers:mushroom_red"]={"default:bush_sapling", "default:acacia_bush_sapling", "default:blueberry_bush_sapling", "default:pine_bush_sapling"},
}

local dialogs= {
"Thanks for your hospitality!",
"It was about time...",
"Why did I come here again? Oh, yeah. to trade garba... I mean stuff!",
"Thanks for your business.",
"Don't ask me where I got this...",
"I hope it will be helpful to you.",
"Don't hurt yourself with that.",
"And that was the last one. I can finally go home.",
"Sry gtg wife calling.",
"Have we met before?",
"I'll come back for sure!",
"Maybe one day, I'll be an honest person...",
"You really live here?",
"I'll recommand your shop!",
"Wow, you scared me.",
"I've heard a weird noise.",
"I love this place!",
"Find me more of this!",
"No offense, but I've been wondering for a while... Are you human?",
"See you!",
"I like you, you know? I kept this one just for you!",
"My friend told me not to go to this dive anymore, but I can't help myself.",
"I am not here to play games.",
"Deal!",
"Good luck!",
"Have a nice day.",
"Fashion is a concept yet to be invented here.",
"Wait! I'll get some more! Just wait here!",
"No, sorry, I cannot take you with me.",
"Sure!",
"Have you seen my cat?",
"Our legends say that our ancestors had to flee their world. It could be this world.",
"Our researchers think the destruction and the contamination of this world by Mese was caused by a massive meteor shower.",
"Yes, we abduct the bugs on this world. To extract Mese, and for research.",
"My home world is safe, but dull. People call me a reckless adventurer when they learn I'm doing business here.",
"You call it a moon... But you actually are on the moon.",
"You have toilets, you have paper, but you don't have toilet paper!?",
"Do you think it's safe for us to go outside?",
"Maybe I'll get super-powers too if I start living here.",
"Your trading skill has increased.",
}

local function pick(list) return list[math.random(1, #list)] end

mobs:register_mob("mf_npc:npc", {
	type = "npc",
	passive = true,
	runaway=true,
	damage = 1,
	attack_type = "dogfight",
	attacks_monsters = false,
	owner_loyal = true,
	hp_min = 5,
	hp_max = 5,
	collisionbox = colbox,
	visual = "mesh",
	mesh = "character.b3d",
	drawtype = "front",
	textures = {
		{"mobs_trader.png"},
		{"mobs_trader2.png"},
		{"mobs_trader3.png"},
		{"mobs_trader_female.png"},
		{"mobs_trader_female2.png"},
	},
	makes_footstep_sound = true,
	sounds = {},
	--walk_chance=1,
	walk_velocity = 1,
	run_velocity = 1,
	jump = true,
	jump_height=0.01,
	stepheight=0.6,
	
	view_range = 10,
	owner = "",
	order = "follow",
	--fear_height = 3,
	animation = animation,
	show_on_minimap=true,
	infotext="Trader",
	env_dmg={["melding:node"]=-1},

	on_rightclick = function(self, clicker)
		local name = clicker:get_player_name()
		local item = clicker:get_wielded_item()
		if not item then return end
		local trade=trades[item:get_name()]
		local pos=self.object:get_pos()
		pos.y=pos.y+1
		if item:get_name()=="more_mese:mese_crystal_fragment_grey" then
			local pw=compass.get_wp2(name)
			if not pw then
				minetest.chat_send_player(name, minetest.colorize("orange", "Your waypoint is not set"))
				return 
			end
			pw.y=pw.y+1
			clicker:set_pos(pw)
			minetest.sound_play("nether_portal_teleport", {pos=pw, max_hear_distance=16}, true)
			item:take_item()
			clicker:set_wielded_item(item)
		elseif trade then
			minetest.add_item(pos, pick(trade))
			item:take_item()
			clicker:set_wielded_item(item)
			mobs.tp_fx(pos)
			self.object:remove()
			minetest.chat_send_player(name, minetest.colorize("cyan", pick(dialogs)))
		else
			minetest.chat_send_player(name, minetest.colorize("cyan", "Not interested"))
			mobs.turn_away_from(self, clicker:get_pos())
		end
	end,
})

mobs:spawn({
	name = "mf_npc:npc",
	nodes = {"group:cloth"},
	neighbors={"group:furniture"},
	interval= 29,
	chance = 400,
	min_height=-999,
	max_height=999,
	min_light=8,
	max_light=14,
})

