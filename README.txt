MINEFALL
========

Minefall is a survival game. Its signature feature is the ability
for players to "fly" if they have enough mana. Actually, the feature is called
"levitation" and is similar to "jetpacks" that one can find in other games.
Another distinct feature is the use of ranged weapons (wands) that have various
interesting effects.

The game has no particular goal, though a prerequisite is to survive because
most mobs are hostile and dangerous, especially in numbers.

Although the game has been designed with multiplayer in mind, it is fairly playable
alone. Some things are a lot easier, but on the other hand the player cannot
rely on trade. It can be advantageous to play multiple characters, each
specialized in a particular task and/or location.

Below is a description of the many aspects of the game. I chose to present it as
a lexicon or dictionary. Of course, it is full of SPOILERS.

Entries are surrounded by '+' (e.g. +LICENSE+) to ease search, references use
a single '+' (e.g. +weather). Entries are always in singular form and allcaps
while references may sometimes be in plural form.

Please note that as this file contains a lot of info and as I tune things a
lot, some info may be inaccurate or no longer true.

Last update: "A Light Unseen" release

+ABERRANT+ (tree)

Now called "Melding tree" in-game.

Aberrant trees are giant trees made of two or three random materials. Normal
saplings that grow next to melding become aberrant tree saplings.

They are intended to be both "city trees", as each tree is unique, and lotery
tickets, because the materials can be either common or uncommon. They can
bring camality too, as some materials are the spawning anchor of certain mobs.

+ABYSS+

The abyss is the layer below the basalt/lava layer.
It is intended to provide a reason for investing in mining and exploration:

- new building materials. There's the relatively cheap "abyssite cobble', with
  its unique color. Abyssite and glowstone can be obtained by using the
  obsidian shard.
- A new mob to fight and a new reward in the form of red mese, that can be
  used to in two ways:
- The blast wand, that causes huge explosions. It can be used mainly for
  mining or to remove +melding. It is only worth using against mobs if several
  can be hit by one shot.
- Greater armor pieces. This is the best armor in the game, a couple of rare
  drops from loot crates.
- Abyss dungeon bricks, which can mimic any wall block (stone brick/stone
  block and variations).
- Ruins with lots of clear glass.

The spider that lives, which drops red mese, is a fearsome enemy: it shoots
missiles that spawn spiderlings like the big orange spider, and those
spiderlings shoot teleportation missiles like the rock crab.

+ADMINISTRATION+

Contrary to some visitors I had on my test server, I don't want to be a moderator
or an "admin", I want to play this game. So I tried to minimize these tasks
and even make them part of the game. To the point that active players can
invite or revoke the invitation of other players (see +RP).

+ADVANCED+ (player)

See +veteran (player)

+ANCHORSTONE+

"Anchorstone" is the name of the mod that provided the base feature. In-game,
they are usually refered to as "dungeon beacons".
As the name suggests, they are a type of beacon found in dungeons.

They can picked up by players and placed somewhere else, then be used as
teleporters to their original location. They can also pick them up without
activating them at the cost of an red mese crystal; in which case they can
effectly choose where it teleports to.
Anchorstones therefore allow player to build teleportation networks, making
dungeons more attractive places.

Another specific feature is that players can set their second waypoint to the
orginal place, by simply feeding a compass to the beacon. Dungeons often come
in clusters, providing multiple beacons that allow to teleport to the same
region. With this feature, some of the beacons can be used for navigation.

Due to the +clan "twist", it's actually two teleportations networks that are
created: Men's and Women's.

+ANVIL+

The anvil is in the game primarily in order to add something more to do during
nights. It is sort of a mini game where players have to optimize their
throughput. It can be accelerated using armor (mana regen helmets) or food
(using +overeat).
On the paper it is also a way to "produce" metal: repairing a piece of armor
wears out the hammer, which means that one has spent some metal (to make a
hammer). Given the cost of the hammer, some repairs are totally not worth it
(anything bronze, for instance).

+AUTOMATION+

I did not wish to include automation mods (Mesecon, hoppers, etc.) because +MF
is mainly an action game, and it is players that should act, not their machines.
Furthermore it is an incentive to employ other players to do the boring tasks,
hence stimulating trade and interactions.
I am also not into "programming games", as programming is my day job. I love
programming, but it is off-topic in a game for me.


+BEACON+

Beacons can be obtained by trading with NPCs.
Players can set their "home" by feeding a green mese crystal to a beacon, and
they can teleport to it by feeding it a grey mese shard (yellow for +travelers).
Beacons are subject to the Men/Women +clan divide: players can only use a
beacon set by a player of the same clan, although the home of all new players
will be the first beacon placed by Singleplayer (or the server admin). If a
player changes their home, they won't be able to set it back to this beacon,
though.
If a player removes a beacon, homes associated to it remain valid.
If a player dies with 100 m of their beacon, the penalities associated to it
(RP loss, bones) do not apply.

See also +anchorstone.

+BEGINNER+ (player)

In +MF, this is the level just above +guest; it is kind of a guest with
interaction.

From the game design perspective, beginners are a problem because they don't
have much and therefore cannot do much. This leads to a "slow start" when we
would rather want a "fast start" to make the game engaging - commercial games
and other F2P games keep a close look on the "first hour experience".

The problem is different in single play or online play. For online play, I
consider it is up to players to help newcomers. They have already invested a
bunch of RPs to invite them, it wouldn't make sense to let them down.
For offline play, starting is a bit easier as the player should start in a
region with plenty of resources (especially if they use the recommended seed).

+BELIEVABLE+

... Is a word I use instead of "realistic". Sometimes players insist that things
should be realistic, but in my opinion it is a) expensive b) not always fun
and c) how a game made with cubes could possibly be realistic anyway?
The "not fun" is the most important point. Being able to carry 500 logs is not
realistic, but restricting players to one or two logs, or simulating weight,
is a deal-breaker.

+BIOMES+

+MF's biomes are mostly MTG biomes with lots of edits and a few additions.

All forests are significantly less dense than in +MTG and there are a lot
more logs lying on the ground. One reason is that forests naturally grow by
the action of the bugs that turn plants into saplings (often in two steps:
plant -> bush sapling -> tree sapling). The other reason is to provide an
affordance to build a story on "something bad happened here".

Above 60 m no tree or plant is placed. Ground is covered with gravel or snow in
colder biomes. In deserts, one sees bare rock without a sand cover, a nice
place to meet orange spiders under the sun...

Dungeons have been customized in each biomes, some are the unique source of
certain blocks.

+BOATS+

Minefall's boats are MTG's boats except that players can move forward only,
and by pressing the jump key. This consumes mana, so one can say that the
boats are mana-powered. This change was made to keep the player active.

+BOOKSHELVES+

Why so many bookshelves? Three reasons:
- it is only a matter of puting one overlay texture on the different types of
  planks, so it is relatively cheap, assets-wise.
- Add some diversity to furniture, adapt to the various interior decoration
  styles (a pine wood shelf on top of a pine wood floor is a bit sad).
- They can be hidden in floors, thus providing protection and the furniture
  block that activates (wrt +traders) cloth blocks in a stealth way. It helps
  with making protected beds, chairs or tables.

Shelve are not book-only storages like in +MT. In +FF it would not make much
sense. At some point they acted as locked chests; but that looked a bit weird,
and overlapping. Eventually they functionally took the place of the crafting
sign.

+BUTTERFLY+

Butterflies bring life in the world by emitting light sometimes, but they are
not just decorative mobs. They spawn at night only.
Their main role is to transform flowers to saplings. Each flower gives
a different kind of sapling.
Their secondary function is to serve as "fuel" for the +hives.

+CACTUS+

Cactus are IRL sort of like "water tanks", that's why they usually need spikes
to protect themselves from thirsty animals (when not, they can be toxic). However
when dried, they do provide wood-like material.
So in MF, I made it an abundant but a bit expensive (chopping+drying) source
of sticks. Mantis transform dry bushes into cactus saplings.
Unlike the MTG cliché, one can grow them outside of deserts and on plain soil.
They can also be used to grow mushrooms like wood logs.

+CAT+

Cats are an item of loot crates.
They don't jump because it often leads to dumb-looking behaviors.
Purely decorative. They don't do shit otherwise, like IRL.

+CANCELLATION+ (damage)

Damage cancellation (DC) is the fact that incoming damage is reduced to 1 HP;
however, all equipped armor pieces are damaged (by a fixed amount).

A 1 HP hit does not damage the armor.

All armor pieces (except the very rare drops from lootboxes) provide 20%
cancellation chance (so a full armor set gives 80% DC chance).

The reason for DC instead of the usual incoming damage reduction,
is that in +MT players can only have 20 HP maximum (this was made modifiable well
after I started +MF), and is a discreet value: you cannot have 12.5 HP, which is what
would happen if 3 points of damage was reduced by 50% by armor.

So the traditional damage reduction doesn't work well in +MT. If a player has 51%
damage reduction and a rat bites them for 1 damage, nothing happens, because 
the damage after the reduction is below 1, and MT rounds it to zero.
It might make sense that if you wear armor, you laugh at rat bites, but in
other cases this is more problematic. It leads to damage inflation at the
disadvantage of newcomers.

Furthermore, I did not want for the lower tier of the bestiary to become
irrelevant once the player gets some equipment. I don't have enough resources
to "waste" mobs.

With damage cancellation, small wasps can still deal just 1 point of damage,
they can still be beaten by new players with bare hands, and players with more
stuff still have to watch out for them.

+CLAN+

Clans introduce mild competition among players, the subject of this
competition being +anchorstones and indirectly, locations.
A clan member can only use facilities placed by other members of their clan.
"Facilities" include +anchorstones, +beacons, +anvils... (non exhaustive list)

I chose the man/woman criteria to create two clans because it doesn't require
additional information storage (it is computed from the name like the
players', which also determine their appearence) and is easy to check for players.

Players therefore cannot choose their clan, unless they create a new
character with the "right" name (the only purpose of the /skin command is to
tell them how). They can then play both clans with different characters, which
should lead them to different places thanks to the different teleportation
networks.

A social science experiment showed that group preferences can appear even if how
the groups were made is arbitrary, and even if belonging to a group doesn't give
any significant advantage. They also persist when groups are merged.
Therefore despite the rigidity and the simplicity of our "clans", I expect it
to create interesting dynamics.

+CLOTH+

Cloth are relatively expensive, as gathering the material to make strings is
either slow or difficult.
They have two unique features: vivid colors found nowhere else in the game (for
a full block), and the ability to make +slabs of cloth.
Their main function is to attract +NPCs.

+COAL+

Coal has two purposes:
- an explosive
- a fuel

The stone-with-coal block can catch fire (most likely as a result of the "light"
+focus). It then can explode at any time.
The intent of this feature is to help with mining, although it also introduces
some dangers for the miner (e.g. if they set on fire cobwebs and don't see
there's coal there). Especially in combination with lava, as basalt features
grey stone "blobs" where the coal ore can be generated.
As blasts make coal explode, chained explosions are not rare and can instantly
kill players.

The craftable coal block has been removed, due to the lack of alternative
uses.

+CRAFTING+ (table)

I never played Minecraft (or its +MT clones), so I only discovered that feature
when someone made a crafting table mod.

However, before that I've always found that being able to craft anything on
the spot was a little bit too easy, and for what it's worth, a bit not +believable.
Still, players can carry a crafting table (and a furnace) with them. However,
it takes one slot in their inventory. So this feature encourages a bit players
to build outposts where they can craft safely.

The crafting table had to come from loot crates in order to solve the
chicken-and-egg problem of "you need a crafting table to craft a crafting
table", which comes from the fact that I didn't want MC's 2x2 crafting grid.
But then, the crafting table had to be a common drop from lootboxes; also,
even if not common, players would eventually get more than they need. In order
to solve that issue, I made the crafting table a recipe component for useful
things, like chests and stores (makes sense), furnaces and trap/doors. The
latter ones are a bit far-fetched (although the furnace is somewhat like a
crafting device).

It often takes multiple steps to craft something in MF, sometimes maybe one
could say that it is a lot of clicks for relatively simple things. Two reasons
for that:
- it feels like you are actually crafting.
- some blocks are upgradable, eg from simple trapdoor to steel trapdoor.
  Everything has a cost in +MF, so letting players "recycle" their blocks
  helps with progression.


+DIAMOND+

Were eventually removed from +MF. Too little uses. Any mention here is a
leftover; features using diamonds have either been removed or changed.

+DISCONNECT+

A player that gets accidently disconnected could be in serious trouble when
they reconnect. If they were levitating at high altitude (mostly advanced
players), performing a controlled fall (see +levitation) or riding a +whale,
upon reconnection they have no mana or the whale may have wandered off.
Players start with an empty mana pool in order to prevent disconnection
abuses, and mainly because player disconnections/reconnection puts an
additional load on the server.

To prevent those accidental deaths, upon reconnection the first hit is always
cancelled (see +cancellation), even if the player doesn't wear armor.

Obviously this could also be abused, but I felt that the game cannot be both
unforgiving and unfair.

+DOOR+

Why no doors? Because they don't work well with our +protection system.
Visually they look like something that takes two blocks but it really is just
one block. And that leads to various issues even outside of our bizarre
protection scheme.
However trapdoors have a feature where when players stack them, they behave so
that one can create the equivalent of a door: opening/closing a trapdoor
switches the state of the trapdoor below it. Which in turn can switch the one
below itself, so players can create 3+ blocks door. It also allow "double
panel" doors: one can reclose the lower part and leave the top open. A type
of door that used to be found in farms, to prevent animals from going in or
out while cooling the building/house.

+DROP+

The main item dropped by mobs is +mese. The quantities are random, so they may
drop nothing at all, but on average they drop certain quantities.
Those quantities are set with regard mainly to how much HP they have. Bigger
mobs drop more, but they also drop proportionally more than smaller mobs.
When a mob drops something unique to it (like seeds), yellow mese drops
are usually less likely.

The intend to have player go after bigger mobs because they are more
profitable.

+DUNGEON+

Dungeons have three purposes in MF:
- create a teleportation network thanks to dungeon beacons
- help with mining. Some dungeons have breakable walls, so they are like caves 
- provide unique building blocks, such as mossy cobble or basalt bricks.

+ECONOMY+

I am no economist, I just have very basic notions of supply and demand,
sources and sinks, price and rarity.

I kept an eye on the economy in the game mainly to make trade interesting,
which in turn hopefully makes various +roles or specializations viable.
perhaps the strongest currency in the game is +RP. Because it determines the
ability to play and extends it ( /recall command, +talents ). But it cannot be
traded with shops.

Players should be encouraged to use mese as a currency, because fragments can
be assembled into crystals that can be grouped into blocks. The fact that Mese
can be "freely produced" by killing mobs is partly balanced by the fact that
it is needed in some crafts and traded with NPCs, so inflation should be
limited (besides, hunting mobs isn't actually free).

+ECOSYSTEM+

The fauna is mainly bugs. Partly because +Firefall had iconic spider-like
hostile mobs ("Arahnas"), partly because I didn't want players to kill
representations of actual animals (wolves,...) or humans/humanoids (we have
enough of that shit IRL, don't you think?).
Yet I feel a bit guilty of presenting +spiders and +wasps as "bad bugs".
However, players might have interest to leave them alone (because some of them
create saplings).

Fruits will regrow or not, randomly. This somewhat reproduces our evolution
from hunters/gatherers to farmers, as the "low hanging fruits" will eventually
be depleted by players.

Growth is generally slower that in MTG, partly because the duration of the day
is much longer, partly to make things a bit more +believable (no tree fully grows
in one day)

+FARLANDS+

Farlands is an MT game which is the second source of inspiration after
+Firefall, and also the source of many assets.
The great idea of having players build things to attract NPCs, and how to
implement it, comes directly from this game.
(note: there are some efforts to reboot Farlands - it was an abandonned game.
I refer here to the first version).

+FARMING+

Farming is the basis of the economy.

Food production is important because it is the only way for players to heal
themselves, and the natural sources of food (apple trees, blueberry bushes)
will eventually be depleted because they are not easy to renew.

Farming probably is the best activity for players who don't like
fighting mobs. As it involves some waiting for things to grow, it also fits
better the "socializer" +role. 

A problem with farming is that, the more you get the more you can get: for
instance if you grow mushrooms, the more you have the faster they multiply, and
then the more mushrooms you can grow.

So the quantities when farming tend to follow an exponential growth. I tried to
counteract it as much as possible. It starts with the default farming mod,
that I have edited so that harvesting cotton doesn't give back seeds.
Brown mushrooms turn the log they are on into dirt, thus preventing them to
spread multiple times.

Apple trees and blueberry bushes will randomly stop regrowing fruits, so
players will have to switch to renewable food sources (mushrooms, bread, fish,
honey).

Our farming mod is a simplified version of the one found in +MTG:
- one crafts farm plot blocks with seeds and dirt. When the seeds grow, the
  farm block turns back to dirt. No hoe needed.
- Light checks are done when the seed is ready to grow into a plant. If there
  are not under the sun, they won't.
- Dirt must be next to water in order to be able to turn it into a plantable
  field.
- crops grow in one step, after a delay that depends on biome's climate. The
  multistep scheme of the orginal mod is nice, but this is a severe penality
  for the players who will lose a lot of growth time if they go away.

Manticores (scorpionoids) and beetles may eat cotton and wheat. 

+FIREFALL+

Minefall is inspired from the defunct game "Firefall", which was something like
"an open world MMO FPS RPG with Jetpacks". A weird combo that made it perhaps a
bit niche. But it was a fun cooperative looting game. Some say it failed because
the project was poorly managed.

+FOCUS+

The "focus" chosen by players decides what their Focus wand does. Choosing a focus
is the way to level up to +regular player.

The light focus is most interesting to miners, as it provides light and the fire
than can make +coal explode.
The shadow focus is the one for players who want to specialize in hunting.
The air focus is interesting for explorers
The Wind focus is safety oriented, as it can be used to break a fall etc., but
is also fun to use.

+FURNITURE+

Furniture are blocks near which +traders will spawn. This category includes shops,
cabinets and shelves. The crafting guide mentions the property in the tooltips of
recipes.
The purpose of the furniture/cloth/NPC system is to provide some incentive for
players to decorate.

+GAME+

What is a game? People more clever than I provided definitions, but to me games
are about choices. Players make choices in order to achieve a goal, which can be
either set by the rules of the game or set by the players themselves.
When you select your sword rather than your wand to fight a mob, you make a
choice. When you spend some bronze on protections rather than making tools, you
make a choice. Even when the outcome of an action is determined by luck, you
take risks and expect rewards, so you make a choice (by the way, choices in the
presence of luck is the topic of "game theory" in mathematics).

Furthermore, games are, I believe, deeply related to learning and training. When
you watch puppies or kittens play together, it is obvious that those plays are
softer versions of what happens when dogs or cats fight for territory. So are
human games. Even when you play chess, you are training your tactics (Chess,
with its pieces that have different abilities, do mimick battles). From
cops-and-thieves to Go to video games, the relationship between the game world
and the real world has become generally thiner and thiner; some video games
do learn you things about the real world, even if it is a much simplified
version like in Sim City for instance. Sometimes the learning happens not inside
the game itself, but around it in chats and forums, where people discuss and try
to make points. It's not uncommon to see reasoning and arithmetic being used there
to backup claims. Personally I learned a few things about alloys, geology,
botanics while making the game.

In particular, as Minefall is an open world game with no set goal (although
the genre strongly favors "building stuff" as the main goal), it "teaches"
player how to set their own goals, which is an important skill IRL because the
real world is like that.

Those two aspects, choices and learning, influenced the designs of Minefall.

+GIFT+

See +perk.

+GLASS+

The most common type of glass looks good but actually hinders the view. Clear
glass can be found in jungle ruins and deep in the abyss, or is created by a
lightning impact on sand. Obsidian glass can be obtained by cooling lava.
Aside from obsidian glass, players cannot make glass.
This is a personal choice. I am for some reason fascinated by transparency in
games, but (or "so") I don't want to see it everywhere.

+GLIDER+

Gliders were one of the fun features in +FF. Fixed glider spots were placed by
world designers at high locations, players could use them to get back to the
nearest base quickly. You could also soar for a very long time. All for free.

A problem in +MF, though, is that fast transportations means sort of
"shrinking" the world. So you have to be careful about them.

Building glider spots is a bit expensive, and gliders themselves are not free.
I chose the "string" item as the price for them in order to create an
opportunity for trade. Cotton is not that expensive, but the process to get
them is pretty long. Typically a farmer would build the spot (+windmill) and
then place a shop nearby that sells strings. Their investment will eventually
be profitable.

+GOAL+

There is no particular goal in Minefall. However, a primary objective in
singleplayer mode is to build a farm; without it players cannot gain RP and
level up.

I believe that +MF puts enough roadblocks that players can have fun trying to
achieve the goals they set for themselves: builders will find it difficult to
gather materials, fighters will have to fight mobs with varied
characteristics, explorers will have to travel a lot to find remarkable places
and dungeons to link them together, and socializers will have plenty of
chances to interact with other players.

I also think that each type of player get something valuable from what they
like to do, which makes all of those activities viable by the way of trade.

+GRIND+

"Grinding" or "farming" are sometimes terms used to designate the fact that
players perform the same action again and again in order to gain something.
This is generally seen as a bad thing. To me it is a "necessary evil" in an open
world game. If the best items in the game are made with diamonds and if all you
have to do is mine a full stack of them, then the game is mostly over when you
gain access to a diamond mine.
What I have tried to do is make "grinding" fun. I think players have some
prefered activity (like killing monsters) that they don't mind to do all day. If
this activity is rewarding them with something, they can trade it and achieve
their actual goal. Perhaps it is not the most efficient way to do it, but
they'll have fun doing it.

+GUEST+

A guest is a newcomer who has not been granted the "interact" privilege by
another player via the +RP system.
The game sometimes employs the term "guest mode" which is a bit different,
because players can lose the "interact" privilege without losing their
+regular or +advanced level.
Players without "interact", in addition to being unable to interact with
anything, are ignored by mobs and cannot cause thunderstrikes. It prevents
most indirect griefing/trolling, but can also be used to tour safely the world
before actually playing the game.

Clearly, neither this system nor +MF is designed to be played by a
large number of random players, but rather a small number of selected
players - the power to select players being mostly in the hands of active
players thanks to the +RP system.

+HAMMER+

The hammer doubles as a weapon in order to give a wider range of options for
fights. It's signature feature is its "stopping" power, a 1 second stun on
hit. It deals a bit more damage than the iron sword but has a longer cooldown,
so its damage per second (DPS) is roughly the same.

+HIVE+

Yes, this is a "butterfly hive". I didn't like the bees of the original mod,
and it gives another function to +butterflies. The hive has to have a bunch of
flowers next to it to "operate".
The conversion of the egg into honey is done randomly on a rather long timer,
so "when" it happens is unpredictable.  Players will probably want to use
multiple hives to get a reliable supply of +honey.

+HONEY+

To be honnest, I had trouble with honey and changed its effect a couple of
times.
I wanted something more to do for farmers; the +hives can only produce a
type of food, but we already have various foods.
That said, the more interesting use of foods that restore more than 2 HP at a
time is for trading with NPCs, which implicitely make those ingredients a bit
expensive.
The current definition of honey is under this "trading presure", so it is
unlikely to be used "on the field".
It is slow but relatively easy to produce. It can be mass produced if a player
invests massively in this activity.
 
+HOTBAR+

The hotbar/selection bar was kept at 8 slots for two reasons:
- It fits the shape of the inventory panel (8x4). Even if you extend it to
  16 elements (non multiples of 8 are a no-go for usability), it still feels
  weird to use.
- I wanted to have the players think about their "loadout", that is what
  should be accessible rapidly. A miner will probably want to switch quickly
  between lamps, the compass, the pick and the sword. A hunter will probably
  fill their hotbar with wands, swords and food.

+HUNGER+

There is no hunger in +MF. It is easy to understand why, when one thinks about
the problems "hunger" is solving.

One problem is that often, in other games, it is easy to avoid getting hit and
losing HP (+armor makes it even worse). This is quite the opposite in MF. One
could say that the only way to not lose HP in MF it to do nothing.

The second problem is that in other games, it is easy to over-produce food,
so you have to "force" the players to eat so that farming can be a thing. MF
solves this problem using various mitigations regarding food production, and
by giving food another useful purposes (see +mana, NPC trading).

+I+

+J+

+K+

+LEVEL+

There are three-and-half player levels:
- +guest : you cannot really play, that's the "half" part.
- +beginner
- +regular
- +veteran (or +advanced)
Each level unlocks some new possibilities which make the game a bit easier, but
also increases the risk of losing stuff. So roughly speaking, this is the
classic risk/reward formula where both the risks and rewards are increased for
each level.
 
+LEVITATION+

The ability to "fly" for a short while was a signature feature of +Firefall.
It is a cornerstone on which is built Minefall.
Before Minetest introduced the "auto-jump" feature, it was also a huge
improvement for mobility and exploration. 
Levitation is implemented by making gravity 0. It is the jump impulse that
makes players move up, mainly.
One +perk makes it slightly negative, thus allowing players to break their
fall or go up after a short fall. 

+LICENSE+

Minefall is under AGPLv3 license, in order to be compatible with the licensing
terms of some of its components.

While I tried my best to respect the licensing terms of the mods I have used
to make Minefall, there are many of them and I tried a lot more during the
years I have spent on it. A mistake is likely (like a texture imported from
another mod, or a violation of the terms of a license).  If you spot such a
mistake, please let me know.
When there's no README with licensing statements, please take a look at the top
of the init.lua file of the mod.

As far as I am concerned, for the bits I did, my general policy is to
license it the same as the original mod (often licenses require it anyway).
Otherwise, my works are by default under "same license as Minetest", that is CC
BY-SA 3.0. If a license name for a piece I am the author of is ambiguous or
mispelled, please assume this license. In any case, I am unlikely to sue you
for the illegitimate reuse of my humble work, unless it is used to harm
kitties.

Finally, I would like to thank again the many mod makers of the MT community
who not only have made their hard work available for free, but also often
support their users for free.

+LIGHT+

MT has gotten better with light and light effects, but is still relatively
limited. Limitations are stimulating. The first tweak I introduced when I
started this game was to make those "useless" red mushrooms glow. Now they are
the "sine qua non" component of lamps. Glowing waters and glowing frozen grass
add a touch of magic to Minefall nights.

A general "lore" theme is that mese glows. Red mushroom glow because they
absorb and accumulate mese from the ground (this inspired from some
real-life mushrooms that accumulate metals), water glows because of dissolved
Mese.

+LOOT+ +LOOTBOX+

For mobs' "loot", see +drops.
Lootboxes are found in ruins and dungeons. Their primary purpose is to
provide non-craftable items like crafting tables or compass.
There is a rare chance that they drop a special items.

+LORE+

The suggested lore is as follows:
Players come from a medieval world that is ruled by a tyrant. This home world
is similar to this world except for Mese, which is much more rare. Magic and
mana are state secrets.
It is rummored that people, be they criminals, rebels or wrongly accused
innocents, are thrown into a "hell gate", either as a death sentence or for
experimentation purposes.  This "gate" is probably a beacon like one can find in
dungeons, that leads to this world.
People forced through this gate experience temporary or permanent amnesia (how
convenient... :-).
Minefall's world obviously seems to have been devastated by some sort of
cataclysm in the past, that wiped out an entire civilization. Interestingly,
legends of the home world suggest that people escaped a world on the verge of
destruction thanks to a divine intervention.
Everything strange or unnatural in the current world is supposedly caused by
+Mese, including +levitation and the ability to communicate over long
distances (i.e. the in-game chat feature, which is a form of telepathy).
+Traders are mysterious people from another world. They refuse to talk about
who they are or where they come from. You cannot really kill them, like players
they are "recalled" when critically injured.

+MANA+

Mana is used for +levitation, by +wands, by the hammer with the +anvil, by the
boat and the glider.

When eating at full health, the mana pool is refilled for the equivalent HP
value of the food item (+overeat). This feature extends the capabilities of
the player at the cost of food.

+MAPGEN+

Minefall is compatible with "mgv7" only. V7 sometimes generates improbable
landscapes, which is what I want for a fantasy game. Floating rocks here and
there don't concern me in a magic world.

By default, upon the creation of a new world, the game will use a random seed.
It can be challenging as often you'll spawn in hot or cold deserts.

The seed 12341234 is a good one for an easier start.

+MELDING+

Another feature of +Firefall. In the original game, there was this thing that
formed big walls around the world, preventing access. It was sort of like
"visible invisible walls", except they had events where players could push
those walls back temporarily.

Although +Firefall was a very "open world" game, after some experiments I
concluded that the feature does not fit in our open world. More precisely, it
creates a closed world that opens up, which is problematic for us in various ways.
So I chose to keep the world "open" by default, and then it closes down slowly
unless players take action.

The agents for this are mobs. Mobs have a lifespan in order to prevent
over-population. When a mob reaches its lifespan, it is simply removed with a
"teleport" effect to make it clear to players that they are gone, not just out
of sight range. But if a mob has been hit by a player or NPC, it leaves melding
behind.

Then the melding nodes self-replicate slowly with an ABM. The only ways to
remove it entirely are explosion blasts, which are generally a bit expensive
to set up, or planting saplings next to them; saplings absorb the melding and
become +aberrant tree saplings.

Players can also simply contain melding by building around it; protections
stop it as well.

But it is probably unavoidable that a unseen bit of melding will grow into an
uncontrollable cluster, especially on servers. However, melding does not
expands over 50 m. It leaves the hope for players to survive on mountains.


+MESE+

From a "lore" perspective, Mese is associated with magic. By
extension, it can also be associated with technology ("Any sufficiently
advanced technology is indistinguishable from magic" -- Arthur C. Clarke). For
instance one needs Mese to make paper from straw; it is sort of a "metaphore"
of the industrial process that is applied IRL.
Mese is thought to be the cause of all the strange things in the world:
+levitation, giant bugs, rocks hanging in the sky, things growing fast...
Mese is also intended to be used as a currency for players.

+METAL+

Metal blocks provide +protection.

Copper is very common. It will typically used for protection, because
+lootboxes also provide a good amount of it as bronze.

Tin is the second most common, but is under high demand pressure because of
its use in lamps.

Steel has 5 times the durability of bronze for tools, while being 4 times 
rarer.

Gold is the rarest but also not used for tools except the hammer,
which is best used to repair gold armor. A gold key is required to craft
+furniture.

+MF+

Short for +Minefall

+MINEFALL+

The name comes from the game it is inspired from, +Firefall. Nothing to do with
the MC mod or anything else. I was naively thinking it was an original name, so
I didn't do any Internet search...
Amusingly +Firefall, from which half of the name comes from, also clashes with
the name of a band.

+MOBS+

Due to how the engine works, it becomes quickly obvious that nothing happens
where there's no players. The +lore can simply acknowledge that indeed the
presence of players sort of brings the world back to life... For some magic
reason.

Nearly all mobs are hostile in +MF. They are used to protect resources, that
is to put something between the players and what they strive for.
Most spiders and wasps, the two big families of mobs, spawn near ores if not
on top of it. If they don't (like the Ice spider or the giant beetle), they
protect a biome as a whole, in particular the +ruins.

They can be a resource themselves. All drop mese, some drop seeds, etc.
So players who just like to fight stuff can make a living from their favorite
activity.

+MOON+

The moon in +MF is 4 times bigger than the sun. One may assume that it is
actually a planet and this world is actually a moon (nevermind the
astrophysical objections). Two reasons:
- I wanted less dark nights, and the bigger moon provides an explaination
- increase the fantasy world feel.

+MTG+

Short for "MineTest Game", the default game that used to be shipped with
Minetest, on which +MF is based.
MF is not a strict superset of MTG: some mods were added, others removed (e.g.
beds, vessels, buckets, minekart,...), others edited.
The non-obvious differences are listed in the Help tab.

+MULTIBOXING+

"Multiboxing" is the fact that players use multiple characters.
Sometimes games disallow it, but IMO there's no point in making rules one
cannot enforce.
If you cannot prevent it, then embrace it. The fact that characters can have
only one +focus and one +talent partly comes from that idea - it is more
effective to level up multiple characters and specialize each in certain
tasks.

The +clan system also could result in players having multiple character.

This however limits what the game can do; for instance it cannot give RP to
new players because players would create characters just to harvest RPs for
their main character; there cannot be direct competition between clans because
players could use puppet characters to spy on and/or sabotage the other clan.

+N+

+OBSIDIAN+

The obsidian shard has the unique ability to allow picking nodes as-is,
something called "silk touch" elsewhere. It can be obtained from traders for
an obsidian block, itself obtained by cooling lava.
Some nodes won't allow this for obvious reason: dungeon beacons, containers,
loot crates.

+ORE+

Metal ores can be found at all altitudes with the same density. Moreover, a
stone containing ore can drop any metal lump. The chance for each metal vary
between grey stone and desert stone.

I made it that way so that the whole world, both underground and mountains, is
worth exploring. Explorers will probably have a field day collecting "easy"
ores near the entrance of caves and on cliffs, but as more and more
people explore and pick easy ores, they'll have to go further and further
away. Caves are still interesting for miners because one needs specific
equipment to mine (lamps, compass and some weapons because of spiders). It is
less easy, but it should give more guarantees to find something. This is even
more true for underwater caves. That specialization also pays off as it gives
access to the basalt and abyss layers.

Smelting ores into ingots takes a lot of time. Partly to make it more
+believable, but also to increase the value of wood and the importance of
farming - tree growing being one possible aspects of it. It also increases
the value of +coal.
Another side effect is that having more than one furnace is not too fancy.
Actually two is almost a minimum (one for smelting, one for other tasks like
baking bread). With two or three, players can divide the time it takes to
smelt a stack of ore.

+OVEREAT+

"Overeat" happens when players eat at full health, or eat something with a HP
value greater than needed to fill their HP bar (for instance when eating an
apple at 19HP).
+MTG does nothing to prevent this. +MF fills the mana bar instead. This can be
used to "reload" faster during combat, or to prevent a deadly fall sometimes.
Players with the 'flyer' +talent can also use it to make longer flights.

+PAPER+

Paper in +MF is used for two valuable things: basic lamps and books. The main
use of books are +bookshelves, a type of +furniture.
The recipe mimics the Soda process, which is one of the first industrial
paper-making processes.

+PAPYRUS+

Papyrus could be grown and duplicated easily. Whatever resource it would give,
this resource would instantly become worthless.
In +MTG it is used to make paper, so paper is worthless and indeed, you don't do
much with paper. in +MF, +paper is technically cheap but quite long to obtain
in a sustainable way.


+PERK+

AKA "gift" in-game

A feature given to a player character based on its name, and not revealed to
the player. There are 3:
- better +levitation lift (gravity is slightly more negative when levitating)
- increased fishernet durability
- increased yellow mese wand durability

This extra player character feature is meant to enhance +multiboxing and
replaybility. With +focus, +talent and +perk features, there are 36 possible
combinations.

+PROGRESSION+

One thing I absolutely wanted to avoid with progression is to make the game
easier as players level up, because it also means that the game is more
difficult for newcomers, which is definitely a silly design.
Other games, in particular (MMO)RPGs, have zones associated with the various
levels of progression, where mobs become "beefier" and hit harder to
compensate the increasing power of the players.

The general idea of the design I came up with is simply to increase the
rewards and the risks together as the player levels up: leveling up unlocks
desirable +focus and +talents, but also increases the risk of losing your inventory
if you make a mistake and die because of it. This opens the possibility of
regression for players, which can be frustrating. But IMO the risk is also
more thrilling.

A second idea is that the rythme of progression should not be determined by XP
farming, but rather influenced by how good the players are at using their
resources. Leveling up or not is the players', not the game's, decision and
this decision is not a no-brainer (e.g. it is pointless to go regular if you
don't have the metal to make armor).

+PROTECTION+

The "peri" mod is basically a stripped-down version of "Protector_redo" with a
radius of one.

The scheme simplifies and (hopefully) fastens protection checks.
I did not go for whole-area protections, because those areas become "blocked"
if the player becomes inactive.  Protection is therefore more "progressive"
and is linked to the wealth of the players. It's also, I think, relatively
simple to use. The screwdriver can be used by players in order to check
their protections (but not others' - it would make it easy to find a "hole").
A side effect of this scheme, I found out, is that it also introduces
architectural constrains: how to use as few as possible protective blocks, and
how to hide them or integrate them in the decoration of the building.

An important effect of protection is to nullify missiles.  mainly in order to
protects against mobs' missiles, in particular those with area of effects such
as the wasp's smog. This is particularly useful when a player builds something
near a dangerous area where "shooter" mobs spawn often. 

Another effect is to prevent "monster" mobs from spawning, and to
hurt them. So even in solo play, protections are useful.

A weaker form of protection is the "columnar" feature, which originally was
implemented to protect +trees.

+PVP+

The game allows PvP, but the game is also hard enough that players would
rather cooperate until they can afford jousts. The game is invite-only so
it is expected players won't "PK" without some sort of agreement.
"Friendly fire" can happen in particular with wands. That's deliberate.

+Q+

+RANDOM+

Minetest features procedural generation using "perlin noise", a form a
randomness that's suitable to make landscapes.  Minefall uses "luck",
"randomness" a lot.
One reason is to make the game more interesting. When all is well something bad
can happen. When you're in the mud something good can happen and improve your
situation. Of course the reverse can happen (eg be wealthy and win the lotery).
The second reason is that it is a cheap way to simulate complicated things,
like +weather for instance.
The third, even less glorious reason, is that random rewards is something
that naturally keeps players playing. this is well-known and sometimes abused
in particular in free-to-play games. I have no interest in making our players
"addicted", but if the cause of the addiction is a form of pleasure, a little
bit shouldn't hurt (that said it can be frustrating at times; like when you
get 10 crafting tables in a row from lootboxes in the early game).

+REGULAR+ (player)

The "Regular player" level is an intermediate level. It unlocks enough
features while providing a good level of safety (bones), that some players
might choose to stay at this level rather than going +veteran.

+REPUTATION+

See +RP

+REWARD+

Drops and other rewards in +MF follows two principles: random and not too big.
Both try to workaround the fact that mobs can get stuck and be easy to kill.
What's more, players can modify the environment to have it happen or build
traps.
That's why there's no big boss that drops big rewards. The drops are generally
proportionate to how difficult it is to kill a mob, but even the abyss spider, which
is the "boss" of the game, has no guarantee to drop red mese.
The randomness is there just to add some variety. It seems to me that not
being sure to get item X when you kill mob Y makes the decision to fight it or
avoid it more interesting. See also +random

+ROLE+

The game is loosily based on the old Bartley classification of players:
- the socializers, who like to talk and share
- the achievers, who like to "max out" as much things as they can
- the explorers, who like to explore and discover stuff
- the killers, who like to kill stuff, including other players.

The way I see it, "socializers" would be farmers, as waiting for things to grow
can give a lot of free time (unless they intend to get rich, but that's more
an "achiever" thing). "killers" would be players who like to fight monsters,
or do PvP fights. "Explorers" are easy to figure out. "Achievers" would be builders.

+RP+

RP means "Reputation point(s)". The idea is to reward active players, and also
let players moderate each others a bit.
A player who lose all their RP lose automatically the "interact" privilege
(except admins), in other words they revert to simple guests (except they do
keep their level).
If a player dies, they lose 1 RP. It prevents suicidal behaviors, which could
be used in order to get resources without risk (e.g.  could create an alt,
invite it into the game with their main, and do the dirty/risky stuff with
this alt).

Digging/placing a crate costs/awards 1 RP. The cost is there to prevent
players from ransacking mindlessly ruins and dungeons.

The main source of RP is farming. It is a relatively elaborate process in
which players must acquire and manage various things to get lootboxes
from +NPCs.

Note: when a player has the "give" privilege on, the /rep command won't affect
their own RPs.
 
+SHADOW+

Mob:
The Shadow is an allied mob that can be summoned with the "shadow"-focused
+wand. It has a negative regen, so it loses about 1 HP per second. 
It can be used as a DPS support or just to distract hostile mobs, and perhaps as
an element of surprise, meatshield or blocker in PvP.

Engine feature:
It certainly brings more life to the game, but also visual noise. These days I
play with dynamic shadows turned off, but I spent quite some time to tune them
so that they look right night and day, and so that they are consistent with
the cloud coverage too.

+SHOP+

The shop mod used here is a bit inconvenient because the seller has to have
the item they want to buy.

Shops can be used as letterboxes or donation boxes (ask for a thing, give
nothing in return).

Shops are a protected +furniture. Their storage capacity is larger than other
containers, so they are still somewhat interesting in singleplayer.

+SHOVEL+

Better metal for the shovel translates to very little digging speed
improvment. In my experience, a shovel that digs too fast can be detrimental,
as you might dig nodes you don't want to. Instead, they are more durable.
This is more or less also the case for the other tools.

+SLATE+

Slate "cobble" is the alternative, non-flammable roofing material.

Besides the looks, it also differs from terracotta in that it is not
craftable; if the slate cobble from ruins are not enough, players can go for
slate blobs found in snowy biomes. With abyssite, it is one of the two stone
types that drop something when dug. It can also be interesting to pick them
with an +obsidian shard for decoration.

+SMOKE+

Smoke appears near flames and furnaces. Usually right on top unless
something is blocking. It normally moves up. It does not pass through
trapdoors, or other "walk through" nodes. Smoke can become +smog if it has
nowhere to go.
Smoke is implemented as air-like nodes and moved by timers. As a result, Smoke
will remain if all players live the area, so it can be visible from
afar and not "alive". That's a drawback of the implementation choices, but it
lets us have entire forests on fire with acceptable performance impact.

+SMOG+

Smog is a denser form of smoke, which causes 1 damage per second. Smog won't
go up or down unless it has nowhere else to go.
It appears near lava or "thanks" to certain mobs. Trapped +smoke also turns
into smog.

+SPIDER+

The orange spider spawns on top of all metal ore. It shoots
at the players a missile that turns into a smaller orange spider when it hits
a node. Smaller spiders also spawn from the cobwebs their "mothers" lay down,
if the cobwebs are lit (it is to prevent overpopulation in undiscovered caves).

The ice spider spawns on snow. It is a fast shooter.

The Abyss spider spawns on top of the Abyss' light blocks. Light the orange
spider, it shoots missiles that turn into "spiderlings". Abyss spiderlings
also are shooters (Beach crabs' teleportation missiles).

+STAIR+

The "stair" of the stairs-and-slabs mod has been reduced to a single "bar". This
shape is more versatile.
The slabs were removed because their lighting is buggy if you try to make them
block light (not really a solvable problem unless the engine spends a lot of
resources to get it right). An exception is cloth, because it is acceptable
for them to let some light through.
Stairs are not directly craftable. They drop when players dig a cobble-type or
wood-type block. This is to avoid the cluttering of the crafting book with
their recipes.

+STARTING+

An automated help system provides a message every two minutes to help
newcomers in solo play.

For starting a new world with the goal of opening a server, I suggest to start
playing it offline in order to get familiar with the area.

Play until you can set singleplayer's new home, which will be the default
spawning point for all new players. Then switch to your online avatar.
Temporarily grant yourself all privs in order to give yourself some RPs and
maybe remove all trace of singleplayer (protections - you can remove and place
back the beacon, it won't affect the spawning position itself).

+STONE+

Stones when dug generally drop nothing. Dungeons and ruins provide cobble,
which makes these buildings interesting to look for, and cobble itself less
"dirt-cheap".

+SWAMP+

The swamps biome is inspired from +Farlands. They feature giant grass that
beetles can turn into aspen saplings, and small water springs that permit in-land
farming.
The drawback is the poor visibility and the slowing giant grass that make it
easy for giant beetles to ambush players. 
It is also a wet climate biome where rain and thunderstorms happen often.

+TALENT+

At +regular level, players can use the /talent command to activate a talent
and level up to +veteran player.

The "flyer" talent is a good first choice as it is useful all the time.

The "fighter" talent is actually an economic choice, as it preserves food and
armor. It is however a double-edged sword, as multiple or heavy hits can leave
the player without an escape; unless they have some solid food ready (bread,
honey, fish).

The "traveler" talent is most interesting for traders/farmer, as it allows them
to move almost freely between their various locations and refill their shops.
 
Players can change their talent anytime for the same cost.

+TEAM+

Playing in groups or teams make things a lot easier, so I did very little to
help teamplay.

+TELEPORTATION+

Teleportation can happen in three ways:
- short range teleportation with the teleportation wand
- long range controlled teleportation with beacons
- the /recall command; this one is more intended for situations when a player
  is stuck somewhere. It costs 1 RP.

+THUNDERSTORM+

Thunderstorms happen when the weather is very bad. Thunderstrikes generally
have negative consequences for the players. It is meant to be like a
mini-event.

+TIN+

It has been difficult to find uses for tin in a low-tech world. Modern uses
are for alloys other than bronze with modern applications.

The default +MTG has been changed from 1/8 to 1/3 tin/copper ratio to make
bronze. The real-world ratio is around 1/10, so MTG is actually right on this,
but increasing it 1/3 is an acceptable approximation. It also helps a bit in
the early game because beginners can make their first bronze ingots faster.

Tin is mostly used for lamps, which makes it a metal in high demand. The
metaphore is that it is used as reflector (mirror) to amplify the light
source.

+TIPS+

The /tips command gives useful tips, but does not give the most useful ones,
which can look like "exploits", but are legit in my book:

- one can teleport mobs to make it easier to fight them, for instance between
  walls or trees. One can even prepare the place with protections for extra
  damage or even risk-free slow kill.
- obsidian shards lets one create "mob farms" e.g. by placing ore stone in
  favorable environments.
- there must be two non-walkable nodes above a spawning node for mobs to spawn.
  So one can place sand on top of known spawning nodes.

+TODO+

- check if channels prevent from seeing general chat.
- online: how to create spontaneous cooperation?
- bestiary screen

+TRADER+

From the +lore's perspective, traders are coming from another world.

From the gameplay perspective, traders create an incentive to build more and
bigger. Trading makes farming and building a more viable activity, if not
vital for the community: without farmers, gaining RP or obtaining certain
uncraftable items is a lot more difficult. 

Traders spawn on top of cloth blocks, exclusively near +furniture and
in places neither directly exposed to the sun nor too dark.
Cloth slabs and stairs have respectively 1/2 and 1/4 the chance to spawn
traders, compared to full blocks (this is fair wrt to the cost).

Until +regular level, the focus wand can be used to summon a trader. This
feature aims at letting players "speed up" the early game, at the cost of a
slowdown later.

+TRAVELER+

One of the +talents, but I might have used this word as a synonym for +trader
(actually, traveler players are likely to specialize in trading).

+TREE+

There are a lot of mods aimed at preventing the "trunkless trees syndrome"
caused by players that randomly cut bits of trees and then walk away. I made
yet another tree protection mod (YATPM) that prevents them from chopping a
tree node if there's the same node above it. Thanks to +levitation, players can
jump on top of a tree to cut it down.

This prevention is not flawless by any means, but at least it sends a signal
to the players: we don't like it, don't do that.

Also, as players mine the "easy" coal ores, it becomes more and more difficult
to get coal, and chopping down trees become the easier source of fuel.
Various mechanisms involving mobs create new saplings, so forests are
renewable - better so with some forest management.

In addition, cutting down a whole tree will make the leaves decay, which
is a significative amount of straw which is needed for paper.

A side effect of YATM is that houses made of tree logs benefit from a minor
protection: other players cannot dig a random part of the wall as they could
with most other material; astute players will probably figure out that they
only need to protect the top of the walls (this is usually where the roof
starts...). That said, fire could be an issue (protection prevents fire from
destroying protected nodes, but won't prevent the fire from spreading).

Another peculiarity in +MF with regard to trees is the leaves: they are not solid
(walkable), and the texture layout is different from +MTG.
Leaves are not walkable for several reasons. First, it avoids having mobs
on trees. Second, mobs generally have some difficulty to move in a world made of
cubes. Pass-through leaves clears their paths. Third, levitation would make it
too easy to "walk over" the mobs that typically spawn on the ground.

The leaves use a layout usually used for plants in order to obstruct the view
when the player is in a tree, and also in order to make the world a little bit
less cubic.

All leaves also have the move_resistance attribute, for realism. If they can
save players by breaking a fall, they also nearly nullify +levitation, which
can turn them into deadly traps.

The small pine tree present in MTG was removed because it had a three
leaves node top, which forces a leaf decay radius of 3 when 2 would be enough
if not for this. But having different trees with same trunk/leave types and
different decay radius requirements lead to inconsistent foliage shapes after
leaf decay, and sometimes irritating leaves hanging in mid-air. That was my
conclusion after trying to solve this problem many times, to no avail.
Modifying the schematic of the offender was a work not worth it.

+U+

+VETERAN+

"Veteran player" or "Advanced player" is the max level for a player.
It is obtained when a +regular player choses a +talent.
The perspective of losing one's inventory on death is quite scaring, in
particular because at this point, the player should have relatively expensive
items on them. Armor isn't dropped in order to not make it too punishing.

Probably the most interesting perk of leveling up, in the long run, is the
potentially infinite extension of the mana pool with RPs; 1 RP gives 0.01
extra mana, in other words 1 extra mana for 100 RP.

+WAND+

Wands are the tools that bring in the game some of the elements of +Firefall,
which had, like many MOBA/MMORPG games, player-selectable skills.
Except perhaps for the Mese wand that tries to bring some FPS feel. Of course,
MT is not an FPS engine, so we can't go too far there. Some mods provide
firearms, but I didn't want to give wrong expectations. Besides, firearms
don't really fit in our world.

+WASP+

When I started to make +MF it became quickly clear that it was difficult for
mobs to walk in a cube world. Especially if the mob is bigger than one of
those "cubes".
The way out was to allow them to +jump+ higher - sometimes laughably (but also
terrifyingly!) high, or to make them fly.

+WHALE+

Whales can be found not in water, but in the sky. I simply could not resist
this touch of fantasy. They spawn on top of snow blocks above altitude 60.
Players can ride whales. When doing so, mobs tend to not "see" the player -
probably because of its huge collision box - provided they don't get too
close.
The whale itself has 999 HP, so it can survive lots of accidental hits.
Players can hit (without hurting it) the whale to set its heading to the
direction they are looking.
So they provide a rather slow but convenient transport.
See also +disconnect.

+WEATHER+

Weather depends on the biome. Biomes with more humidity are more cloudy and
rainy. There is also a yearly cycle of about 63 in-game days.
Moon (day) 0 in the game corresponds to spring, so you should notice an
improvement of the weather in the first 15 days (summer).

In wet biomes during the rainy seasons it can happen that night seems to fall
in the middle of the day because of a +thunderstorm.

+WINDMILL+

The main purpose of windmills is to provide +gliders. Windmills can only be
placed above altitude 50, so that players won't place them at random
locations.
They are also relatively expensive, in order to make them a little rare.

+X+
+Y+
+Z+

